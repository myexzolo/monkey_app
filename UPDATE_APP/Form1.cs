﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace UPDATE_APP
{
    public partial class Form1 : Form
    {
        public string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string fileName = @"GYM Monkey.exe";
            closeProgram(fileName);

            string destinationPath  = CURRENT_DIRECTORY;
            string sourcePath       = CURRENT_DIRECTORY + @"\temp";

        
            CopyFile(sourcePath, destinationPath, fileName);

            string programClient = CURRENT_DIRECTORY + @"\" + fileName;
            Process.Start(programClient);
            this.Close();
        }

        private void CopyFile(string sourcePath, string destinationPath, string sourceFileName)
        {

            try
            {
                string sourcefile = System.IO.Path.Combine(sourcePath, sourceFileName);
                string destfile = System.IO.Path.Combine(destinationPath, sourceFileName);
                System.IO.File.Copy(sourcefile, destfile, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void closeProgram(string fileName)
        {
            
            string RunningProcess = fileName;
            foreach (Process proc in Process.GetProcessesByName(RunningProcess))
            {
                proc.Kill();
            }

        }
    }
}
