﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_checkin : Form
    {
        private string PersonCode;
        public frm_checkin(string PersonCode)
        {
            InitializeComponent();
            this.PersonCode = PersonCode;
        }

        private DataTable dtm;
        //private DataView dv;

        private string picCus = "";
        string pamPersonCode = "";
        string checkin = "";
        List<Person> PersonList;




        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                rowindex = -1;
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();

                if (txtSearch.Text.Equals(""))
                {
                    clearData();
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button7_Click ::" + ex.ToString(), "frm_checkin");
            }

        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private void hideloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = false;
                }));
            }
            else
            {
                picLoading.Visible = false;
            }

        }

        private void frm_customer_Load(object sender, EventArgs e)
        {

            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight = Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth - 265;

            int panelM = panelw1 / 100 * 96;

            panelMemberList.Width = panelw1;
            panelHistory.Width = panelw1;
            panelHistory.Height = scrHeight - 580;

            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);
            
            getPersons();
            setPersonCheckin("'CI'");

            
            txtSearch.Focus();
            picLoading.Visible = false;
        }


        private void getPersons()
        {
            try
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("ลายนิ้วมือ", typeof(bool));
                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อเล่น", typeof(string));
                dt.Columns.Add("คำนำหน้า", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("นามสกุล", typeof(string));
                dt.Columns.Add("วันเกิด", typeof(string));
                dt.Columns.Add("เริ่มวันที่", typeof(string));
                dt.Columns.Add("สิ้นสุดวันที่", typeof(string));
                dt.Columns.Add("Note", typeof(string));
                dt.Columns.Add("รูป", typeof(string));
                dt.Columns.Add("เบอร์โทร", typeof(string));
                dt.Columns.Add("E-Mail", typeof(string));
                dt.Columns.Add("สถานะ", typeof(string));

                PersonList = Global.PersonTrainerList;



                for (int i = 0; i < PersonList.Count(); i++)
                {
                    Person person = PersonList[i];

                    string status = "Active";
                    if (person.PERSON_STATUS.Equals("E"))
                    {
                        status = "Expire";
                    }

                    bool fgn = false;
                    dt.Rows.Add(fgn,
                    person.PERSON_CODE, person.PERSON_NICKNAME, person.PERSON_TITLE,
                    person.PERSON_NAME, person.PERSON_LASTNAME, person.PERSON_BIRTH_DATE_TXT,
                    person.PERSON_REGISTER_DATE_TXT, person.PERSON_EXPIRE_DATE_TXT, person.PERSON_NOTE, person.PERSON_IMAGE, person.PERSON_TEL_MOBILE, person.PERSON_EMAIL, status
                    );
                    

                }

                dtm = dt;
                //dataGridView1.ClearSelection();
                dataGridView1.DataSource = dt;
                //dataGridView1.Refresh();

                dataGridView1.Columns[1].Width = 120;
                dataGridView1.Columns[2].Width = 110;
                dataGridView1.Columns[3].Width = 130;
                dataGridView1.Columns[4].Width = 200;
                dataGridView1.Columns[5].Width = 220;
                dataGridView1.Columns[6].Width = 125;
                dataGridView1.Columns[7].Width = 125;
                dataGridView1.Columns[8].Width = 125;
                dataGridView1.Columns[11].Width = 120;
                dataGridView1.Columns[12].Width = 185;
                dataGridView1.Columns[13].Width = 120;

                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;

                dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[12].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[13].SortMode = DataGridViewColumnSortMode.NotSortable;


                if (!PersonCode.Equals(""))
                {
                    txtSearch.Text = PersonCode;
                    //PersonCode = "";
                }
                //dataGridView1.BeginEdit(false);
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPersons ::" + ex.ToString(), "frm_checkin");
            }
            finally
            {
                hideloading();

            }


        }


        private async void setPersonCheckin(string stausCheckin)
        {

            try
            {
                ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPersonCheckin(Global.BRANCH.branch_code, stausCheckin, "", "50","", "DESC","");
                dataGridView2.Rows.Clear();
                DateTime dDate;
                for (var i = 0; i < psList.Count; i++)
                {
                    PersonCheckin ps = psList[i];

                    string dstart = "";
                    string dexp = "";


                    if (DateTime.TryParse(ps.date_start, out dDate))
                    {

                        DateTime ds = Convert.ToDateTime(ps.date_start);
                        string dm = ds.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(ds.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }

                        dstart = dm + "/" + yy.ToString();
                    }

                    if (DateTime.TryParse(ps.date_expire, out dDate))
                    {
                        DateTime de = Convert.ToDateTime(ps.date_expire);
                        string dm = de.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(de.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        dexp = dm + "/" + yy.ToString();
                    }

                    dataGridView2.Rows.Add(ps.checkin_date, ps.PERSON_CODE, ps.PERSON_NICKNAME,
                    ps.PERSON_NAME + " " + ps.PERSON_LASTNAME ,ps.package_name, ps.use_package + "/" + ps.num_use, ps.package_unit, dstart, dexp, ps.reg_no);
                }
                dataGridView2.Refresh();
                dataGridView2.DefaultCellStyle.SelectionBackColor = dataGridView2.DefaultCellStyle.BackColor;
                dataGridView2.DefaultCellStyle.SelectionForeColor = dataGridView2.DefaultCellStyle.ForeColor;
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonCheckin ::" + ex.ToString(), "frm_checkin");

            }
        }

        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

            column.HeaderText = HeaderText;
            column.Name = Name;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.Width = Width;

            return column;
        }

        private int rowindex = -1;

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentCell  != null )
                {
                    rowindex = dataGridView1.CurrentCell.RowIndex;
                    Global.PERSON = new Person();

                    rowindex = dataGridView1.CurrentCell.RowIndex;
                    Global.PERSON.PERSON_CODE = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                    Global.PERSON.PERSON_NICKNAME = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                    Global.PERSON.PERSON_TITLE = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();
                    Global.PERSON.PERSON_NAME = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                    Global.PERSON.PERSON_LASTNAME = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                    Global.PERSON.PERSON_BIRTH_DATE = dataGridView1.Rows[rowindex].Cells[6].Value.ToString();
                    Global.PERSON.PERSON_REGISTER_DATE = dataGridView1.Rows[rowindex].Cells[7].Value.ToString();
                    Global.PERSON.PERSON_EXPIRE_DATE = dataGridView1.Rows[rowindex].Cells[8].Value.ToString();
                    Global.PERSON.PERSON_NOTE = dataGridView1.Rows[rowindex].Cells[9].Value.ToString();
                    Global.PERSON.PERSON_IMAGE = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                    Global.PERSON.PERSON_TEL_MOBILE = dataGridView1.Rows[rowindex].Cells[11].Value.ToString();
                    Global.PERSON.PERSON_EMAIL = dataGridView1.Rows[rowindex].Cells[12].Value.ToString();
                    Global.PERSON.PERSON_STATUS = dataGridView1.Rows[rowindex].Cells[13].Value.ToString();


                    labCode.Text = Global.PERSON.PERSON_CODE;
                    labName.Text = Global.PERSON.PERSON_NAME;
                    labLname.Text = Global.PERSON.PERSON_LASTNAME;
                    txtNote.Text = Global.PERSON.PERSON_NOTE;

                    if (Global.PERSON.PERSON_REGISTER_DATE != null && Global.PERSON.PERSON_EXPIRE_DATE != null && !Global.PERSON.PERSON_EXPIRE_DATE.Equals(""))
                    {
                        DateTime ds = DateTime.Parse(Global.PERSON.PERSON_REGISTER_DATE);
                        DateTime de = DateTime.Parse(Global.PERSON.PERSON_EXPIRE_DATE);
                        if (de > ds)
                        {
                            this.labStatus.ForeColor = System.Drawing.Color.GreenYellow;
                            labStatus.Text = "ACTIVE";
                        }
                        else
                        {
                            this.labStatus.ForeColor = System.Drawing.Color.Red;
                            labStatus.Text = "EXPIRE";
                        }
                    }
                    else
                    {
                        this.labStatus.ForeColor = System.Drawing.Color.Yellow;
                        labStatus.Text = "INACTIVE";
                    }


                    if (!dataGridView1.Rows[rowindex].Cells[10].Value.ToString().Equals(""))
                    {
                        picCus = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                        Utils.setImage(picCus, picPerson);
                    }
                    else
                    {
                        picCus = "";
                        picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                        picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                        picPerson.Size = new System.Drawing.Size(148, 178);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_SelectionChanged ::" + ex.ToString(), "frm_checkin");
            }
        }


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try {
                rowindex = -1;
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();

                if (txtSearch.Text.Equals(""))
                {
                    clearData();
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: txtSearch_TextChanged ::" + ex.ToString(), "frm_checkin");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (Global.PERSON.PERSON_CODE != null && !Global.PERSON.PERSON_CODE.Equals(""))
                {
                    frm_manage_checkin fmc = new frm_manage_checkin();
                    var result = fmc.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        getPersons();
                        setPersonCheckin("'CI'");
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: txtSearch_TextChanged ::" + ex.ToString(), "frm_checkin");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            getPersonsList();
        }



        private async void getPersonsList()
        {
            Global.PersonTrainerList = await ApiProcess.getPerson("TRAINER");
            getPersons();
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView2.DefaultCellStyle.SelectionBackColor = dataGridView2.DefaultCellStyle.BackColor;
            dataGridView2.DefaultCellStyle.SelectionForeColor = dataGridView2.DefaultCellStyle.ForeColor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm_fg_checkin_member fgc = new frm_fg_checkin_member("All", "", "PERSON");
            var result = fgc.ShowDialog();
            if (result == DialogResult.OK)
            {
                rowindex = -1;
                button2_Click(sender, e);
            }
        }

        private void frm_checkin_Shown(object sender, EventArgs e)
        {
            if (PersonCode.Equals(""))
            {
                clearData();
            }
            hideloading();

        }

        private void clearData()
        {
            dataGridView1.ClearSelection();
            Global.PERSON = new Person();

            labCode.Text = "";
            labName.Text = "";
            labLname.Text = "";
            txtNote.Text = "";
            labStatus.Text = "";
            picCus = "";
            picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            picPerson.Size = new System.Drawing.Size(148, 178);
        }

    }
}
