﻿namespace MONKEY_APP
{
    partial class frm_manage_cus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Note = new System.Windows.Forms.Label();
            this.xx = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLname = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.user_code = new System.Windows.Forms.TextBox();
            this.nameUser = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboGroupAge = new System.Windows.Forms.ComboBox();
            this.comboSex = new System.Windows.Forms.ComboBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textaddrAmphur = new System.Windows.Forms.ComboBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.textaddrTambol = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtaddrProvince = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textTel = new System.Windows.Forms.TextBox();
            this.textTel2 = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textaddrAmphur2 = new System.Windows.Forms.ComboBox();
            this.textaddrTambol2 = new System.Windows.Forms.ComboBox();
            this.txtaddrProvince2 = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTelOffice = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtZip2 = new System.Windows.Forms.TextBox();
            this.txtTexNo = new System.Windows.Forms.TextBox();
            this.textBillAdd = new System.Windows.Forms.TextBox();
            this.txtBillName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cusCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboGroup = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboTitle = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.alertTxt = new System.Windows.Forms.Label();
            this.dateIncome = new System.Windows.Forms.DateTimePicker();
            this.comboYear = new System.Windows.Forms.ComboBox();
            this.comboMonth = new System.Windows.Forms.ComboBox();
            this.comboDate = new System.Windows.Forms.ComboBox();
            this.comboEmp = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1149, 58);
            this.panel1.TabIndex = 115;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.user__1_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "เพิ่มทะเบียนสมาชิก";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::MONKEY_APP.Properties.Resources.id_card;
            this.button1.Location = new System.Drawing.Point(878, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 40);
            this.button1.TabIndex = 115;
            this.button1.Text = "  อ่านข้อมูลจากบัตรประชาชน";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(1119, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(79, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 24);
            this.label4.TabIndex = 116;
            this.label4.Text = "ชื่อ :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(47, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 24);
            this.label5.TabIndex = 116;
            this.label5.Text = "นามสกุล :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(24, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 24);
            this.label6.TabIndex = 116;
            this.label6.Text = "วันเกิด (พศ.) :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(15, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 24);
            this.label7.TabIndex = 116;
            this.label7.Text = "บัตรประชาชน :";
            // 
            // Note
            // 
            this.Note.AutoSize = true;
            this.Note.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Note.Location = new System.Drawing.Point(64, 368);
            this.Note.Name = "Note";
            this.Note.Size = new System.Drawing.Size(51, 24);
            this.Note.TabIndex = 116;
            this.Note.Text = "Note :";
            // 
            // xx
            // 
            this.xx.AutoSize = true;
            this.xx.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.xx.Location = new System.Drawing.Point(64, 487);
            this.xx.Name = "xx";
            this.xx.Size = new System.Drawing.Size(51, 24);
            this.xx.TabIndex = 116;
            this.xx.Text = "ผู้ดูแล :";
            this.xx.Click += new System.EventHandler(this.xx_Click);
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtName.Location = new System.Drawing.Point(121, 177);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(207, 30);
            this.txtName.TabIndex = 117;
            // 
            // txtLname
            // 
            this.txtLname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLname.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtLname.Location = new System.Drawing.Point(121, 224);
            this.txtLname.Multiline = true;
            this.txtLname.Name = "txtLname";
            this.txtLname.Size = new System.Drawing.Size(207, 30);
            this.txtLname.TabIndex = 117;
            // 
            // txtId
            // 
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtId.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtId.Location = new System.Drawing.Point(121, 318);
            this.txtId.Multiline = true;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(207, 30);
            this.txtId.TabIndex = 117;
            // 
            // txtNote
            // 
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNote.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtNote.Location = new System.Drawing.Point(123, 367);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(205, 106);
            this.txtNote.TabIndex = 117;
            // 
            // user_code
            // 
            this.user_code.BackColor = System.Drawing.Color.Gainsboro;
            this.user_code.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.user_code.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.user_code.Location = new System.Drawing.Point(379, 468);
            this.user_code.Multiline = true;
            this.user_code.Name = "user_code";
            this.user_code.ReadOnly = true;
            this.user_code.Size = new System.Drawing.Size(157, 30);
            this.user_code.TabIndex = 117;
            this.user_code.Visible = false;
            // 
            // nameUser
            // 
            this.nameUser.BackColor = System.Drawing.Color.Gainsboro;
            this.nameUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nameUser.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.nameUser.Location = new System.Drawing.Point(542, 468);
            this.nameUser.Multiline = true;
            this.nameUser.Name = "nameUser";
            this.nameUser.ReadOnly = true;
            this.nameUser.Size = new System.Drawing.Size(307, 30);
            this.nameUser.TabIndex = 117;
            this.nameUser.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(376, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 116;
            this.label8.Text = "กลุ่มอายุ :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(386, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 24);
            this.label9.TabIndex = 116;
            this.label9.Text = "ชื่อเล่น :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(402, 226);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 24);
            this.label10.TabIndex = 116;
            this.label10.Text = "เพศ :";
            // 
            // comboGroupAge
            // 
            this.comboGroupAge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboGroupAge.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboGroupAge.FormattingEnabled = true;
            this.comboGroupAge.Location = new System.Drawing.Point(449, 130);
            this.comboGroupAge.Name = "comboGroupAge";
            this.comboGroupAge.Size = new System.Drawing.Size(144, 32);
            this.comboGroupAge.TabIndex = 118;
            // 
            // comboSex
            // 
            this.comboSex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboSex.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboSex.FormattingEnabled = true;
            this.comboSex.Location = new System.Drawing.Point(449, 224);
            this.comboSex.Name = "comboSex";
            this.comboSex.Size = new System.Drawing.Size(144, 32);
            this.comboSex.TabIndex = 118;
            // 
            // txtNickName
            // 
            this.txtNickName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNickName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtNickName.Location = new System.Drawing.Point(449, 177);
            this.txtNickName.Multiline = true;
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(144, 30);
            this.txtNickName.TabIndex = 117;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.tabControl1.Location = new System.Drawing.Point(622, 86);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(518, 376);
            this.tabControl1.TabIndex = 120;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(510, 339);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "    ที่อยู่ปัจุบัน   ";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textaddrAmphur);
            this.panel3.Controls.Add(this.txtZip);
            this.panel3.Controls.Add(this.textaddrTambol);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.txtaddrProvince);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.textEmail);
            this.panel3.Controls.Add(this.textTel);
            this.panel3.Controls.Add(this.textTel2);
            this.panel3.Controls.Add(this.txtAddress2);
            this.panel3.Controls.Add(this.txtAddress1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(504, 333);
            this.panel3.TabIndex = 0;
            // 
            // textaddrAmphur
            // 
            this.textaddrAmphur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textaddrAmphur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrAmphur.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrAmphur.FormattingEnabled = true;
            this.textaddrAmphur.Location = new System.Drawing.Point(356, 118);
            this.textaddrAmphur.Name = "textaddrAmphur";
            this.textaddrAmphur.Size = new System.Drawing.Size(129, 32);
            this.textaddrAmphur.TabIndex = 151;
            this.textaddrAmphur.SelectedIndexChanged += new System.EventHandler(this.textaddrAmphur_SelectedIndexChanged);
            // 
            // txtZip
            // 
            this.txtZip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtZip.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtZip.Location = new System.Drawing.Point(356, 164);
            this.txtZip.Multiline = true;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(129, 30);
            this.txtZip.TabIndex = 148;
            // 
            // textaddrTambol
            // 
            this.textaddrTambol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textaddrTambol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrTambol.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrTambol.FormattingEnabled = true;
            this.textaddrTambol.Location = new System.Drawing.Point(104, 164);
            this.textaddrTambol.Name = "textaddrTambol";
            this.textaddrTambol.Size = new System.Drawing.Size(157, 32);
            this.textaddrTambol.TabIndex = 149;
            this.textaddrTambol.SelectedIndexChanged += new System.EventHandler(this.textaddrTambol_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(242, 260);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(107, 24);
            this.label17.TabIndex = 118;
            this.label17.Text = "เบอร์โทรฉุกเฉิน :";
            // 
            // txtaddrProvince
            // 
            this.txtaddrProvince.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtaddrProvince.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtaddrProvince.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtaddrProvince.FormattingEnabled = true;
            this.txtaddrProvince.Location = new System.Drawing.Point(104, 118);
            this.txtaddrProvince.Name = "txtaddrProvince";
            this.txtaddrProvince.Size = new System.Drawing.Size(157, 32);
            this.txtaddrProvince.TabIndex = 150;
            this.txtaddrProvince.SelectedIndexChanged += new System.EventHandler(this.txtaddrProvince_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(280, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 24);
            this.label14.TabIndex = 119;
            this.label14.Text = "ไปรษณีย์ :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(267, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 24);
            this.label11.TabIndex = 120;
            this.label11.Text = "เขต/อำเภอ :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(43, 214);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 24);
            this.label16.TabIndex = 121;
            this.label16.Text = "E-mail :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(50, 260);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 24);
            this.label15.TabIndex = 121;
            this.label15.Text = "มือถือ :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(13, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 24);
            this.label13.TabIndex = 122;
            this.label13.Text = "แขวง/ตำบล :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(43, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 24);
            this.label12.TabIndex = 123;
            this.label12.Text = "จังหวัด :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(58, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 24);
            this.label2.TabIndex = 124;
            this.label2.Text = "ที่อยู่ :";
            // 
            // textEmail
            // 
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textEmail.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textEmail.Location = new System.Drawing.Point(104, 212);
            this.textEmail.Multiline = true;
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(381, 30);
            this.textEmail.TabIndex = 125;
            // 
            // textTel
            // 
            this.textTel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textTel.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textTel.Location = new System.Drawing.Point(104, 257);
            this.textTel.Multiline = true;
            this.textTel.Name = "textTel";
            this.textTel.Size = new System.Drawing.Size(132, 30);
            this.textTel.TabIndex = 125;
            // 
            // textTel2
            // 
            this.textTel2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textTel2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textTel2.Location = new System.Drawing.Point(356, 257);
            this.textTel2.Multiline = true;
            this.textTel2.Name = "textTel2";
            this.textTel2.Size = new System.Drawing.Size(129, 30);
            this.textTel2.TabIndex = 126;
            // 
            // txtAddress2
            // 
            this.txtAddress2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAddress2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAddress2.Location = new System.Drawing.Point(104, 68);
            this.txtAddress2.Multiline = true;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(381, 30);
            this.txtAddress2.TabIndex = 131;
            // 
            // txtAddress1
            // 
            this.txtAddress1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAddress1.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAddress1.Location = new System.Drawing.Point(104, 18);
            this.txtAddress1.Multiline = true;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(381, 30);
            this.txtAddress1.TabIndex = 132;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(510, 339);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "    ที่อยู่ในการออกใบเสร็จ   ";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textaddrAmphur2);
            this.panel4.Controls.Add(this.textaddrTambol2);
            this.panel4.Controls.Add(this.txtaddrProvince2);
            this.panel4.Controls.Add(this.button6);
            this.panel4.Controls.Add(this.button3);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label25);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.txtTelOffice);
            this.panel4.Controls.Add(this.txtFax);
            this.panel4.Controls.Add(this.txtZip2);
            this.panel4.Controls.Add(this.txtTexNo);
            this.panel4.Controls.Add(this.textBillAdd);
            this.panel4.Controls.Add(this.txtBillName);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(504, 333);
            this.panel4.TabIndex = 1;
            // 
            // textaddrAmphur2
            // 
            this.textaddrAmphur2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textaddrAmphur2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrAmphur2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrAmphur2.FormattingEnabled = true;
            this.textaddrAmphur2.Location = new System.Drawing.Point(355, 137);
            this.textaddrAmphur2.Name = "textaddrAmphur2";
            this.textaddrAmphur2.Size = new System.Drawing.Size(133, 32);
            this.textaddrAmphur2.TabIndex = 153;
            this.textaddrAmphur2.SelectedIndexChanged += new System.EventHandler(this.textaddrAmphur2_SelectedIndexChanged);
            // 
            // textaddrTambol2
            // 
            this.textaddrTambol2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textaddrTambol2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrTambol2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrTambol2.FormattingEnabled = true;
            this.textaddrTambol2.Location = new System.Drawing.Point(115, 183);
            this.textaddrTambol2.Name = "textaddrTambol2";
            this.textaddrTambol2.Size = new System.Drawing.Size(146, 32);
            this.textaddrTambol2.TabIndex = 151;
            this.textaddrTambol2.SelectedIndexChanged += new System.EventHandler(this.textaddrTambol_SelectedIndexChanged);
            // 
            // txtaddrProvince2
            // 
            this.txtaddrProvince2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtaddrProvince2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtaddrProvince2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtaddrProvince2.FormattingEnabled = true;
            this.txtaddrProvince2.Location = new System.Drawing.Point(115, 137);
            this.txtaddrProvince2.Name = "txtaddrProvince2";
            this.txtaddrProvince2.Size = new System.Drawing.Size(146, 32);
            this.txtaddrProvince2.TabIndex = 152;
            this.txtaddrProvince2.SelectedIndexChanged += new System.EventHandler(this.txtaddrProvince2_SelectedIndexChanged);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button6.Location = new System.Drawing.Point(236, 9);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(115, 29);
            this.button6.TabIndex = 124;
            this.button6.Text = "ตามที่อยู่สมาชิก";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button3.Location = new System.Drawing.Point(115, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 29);
            this.button3.TabIndex = 124;
            this.button3.Text = "ตามชื่อสมาชิก";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(295, 234);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 24);
            this.label21.TabIndex = 118;
            this.label21.Text = "แฟกซ์ :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(280, 188);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 24);
            this.label22.TabIndex = 119;
            this.label22.Text = "ไปรษณีย์ :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label23.Location = new System.Drawing.Point(267, 140);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 24);
            this.label23.TabIndex = 120;
            this.label23.Text = "เขต/อำเภอ :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.Location = new System.Drawing.Point(42, 234);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(67, 24);
            this.label24.TabIndex = 121;
            this.label24.Text = "โทรศัพท์ :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.Location = new System.Drawing.Point(20, 188);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(89, 24);
            this.label25.TabIndex = 122;
            this.label25.Text = "แขวง/ตำบล :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.Location = new System.Drawing.Point(50, 141);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 24);
            this.label26.TabIndex = 123;
            this.label26.Text = "จังหวัด :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(7, 281);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 24);
            this.label28.TabIndex = 124;
            this.label28.Text = "เลขที่ผู้เสียภาษี :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.Location = new System.Drawing.Point(39, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 24);
            this.label29.TabIndex = 124;
            this.label29.Text = "ชื่อบริษัท :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.Location = new System.Drawing.Point(65, 94);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 24);
            this.label27.TabIndex = 124;
            this.label27.Text = "ที่อยู่ :";
            // 
            // txtTelOffice
            // 
            this.txtTelOffice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelOffice.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTelOffice.Location = new System.Drawing.Point(115, 231);
            this.txtTelOffice.Multiline = true;
            this.txtTelOffice.Name = "txtTelOffice";
            this.txtTelOffice.Size = new System.Drawing.Size(146, 30);
            this.txtTelOffice.TabIndex = 125;
            // 
            // txtFax
            // 
            this.txtFax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFax.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtFax.Location = new System.Drawing.Point(356, 231);
            this.txtFax.Multiline = true;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(129, 30);
            this.txtFax.TabIndex = 126;
            // 
            // txtZip2
            // 
            this.txtZip2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtZip2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtZip2.Location = new System.Drawing.Point(356, 184);
            this.txtZip2.Multiline = true;
            this.txtZip2.Name = "txtZip2";
            this.txtZip2.Size = new System.Drawing.Size(129, 30);
            this.txtZip2.TabIndex = 127;
            // 
            // txtTexNo
            // 
            this.txtTexNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTexNo.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTexNo.Location = new System.Drawing.Point(118, 278);
            this.txtTexNo.Multiline = true;
            this.txtTexNo.Name = "txtTexNo";
            this.txtTexNo.Size = new System.Drawing.Size(370, 30);
            this.txtTexNo.TabIndex = 131;
            // 
            // textBillAdd
            // 
            this.textBillAdd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBillAdd.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBillAdd.Location = new System.Drawing.Point(115, 90);
            this.textBillAdd.Multiline = true;
            this.textBillAdd.Name = "textBillAdd";
            this.textBillAdd.Size = new System.Drawing.Size(370, 30);
            this.textBillAdd.TabIndex = 131;
            // 
            // txtBillName
            // 
            this.txtBillName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBillName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtBillName.Location = new System.Drawing.Point(115, 44);
            this.txtBillName.Multiline = true;
            this.txtBillName.Name = "txtBillName";
            this.txtBillName.Size = new System.Drawing.Size(370, 30);
            this.txtBillName.TabIndex = 132;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(30, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 24);
            this.label18.TabIndex = 116;
            this.label18.Text = "รหัสสมาชิก :";
            // 
            // cusCode
            // 
            this.cusCode.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.cusCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cusCode.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cusCode.Location = new System.Drawing.Point(121, 86);
            this.cusCode.Multiline = true;
            this.cusCode.Name = "cusCode";
            this.cusCode.ReadOnly = true;
            this.cusCode.Size = new System.Drawing.Size(207, 30);
            this.cusCode.TabIndex = 117;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(381, 277);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 24);
            this.label19.TabIndex = 116;
            this.label19.Text = "รูปภาพ :";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(449, 274);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(143, 152);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            // 
            // picLoading
            // 
            this.picLoading.BackColor = System.Drawing.Color.White;
            this.picLoading.Image = global::MONKEY_APP.Properties.Resources.loading2;
            this.picLoading.Location = new System.Drawing.Point(542, 241);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(109, 96);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoading.TabIndex = 123;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(599, 524);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 24);
            this.label3.TabIndex = 116;
            this.label3.Text = "กลุ่มสมาชิก :";
            this.label3.Visible = false;
            // 
            // comboGroup
            // 
            this.comboGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboGroup.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboGroup.FormattingEnabled = true;
            this.comboGroup.Location = new System.Drawing.Point(481, 360);
            this.comboGroup.Name = "comboGroup";
            this.comboGroup.Size = new System.Drawing.Size(10, 32);
            this.comboGroup.TabIndex = 118;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(23, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 24);
            this.label20.TabIndex = 116;
            this.label20.Text = "คำนำหน้าชื่อ :";
            // 
            // comboTitle
            // 
            this.comboTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboTitle.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboTitle.FormattingEnabled = true;
            this.comboTitle.Location = new System.Drawing.Point(121, 127);
            this.comboTitle.Name = "comboTitle";
            this.comboTitle.Size = new System.Drawing.Size(207, 32);
            this.comboTitle.TabIndex = 118;
            this.comboTitle.SelectedValueChanged += new System.EventHandler(this.comboTitle_SelectedValueChanged);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button2.Location = new System.Drawing.Point(449, 433);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 29);
            this.button2.TabIndex = 124;
            this.button2.Text = "Upload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(840, 497);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 51);
            this.button4.TabIndex = 151;
            this.button4.Text = "บันทึก";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button5.Location = new System.Drawing.Point(1007, 497);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 51);
            this.button5.TabIndex = 152;
            this.button5.Text = "ยกเลิก";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // alertTxt
            // 
            this.alertTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alertTxt.BackColor = System.Drawing.Color.Transparent;
            this.alertTxt.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.alertTxt.ForeColor = System.Drawing.Color.Red;
            this.alertTxt.Location = new System.Drawing.Point(121, 522);
            this.alertTxt.MinimumSize = new System.Drawing.Size(270, 0);
            this.alertTxt.Name = "alertTxt";
            this.alertTxt.Size = new System.Drawing.Size(458, 34);
            this.alertTxt.TabIndex = 153;
            this.alertTxt.Text = "alert";
            this.alertTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.alertTxt.Visible = false;
            // 
            // dateIncome
            // 
            this.dateIncome.CalendarFont = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.CustomFormat = "yyyy/MM/dd";
            this.dateIncome.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateIncome.Location = new System.Drawing.Point(507, 363);
            this.dateIncome.Name = "dateIncome";
            this.dateIncome.Size = new System.Drawing.Size(48, 31);
            this.dateIncome.TabIndex = 0;
            this.dateIncome.ValueChanged += new System.EventHandler(this.dateIncome_ValueChanged);
            // 
            // comboYear
            // 
            this.comboYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboYear.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboYear.FormattingEnabled = true;
            this.comboYear.Location = new System.Drawing.Point(264, 269);
            this.comboYear.Name = "comboYear";
            this.comboYear.Size = new System.Drawing.Size(64, 32);
            this.comboYear.TabIndex = 173;
            this.comboYear.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboMonth
            // 
            this.comboMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboMonth.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboMonth.FormattingEnabled = true;
            this.comboMonth.Location = new System.Drawing.Point(173, 269);
            this.comboMonth.Name = "comboMonth";
            this.comboMonth.Size = new System.Drawing.Size(85, 32);
            this.comboMonth.TabIndex = 174;
            this.comboMonth.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboDate
            // 
            this.comboDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboDate.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboDate.FormattingEnabled = true;
            this.comboDate.Location = new System.Drawing.Point(121, 269);
            this.comboDate.Name = "comboDate";
            this.comboDate.Size = new System.Drawing.Size(46, 32);
            this.comboDate.TabIndex = 175;
            this.comboDate.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboEmp
            // 
            this.comboEmp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboEmp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboEmp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboEmp.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboEmp.FormattingEnabled = true;
            this.comboEmp.Location = new System.Drawing.Point(122, 487);
            this.comboEmp.Name = "comboEmp";
            this.comboEmp.Size = new System.Drawing.Size(320, 32);
            this.comboEmp.TabIndex = 177;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(346, 95);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 178;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.Location = new System.Drawing.Point(364, 90);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(123, 22);
            this.label30.TabIndex = 179;
            this.label30.Text = "กำหนดรหัสสมาชิกเอง";
            // 
            // frm_manage_cus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1149, 560);
            this.ControlBox = false;
            this.Controls.Add(this.label30);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboEmp);
            this.Controls.Add(this.comboYear);
            this.Controls.Add(this.comboMonth);
            this.Controls.Add(this.comboDate);
            this.Controls.Add(this.alertTxt);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.comboSex);
            this.Controls.Add(this.comboTitle);
            this.Controls.Add(this.comboGroup);
            this.Controls.Add(this.comboGroupAge);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.nameUser);
            this.Controls.Add(this.user_code);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.txtLname);
            this.Controls.Add(this.txtNickName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.cusCode);
            this.Controls.Add(this.xx);
            this.Controls.Add(this.Note);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dateIncome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_manage_cus";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_manage_cus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Note;
        private System.Windows.Forms.Label xx;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLname;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.TextBox user_code;
        private System.Windows.Forms.TextBox nameUser;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboGroupAge;
        private System.Windows.Forms.ComboBox comboSex;
        private System.Windows.Forms.TextBox txtNickName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox cusCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboGroup;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboTitle;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textTel;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtTelOffice;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.TextBox txtZip2;
        private System.Windows.Forms.TextBox txtTexNo;
        private System.Windows.Forms.TextBox textBillAdd;
        private System.Windows.Forms.TextBox txtBillName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox textaddrAmphur;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.ComboBox textaddrTambol;
        private System.Windows.Forms.ComboBox txtaddrProvince;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label alertTxt;
        private System.Windows.Forms.ComboBox textaddrTambol2;
        private System.Windows.Forms.ComboBox txtaddrProvince2;
        private System.Windows.Forms.ComboBox textaddrAmphur2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateIncome;
        private System.Windows.Forms.ComboBox comboYear;
        private System.Windows.Forms.ComboBox comboMonth;
        private System.Windows.Forms.ComboBox comboDate;
        private System.Windows.Forms.ComboBox comboEmp;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.TextBox textTel2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label30;
    }
}