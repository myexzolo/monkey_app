﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_packege : Form
    {

        public frm_packege()
        {
            InitializeComponent();
        }

        string active = "";

        private DataTable dtm;

        public List<Package> PackageList;

        private void button7_Click(object sender, EventArgs e)
        {
            DataView dv = dtm.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or รายละเอียด like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataView dv = dtm.DefaultView;
            dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or รายละเอียด like '%{0}%'", txtSearch.Text);
            dataGridView1.DataSource = dv.ToTable();
        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }


        private void frm_customer_Load(object sender, EventArgs e)
        {
            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight =  Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth;
       
            int panelM  = panelw1 / 100 * 98;

            panelList.Width = panelM;
            panelList.Height = scrHeight - 265;


            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("6"))
                {
                    if (code.Equals("APAG"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.folder__2_;
                    }
                    else if (code.Equals("EPAG"))
                    {
                        width = 160;
                        img = global::MONKEY_APP.Properties.Resources.folder2;
                    }
                    else if (code.Equals("DPAG"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.del;
                    }
                    
                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));
                    num++;
                }

            }

            Button button = genButtonMenu("VPAG", 160, "ดูข้อมูลทั้งหมด", global::MONKEY_APP.Properties.Resources.eye2);

            buttons.Add(button);
            tableLayoutPanel.ColumnCount = buttons.Count;

            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }

            txtSearch.Focus();
            PackageList = Global.PackageList;

            comboStatus.Items.Add(new Item("ใช้งาน", "'Y'"));
            comboStatus.Items.Add(new Item("ไม่ใช้งาน", "'N'"));
            comboStatus.Items.Add(new Item("ทั้งหมด", "'N','Y'"));

            comboStatus.SelectedIndex = 0;
        }


        private async void getPackage(string typeSearch)
        {

            try
            {
                if (active.Equals("ADD") || active.Equals("EDIT") || active.Equals("REF"))
                {
                    ApiRest.InitailizeClient();
                    PackageList = await ApiProcess.getPackage(typeSearch);
                    Global.PackageList = PackageList;
                }


                DataTable dt = new DataTable();

                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("รายละเอียด", typeof(string));
                dt.Columns.Add("ประเภทแพคเกจ", typeof(string));
                dt.Columns.Add("ราคา", typeof(string));
                dt.Columns.Add("ระยะเวลา", typeof(string));
                dt.Columns.Add("หน่วย", typeof(string));
                dt.Columns.Add("อายุการใช้งาน", typeof(string));
                dt.Columns.Add("การแจ้งเตือน", typeof(string));
                dt.Columns.Add("สถานะ", typeof(string));
                dt.Columns.Add("id", typeof(string));

                for (int i = 0; i < PackageList.Count(); i++)
                {
                    string masUnit  = "";
                    string masNoti   = "";
                    string masType   = "";
                    string masStatus = "";

                    Package package = PackageList[i];

                    for (int y = 0; y < Global.TIME_UNIT.Count; y++)
                    {
                        if (Global.TIME_UNIT[y].DATA_GROUP == "TIME_UNIT" && Global.TIME_UNIT[y].DATA_CODE == package.package_unit)
                        {
                            masUnit = Global.TIME_UNIT[y].DATA_DESC1;
                        }

                        if (Global.TIME_UNIT[y].DATA_GROUP == "TIME_UNIT" && Global.TIME_UNIT[y].DATA_CODE == package.notify_unit)
                        {
                            masNoti = Global.TIME_UNIT[y].DATA_DESC1;
                        }
                    }


                    if (package.package_type == "MB")
                    {
                        masType = "Member";
                    }
                    else if (package.package_type == "PT")
                    {
                        masType = "Personal Trainer";
                    }


                    if (package.package_status == "Y")
                    {
                        masStatus = "ใช้งาน";
                    }
                    else if (package.package_status == "N")
                    {
                        masStatus = "ไม่ใช้งาน";
                    }

                    string numPrice = String.Format("{0:n}", package.package_price);
                    string numUse = String.Format("{0}", package.num_use);
                    string maxUse = String.Format("{0}", package.max_use) + " วัน";

                    string strNoti = package.notify_num + " " + masNoti;


                    dt.Rows.Add(package.package_code,package.package_name,
                        package.package_detail, masType,
                        numPrice, numUse, 
                        masUnit, maxUse,
                        strNoti, masStatus, package.package_id
                    );
                }

                dtm = dt;
                dataGridView1.DataSource = dt;
                dataGridView1.Refresh();

                dataGridView1.Columns[0].Width = 100;
                dataGridView1.Columns[1].Width = 250;
                dataGridView1.Columns[2].Width = 500;
                dataGridView1.Columns[3].Width = 150;
                dataGridView1.Columns[4].Width = 100;
                dataGridView1.Columns[5].Width = 100;
                dataGridView1.Columns[6].Width = 100;
                dataGridView1.Columns[7].Width = 150;
                dataGridView1.Columns[8].Width = 200;
                dataGridView1.Columns[9].Width = 120;
                dataGridView1.Columns[10].Visible = false;


                dataGridView1.Columns[4].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[5].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[7].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[8].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[9].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleCenter;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width) {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

            column.HeaderText = HeaderText;
            column.Name = Name;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.Width = Width;

            return column;
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = menuName;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }

        private void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();
            active = "";
            if (code.Equals("APAG"))
            {
                frm_manage_package fmc = new frm_manage_package("ADD");
                var result = fmc.ShowDialog();
                if (result == DialogResult.OK) {
                    active = "ADD";
                    getPackage((comboStatus.SelectedItem as Item).Value.ToString());
                }
            }
            else if (code.Equals("EPAG"))
            {
                frm_manage_package fmc = new frm_manage_package("EDIT");
                var result = fmc.ShowDialog();
                if (result == DialogResult.OK)
                {
                    active = "EDIT";
                    getPackage((comboStatus.SelectedItem as Item).Value.ToString());
                }
            }
            else if (code.Equals("DPAG"))
            {
                frm_dialog d = new frm_dialog("ยืนยันการลบแพคเกจ/สินค้า ! \r\n" + Global.PACKAGE.package_name, "");
                d.ShowDialog();
                if (d.DialogResult == DialogResult.OK)
                {
                    delData((comboStatus.SelectedItem as Item).Value.ToString());
                }
            }
            else if (code.Equals("VPAG"))
            {
                ;
            }
        }

        private async void delData(string status)
        {
            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.managePackage("DEL", Global.PACKAGE, Global.USER.user_login);
                if (res.status)
                {
                    active = "REF";
                    getPackage(status);
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: delData ::" + ex.ToString(), "frm_customer");
            }
            finally
            {
                //picLoading.Visible = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int rowindex = dataGridView1.CurrentCell.RowIndex;
                Global.PACKAGE.package_id = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                Global.PACKAGE.package_name = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
            } 
        }

        private void comboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            getPackage((comboStatus.SelectedItem as Item).Value.ToString());
        }
    }
}
