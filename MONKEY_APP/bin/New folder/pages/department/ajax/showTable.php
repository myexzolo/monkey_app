<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>Name</td>
      <td>สถานะ</td>
      <td>Edit/Del</td>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT * FROM t_department ORDER BY 	department_id DESC";

      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($i=0 ; $i < $dataCount ; $i++) {
    ?>
    <tr class="text-center">
      <td><?=$i + 1;?></td>
      <td><?=$rows[$i]['department_code'];?></td>
      <td><?=$rows[$i]['department_name'];?></td>
      <td><?=$rows[$i]['is_active']=='Y'?"ACTIVE":"NO ACTIVE";?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm('EDIT','<?=$rows[$i]['department_id']?>')">Edit</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="del('<?=$rows[$i]['department_id']?>','<?=$rows[$i]['department_name']?>')">Del</button>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
