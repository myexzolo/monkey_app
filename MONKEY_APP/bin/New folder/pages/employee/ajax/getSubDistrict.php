<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code     = isset($_POST['code'])?$_POST['code']:"";
$name     = isset($_POST['name'])?$_POST['name']:"";

$option  = getoptionSubDistrict($code,$name);

?>

<select name="EMP_ADDR_SUBDISTRICT" id="EMP_ADDR_SUBDISTRICT" class="form-control select2" style="width: 100%;"  onchange="changeSubDistrict()">
  <option value="" selected></option>
  <?= $option ?>
</select>
