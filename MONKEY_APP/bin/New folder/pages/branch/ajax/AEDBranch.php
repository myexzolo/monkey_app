<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action         = isset($_POST['action'])?$_POST['action']:"ADD";
$branch_id      = isset($_POST['branch_id'])?$_POST['branch_id']:"";
$branch_code    = strtoupper(isset($_POST['branch_code'])?$_POST['branch_code']:"");
$cname          = isset($_POST['cname'])?$_POST['cname']:"";
$branch_name    = isset($_POST['branch_name'])?$_POST['branch_name']:"";
$branch_address = isset($_POST['branch_address'])?$_POST['branch_address']:"";
$branch_tel     = isset($_POST['branch_tel'])?$_POST['branch_tel']:"";
$branch_fax     = isset($_POST['branch_fax'])?$_POST['branch_fax']:"";
$branch_tax     = isset($_POST['branch_tel'])?$_POST['branch_tax']:"";
$branch_open    = isset($_POST['branch_open'])?$_POST['branch_open']:"";
$branch_close   = isset($_POST['branch_close'])?$_POST['branch_close']:"";
$is_active      = isset($_POST['is_active'])?$_POST['is_active']:"";


$user_id_update = $_SESSION['member'][0]['user_id'];

$branch_logo  = "";
$updateImg    = "";

$path = "images/";
if(isset($_FILES["branch_logo"])){
  $branch_logo = resizeImageToBase64($_FILES["branch_logo"],'400','400','100',$user_id_update,$path);

  if(!empty($branch_logo)){
      $updateImg    =  "branch_logo = '$branch_logo',";
  }
}


$sql = "";
// --ADD EDIT DELETE Module-- //
if(empty($branch_id) && $action == 'ADD'){
  $sql = "INSERT INTO t_branch
         (branch_code,cname,branch_name,branch_address,branch_tel,
          branch_fax,branch_tax,branch_open,branch_close,is_active,user_id_update,branch_logo)
         VALUES('$branch_code','$cname','$branch_name','$branch_address','$branch_tel',
         '$branch_fax','$branch_tax','$branch_open','$branch_close','$is_active','$user_id_update','$branch_logo')";
}else if($action == 'EDIT'){
  $sql = "UPDATE t_branch SET
                branch_code = '$branch_code',
                cname = '$cname',
                branch_name = '$branch_name',
                branch_address = '$branch_address',
                branch_tel = '$branch_tel',
                branch_fax = '$branch_fax',
                branch_tax = '$branch_tax',
                branch_open = '$branch_open',
                branch_close = '$branch_close',
                ".$updateImg."
                is_active = '$is_active',
                user_id_update = '$user_id_update'
            WHERE branch_id = '$branch_id'";
}else if($action == 'DEL'){
  $sql   = "DELETE FROM t_branch WHERE branch_id = '$branch_id'";
}
// --ADD EDIT Module-- //
//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}

?>
