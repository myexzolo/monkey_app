<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode    = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$staus_checkin  = isset($_GET['staus_checkin'])?$_GET['staus_checkin']:"";
$person_code    = isset($_GET['person_code'])?$_GET['person_code']:"";
$limit          = isset($_GET['limit'])?$_GET['limit']:"100";
$typePakage     = isset($_GET['typePakage'])?$_GET['typePakage']:"";
$id             = isset($_GET['id'])?$_GET['id']:"";

$order     = isset($_GET['order'])?$_GET['order']:"DESC";

$con = "";

$dateNow = date("Y-m-d");

if($id != ""){
  $con .= " and cp.id = '$id' ";
}

if($typePakage != ""){
  $con .= " and pp.type_package in ($typePakage) ";
}

if($staus_checkin != ""){
  $con .= " and cp.staus_checkin in ($staus_checkin) ";
}

if($person_code != ""){
  $con .= " and cp.person_code = '$person_code' ";
}


$sql = "SELECT
        cp.id,
        cp.checkin_date,
        cp.company_code,
        cp.person_code,
        cp.package_person_id,
        cp.sign_person,
        cp.sign_emp,
        cp.sign_manager,
        cp.trainer_code as trainerCode,
        cp.manager_code as managerCode,
        cp.sign_person_date,
        cp.sign_emp_date,
        cp.sign_manager_date,
        cp.staus_checkin,
        cp.use_pack,
        pp.package_code,
        pp.package_name,
        pp.package_detail,
        pp.reg_no,
        pp.use_package,
        pp.num_use,
        pp.package_unit,
        pp.date_start,
        pp.date_expire,
        pp.max_use,
        pp.package_price,
        pp.package_num,
        pp.package_price_total,
        pp.percent_discount,
        pp.discount,
        pp.vat,
        pp.type_vat,
        pp.net_total,
        pp.notify_num,
        pp.notify_unit,
        pp.type_package,
        pp.status_notify,
        pp.trainer_code,
        pp.trainer_name,
        pp.invoice_code,
        pp.receipt_code,
        pp.status,
        p.COMPANY_CODE,
        p.PERSON_RUNNO,
        p.PERSON_CODE,
        p.PERSON_CODE_INT,
        p.PERSON_CODE_OLD,
        p.PERSON_TITLE,
        p.PERSON_NICKNAME,
        p.PERSON_NAME,
        p.PERSON_LASTNAME,
        p.PERSON_NAME_MIDDLE,
        p.PERSON_TITLE_ENG,
        p.PERSON_NAME_ENG,
        p.PERSON_LASTNAME_ENG,
        p.PERSON_SEX,
        p.PERSON_BIRTH_DATE,
        p.PERSON_CARD_ID,
        p.PERSON_CARD_SDATE,
        p.PERSON_CARD_EDATE,
        p.PERSON_CARD_CREATE_BY,
        p.PERSON_TEL_HOME,
        p.PERSON_TEL_MOBILE,
        p.PERSON_TEL_MOBILE2,
        p.PERSON_TEL_OFFICE,
        p.PERSON_FAX,
        p.PERSON_EMAIL,
        p.PERSON_PRIORITY,
        p.PERSON_TYPE,
        p.PERSON_GROUP,
        p.PERSON_STATUS,
        p.PERSON_REGISTER_DATE,
        p.PERSON_EXPIRE_DATE,
        p.PERSON_NOTE,
        p.PERSON_IMAGE
        FROM trans_checkin_person cp, trans_package_person pp, person p
        where cp.package_person_id = pp.id and cp.person_code = p.PERSON_CODE and p.PERSON_STATUS in ('A') and pp.status not in ('D')
        and cp.company_code ='$companycode' $con ORDER BY cp.checkin_date $order LIMIT 0,$limit";


//echo $sql;
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}

?>
