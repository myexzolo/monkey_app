<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companycode  = isset($_GET['companycode'])?$_GET['companycode']:"GYMMK01";
$companyId    = isset($_GET['companyId'])?$_GET['companyId']:"";
$invoiceDate  = isset($_GET['invoiceDate'])?$_GET['invoiceDate']:"";
$typeVat      = isset($_GET['invoiceDate'])?$_GET['typeVat']:"";


$companyId = sprintf("%02d", $companyId);
$code = $typeVat.$companyId.date("ym",strtotime($invoiceDate));

$sql = "SELECT max(invoice_code) as invoice_code FROM tb_invoice where company_code ='$companycode' and invoice_code like '$code%' ";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];;

$NO = "";
if($dataCount > 0){
  $no       = $row[0]['invoice_code'];
  $lastNum  = substr($no,strlen($code));
  $lastNum  = $lastNum + 1;
  $NO = $code.sprintf("%03d", $lastNum);
}else{
  $NO = $code."001";
}

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $NO)));
?>
