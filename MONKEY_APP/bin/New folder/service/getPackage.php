<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$packageStatus = isset($_GET['packageStatus'])?$_GET['packageStatus']:"";



$sql = "SELECT * FROM data_mas_package where package_status in ($packageStatus)  order by package_type,seq";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else if (intval($errorInfo[0]) == 0 && $dataCount == 0){
  header('Content-Type: application/json');
  exit(json_encode(array()));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
