<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"";

$sql = "SELECT * FROM t_branch WHERE branch_code ='$branchcode' AND is_active ='Y'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  $row[0]['status'] = true;
  header('Content-Type: application/json');
  exit(json_encode($row[0]));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
