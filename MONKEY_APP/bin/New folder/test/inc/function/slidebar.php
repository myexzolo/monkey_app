<?php
  include('connect.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $baseurl = "/pages/";
  $REQUEST_URI = $_SESSION['RE_URI'];

  //print_r($_SESSION['member'][0]);
  $MEMBER = $_SESSION['member'][0];
  // Start Function  //
  $branchOptions = "N";

  $sqlu = "SELECT * FROM t_role WHERE role_id IN ({$MEMBER['role_list']})";
  //echo $sqlu;
  $queryu = DbQuery($sqlu,null);
  $rows   = json_decode($queryu, true);
  $rowu   = $rows['data'];
  $dataCount   = $rows['dataCount'];
  $strPage = '';

  if($dataCount > 0)
  {

    foreach ($rowu as $k => $value)
    {
      if($k == 0){
        $strPage .= $value['page_list'];
      }else{
        $strPage .= ','.$value['page_list'];
      }
      if($value['role_code'] = "HOF"){
        $branchOptions = "Y";
      }
    }

    $sqlb   = "SELECT * FROM t_branch WHERE is_active = 'Y'";
    $queryb = DbQuery($sqlb,null);
    $jsonb  = json_decode($queryb, true);
    $rowb   = $jsonb['data'];
    $dataCount  = $jsonb['dataCount'];
    if($dataCount > 0)
    {
      if($branchOptions == "Y")
      {
      ?>
        <div class="form-group" style="margin:5px;">
          <select class="form-control" onchange="changeBranch()" id="branchCodeSlidebar" >
       <?php
      }
      foreach ($rowb as $k => $value)
      {
        $selected = "";

        if($_SESSION['branchCode'] == $value['branch_code'])
        {
          $selected =  'selected="selected"';
        }
      ?>
          <option value="<?= $value['branch_code'] ?>" <?= $selected?>><?= $value['cname'] ?></option>
      <?php
      }


      if($branchOptions == "Y")
      {
       ?>
        </select>
      </div>
      <?php
      }
    }

  $arrPage = array_unique(explode(",",$strPage));
  $arrPage = implode(",",$arrPage);

  $sqlp = "SELECT * FROM t_page WHERE page_id IN ($arrPage)";
  $queryp = DbQuery($sqlp,null);
  $rows   = json_decode($queryp, true);
  $rowp   = $rows['data'];
  $strModule = '';
  $arr = array();
  $strs = '';
  foreach ($rowp as $k => $value) {
    $arr[$k] = $value['page_path'];
    $strs .= $value['page_path'];
    if($k == 0){
      $strModule .= $value['module_id'];
    }else{
      $strModule .= ','.$value['module_id'];
    }
  }

  $arrModule = array_unique(explode(",",$strModule));
  $arrModule = implode(",",$arrModule);

  $sqlm = "SELECT * FROM t_module WHERE module_id IN ($arrModule) AND module_type = '1'";
  $querym = DbQuery($sqlm,null);
  $rows   = json_decode($querym, true);
  $rowm   = $rows['data'];
  $strRoot = '';
  $arr = array();
  foreach ($rowm as $k => $value) {
    if($k == 0){
      $strRoot .= $value['root_id'];
    }else{
      $strRoot .= ','.$value['root_id'];
    }
  }

  $arrRoot = array_unique(explode(",",$strRoot));
  $arrRoot = implode(",",$arrRoot);



  // check Page Role
  $pagess = substr( str_replace($baseurl,"",$REQUEST_URI),0,-1 );
  $pos = strrpos($strs, $pagess);
  if ($pos === false) {
    //exit("<script>alert('ไม่มีสิทธิ์ใช้งาน');window.history.back();</script>");
  }
  // End Function  //

  $page_path = substr(str_replace($baseurl,'',$REQUEST_URI) , 0,-1);
  $sqlRoot   = "SELECT * FROM t_root WHERE root_id IN ($arrRoot) ORDER BY root_seq ASC";
  $queryRoot = DbQuery($sqlRoot,null);
  $rows      = json_decode($queryRoot, true);
  $rowRoot   = $rows['data'];
  foreach ($rowRoot as $valueRoot)
  {
      $root_id = $valueRoot['root_id'];
?>
<li class="header"><?=$valueRoot['root_name']?></li>

<?php
  $sqlModule   = "SELECT * FROM t_module WHERE root_id = '$root_id' AND is_active = 'Y' AND module_id IN ($arrModule) ORDER BY module_order ASC, update_date DESC";
  $queryModule = DbQuery($sqlModule,null);
  $rows        = json_decode($queryModule, true);
  $rowModule   = $rows['data'];
    foreach ($rowModule as $valueModule)
    {
      $module_name = $valueModule['module_name'];
      $module_id = $valueModule['module_id'];
      $module_icon = $valueModule['module_icon'];

      $sqlPage   = "SELECT * FROM t_page WHERE page_path = '$page_path' AND page_id IN ($arrPage)";
      $queryPage = DbQuery($sqlPage,null);
      $rows      = json_decode($queryPage, true);
      $rowPage   = $rows['data'];
      $module_idPage = @$rowPage[0]['module_id'];
?>
<li class="treeview <?=$module_id==$module_idPage?"active menu-open":""?>">
    <a href="#">
      <i class="<?=$module_icon?>"></i>
      <span><?=$module_name?></span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <?php
        $sqlPage   = "SELECT * FROM t_page WHERE module_id = '$module_id' AND is_active = 'Y' AND page_id IN ($arrPage) ORDER BY page_seq ASC, update_date DESC";
        $queryPage = DbQuery($sqlPage,null);
        $rows      = json_decode($queryPage, true);
        $rowPage   = $rows['data'];
          foreach ($rowPage as $value) {
      ?>
      <li class="<?=$page_path==$value['page_path']?"active":""?>"><a href="../<?=$value['page_path']?>/"><i class="<?=$value['page_icon'];?>"></i><?=$value['page_name']?></a></li>
      <?php } ?>
    </ul>
</li>
<?php
  }
 }
}
?>
