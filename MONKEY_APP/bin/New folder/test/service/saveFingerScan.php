<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$code         = isset($_GET['code'])?$_GET['code']:"";
$companyCode  = isset($_GET['companyCode'])?$_GET['companyCode']:"";
$typeSearch   = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";
$finger_num   = isset($_GET['fingNumber'])?$_GET['fingNumber']:"";
$sTemp        = isset($_GET['sTemp'])?$_GET['sTemp']:"";
$sTempV10     = isset($_GET['sTempV10'])?$_GET['sTempV10']:"";

$table = "";
if($code != ""){
  if($typeSearch == "PERSON")
  {
     $table   = "tb_finger_person";
     $table2  = "person";
     $codetype = "PERSON_CODE";
  }else if ($typeSearch == "EMP")
  {
    $table  = "tb_finger_emp";
    $table2 = "data_mas_employee";
    $codetype = "EMP_CODE";
  }

  $sql = "INSERT INTO $table (code,finger_num, finger_str, finger_pic, finger_binary, finger_v10, finger_detail,COMPANY_CODE)
  VALUES ('$code', '$finger_num', '$sTemp', '', '', '$sTempV10', '','$companyCode')";


   $query      = DbQuery($sql,null);
   $json       = json_decode($query, true);
   $errorInfo  = $json['errorInfo'];
   $row        = $json['data'];
   $dataCount  = $json['dataCount'];

   $fngNum = "";
   if($finger_num == '1'){
     $fngNum = "FNG_TEMPLATE1";
   }else if($finger_num == '2'){
     $fngNum = "FNG_TEMPLATE2";
   }else if($finger_num == '3'){
     $fngNum = "FNG_TEMPLATE3";
   }else if($finger_num == '4'){
     $fngNum = "FNG_TEMPLATE4";
   }else if($finger_num == '5'){
     $fngNum = "FNG_TEMPLATE5";
   }else if($finger_num == '6'){
     $fngNum = "FNG_TEMPLATE6";
   }else if($finger_num == '7'){
     $fngNum = "FNG_TEMPLATE7";
   }else if($finger_num == '8'){
     $fngNum = "FNG_TEMPLATE8";
   }else if($finger_num == '9'){
     $fngNum = "FNG_TEMPLATE9";
   }else if($finger_num == '10'){
     $fngNum = "FNG_TEMPLATE10";
   }

   $sql2 = "update $table2 set $fngNum ='".$sTemp."' where ".$codetype."='".$code."' and COMPANY_CODE='".$companyCode."'";
   $query2      = DbQuery($sql2,null);
   $json2       = json_decode($query2, true);
   $errorInfo2  = $json2['errorInfo'];

   if(intval($errorInfo[0]) == 0 && intval($errorInfo2[0]) == 0){
     header('Content-Type: application/json');
     exit(json_encode(array('status' => true,'message' => 'Success')));
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array('status' => false,'message' => 'Fail sql :'.$sql. " ,sql2 : ".$sql2)));
   }
}else
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}



?>
