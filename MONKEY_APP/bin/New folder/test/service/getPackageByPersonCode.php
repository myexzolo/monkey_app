<?php
header('Access-Control-Allow-Origin: *');
session_start();
date_default_timezone_set("Asia/Bangkok");

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$companyCode  = isset($_GET['companyCode'])?$_GET['companyCode']:"";
$personCode   = isset($_GET['personCode'])?$_GET['personCode']:"";


$sql = "SELECT ps.*, iv.invoice_date,(SELECT COUNT(cp.id) FROM trans_checkin_person cp WHERE cp.person_code = '$personCode' and cp.staus_checkin not in ('D') AND cp.package_person_id = ps.id) as num_checkin
FROM trans_package_person ps, tb_invoice iv
where ps.invoice_code = iv.invoice_code and ps.company_code =  '$companyCode' and iv.company_code =  '$companyCode'
and ps.person_code = '$personCode' and 	ps.status = 'A' and iv.status = 'A'  order by ps.date_expire ASC";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];


if(intval($errorInfo[0]) == 0 && $dataCount > 0){
  header('Content-Type: application/json');
  exit(json_encode($row));
}else if (intval($errorInfo[0]) == 0 && $dataCount == 0){
  header('Content-Type: application/json');
  exit(json_encode(array()));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail'.$sql)));
}

?>
