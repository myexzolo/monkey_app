<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$fingerId     = $_GET['fingerId'];
$typeSearch   = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";
$companyCode  = isset($_GET['companyCode'])?$_GET['companyCode']:"";




if($typeSearch == "PERSON")
{
   $sql = "SELECT * FROM tb_finger_person where finger_id = '".$fingerId."' and COMPANY_CODE ='".$companyCode."'";
}else if ($typeSearch == "EMP")
{
   $sql = "SELECT * FROM tb_finger_emp where finger_id = '".$fingerId."' and COMPANY_CODE ='".$companyCode."'";
}

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];

$userCode   = $row[0]['code'];

$sql = "SELECT u.*, m.DATA_DESC1 as department_name, e.EMP_POSITION
        FROM t_user u, data_master m, data_mas_employee e
        WHERE u.user_login ='$userCode'
        AND u.branch_code = '$companyCode' AND u.department_code = m.DATA_CODE
        and m.DATA_GROUP = 'EMP_DEPARTMENT'
        AND u.user_login = e.EMP_CODE";

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$roleName     = "-";
if(intval($errorInfo[0]) == 0 && $dataCount > 0){

  $date = date("Y-m-d H:i:s");
  $user_id = $row[0]['user_id'];

  $roleName   = getRoleName($row[0]['role_list']);

  $emp_position = getDataMaster('EMP_POSITION',$row[0]['EMP_POSITION']);

  $sql = "INSERT INTO t_log_login (user_id,date_login) VALUES('$user_id','$date')";
  $query    = DbInsert($sql,null);
  $idLog    = json_decode($query, true);


  $row[0]['status'] = true;
  $row[0]['log_id'] = $idLog;
  $row[0]['date_login'] = $date;
  $row[0]['role_access'] = "";
  $row[0]['role_name'] = $roleName;
  $row[0]['emp_position'] = $emp_position;

  header('Content-Type: application/json');
  exit(json_encode($row[0]));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => false,'message' => 'Fail')));
}

?>
