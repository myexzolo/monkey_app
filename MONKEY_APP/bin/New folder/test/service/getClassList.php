<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

FIX_PHP_CORSS_ORIGIN();

$branchcode = isset($_GET['branchcode'])?$_GET['branchcode']:"GYMMK01";

date_default_timezone_set("Asia/Bangkok");
$date = date("Y/m/d");

for($i=0;$i<7; $i++)
{
  $dateStr = date('D d/m/Y',strtotime($date . "+$i days"));

  $row[$i]['dayOfClass'] = $dateStr;
  $subClass[0]['timeOfClass'] = "10:30";
  $subClass[0]['className'] = "NAME CLASS1";
  $subClass[0]['trainerName'] = "Trainer Name";
  $subClass[0]['personJoin'] = "10";
  $subClass[0]['personTotal'] = "20";

  $subClass[1]['timeOfClass'] = "10:30";
  $subClass[1]['className'] = "NAME CLASS1";
  $subClass[1]['trainerName'] = "Trainer Name";
  $subClass[1]['personJoin'] = "10";
  $subClass[1]['personTotal'] = "20";

  $row[$i]['subClass'] = $subClass;
}


header('Content-Type: application/json');
exit(json_encode($row));


?>
