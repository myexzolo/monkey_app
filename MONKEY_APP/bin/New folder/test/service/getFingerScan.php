<?php
header('Access-Control-Allow-Origin: *');
session_start();
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$code       = isset($_GET['code'])?$_GET['code']:"";
$typeSearch = isset($_GET['typeSearch'])?$_GET['typeSearch']:"";
$companyCode  = isset($_GET['companyCode'])?$_GET['companyCode']:"";


$con = "";
if($code != ""){
  if($code == "All"){
    $con = "";
  }else{
    $con = " and code ='$code'";
  }

  if($typeSearch == "PERSON")
  {
     $sql = "SELECT f.* FROM tb_finger_person f, person p
             where f.code = p.PERSON_CODE $con and p.COMPANY_CODE ='".$companyCode."' and p.PERSON_STATUS in ('A','Y') and f.COMPANY_CODE ='".$companyCode."'
             order by f.code";
  }else if ($typeSearch == "EMP")
  {
     $sql = "SELECT f.* FROM tb_finger_emp f, data_mas_employee e
             where f.code = e.EMP_CODE and e.COMPANY_CODE ='".$companyCode."' and e.EMP_DATE_RETRY  is null and f.COMPANY_CODE ='".$companyCode."' $con
             order by code";
  }


   $query      = DbQuery($sql,null);
   $json       = json_decode($query, true);
   $errorInfo  = $json['errorInfo'];
   $row        = $json['data'];
   $dataCount  = $json['dataCount'];

   if(intval($errorInfo[0]) == 0 && $dataCount > 0){
     header('Content-Type: application/json');
     exit(json_encode($row));
   }else{
     header('Content-Type: application/json');
     exit(json_encode(array()));
   }
}else
{
  header('Content-Type: application/json');
  exit(json_encode(array()));
}



?>
