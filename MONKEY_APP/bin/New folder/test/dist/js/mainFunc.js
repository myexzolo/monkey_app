$( document ).ajaxStart(function() {
  $(".loadingImg").removeClass('none');
});

$( document ).ajaxStop(function() {
  $(".loadingImg").addClass('none');
});

function showProcessbar(){
  // $.smkProgressBar({
  //   element:'body',
  //   status:'start',
  //   bgColor: '#000',
  //   barColor: '#fff',
  //   content: 'Loading...'
  // });
  // setTimeout(function(){
  //   $.smkProgressBar({
  //     status:'end'
  //   });
  // }, 1000);
}

function GenPass(type=0,num=8) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  if(type == 0){
    text = 'P@ss1234';
  }else{
    for (var i = 0; i < num; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
  }
  return text;
}

function showSlidebar(){
  $.get( "../../inc/function/slidebar.php" )
  .done(function( data ) {
    $("#showSlidebar").html(data);
  });
}

function changeBranch(){
  var branchCode = $("#branchCodeSlidebar").val();
  $.post("../../inc/function/setbranch.php",{branchCode:branchCode})
    .done(function( data ) {
        postURL("../../pages/home/", null);
  });
}

function postURL(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function readURL(input,values) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img width='100'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}
