﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_main : Form
    {
        public string branchCode     = System.Configuration.ConfigurationManager.AppSettings["BranchCode"];
        public string TopMostDisplay = System.Configuration.ConfigurationManager.AppSettings["TopMost"];


        public static frm_customer frmCus = null;
        public static frm_emp frmEmp = null;
        public static frm_packege frmPackege  = null;
        public static frm_packege_cus frmPackegeCus = null;
        public static frm_checkin frmCheckin = null;
        public static frm_trainer frmTrainer = null;
        public static frm_report frmReport = null;
        public static frm_home frmHome = null;

        bool fc = true;
        DialogResult result;



        public frm_main()
        {
            index inx = new index();
            result = inx.ShowDialog();
            InitializeComponent();

            if (TopMostDisplay.Equals("Y")){
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi                 = FileVersionInfo.GetVersionInfo(assembly.Location);

            labVersions.Text = fvi.FileVersion;
            if (result == DialogResult.Cancel)
            {
                this.Close();
            }
            else
            {
                frm_login login = new frm_login();
                login.ShowDialog();

                if (!Global.USER.user_id.Equals(""))
                {
                    nameUser.Text = Global.USER.user_name + " " + Global.USER.user_last;

                    var thread1 = new Thread(loadMaster1);
                    var thread2 = new Thread(loadMaster2);
                    var thread3 = new Thread(loadMaster3);
                    var thread4 = new Thread(loadMaster4);

                    thread1.Start();
                    thread2.Start();
                    thread3.Start();
                    thread4.Start();
                }
                btnhome();
            }
            
        }

        private async void loadMaster1()
        {
            
            try
            {
                ApiRest.InitailizeClient();
                Global.ProvinceList = await ApiProcess.getProvince();
                Global.DistrictList = await ApiProcess.getDistrict();
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: loadMaster1 ::" + ex.ToString(), "frm_main");

            }
        }


        private async void loadMaster2()
        {
            try
            {
                ApiRest.InitailizeClient();
                Global.PACKAGE_STATUS = await ApiProcess.getMesterDataGroupList("PACKAGE_STATUS");
                Global.PAY_METHOD = await ApiProcess.getMesterDataGroupList("PAY_METHOD");
                Global.PAYMENT_FUNC = await ApiProcess.getMesterDataGroupList("PAYMENT_FUNC");
                Global.TrainerList = await ApiProcess.getTrainers();
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: loadMaster2 ::" + ex.ToString(), "frm_main");

            }


        }

        private async void loadMaster3()
        {
            try
            {
                ApiRest.InitailizeClient();
                Global.EmployeeList = await ApiProcess.getEmployee("ACTIVE");
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: loadMaster3 ::" + ex.ToString(), "frm_main");

            }
            
        }

        private async void loadMaster4()
        {
            try
            {
                ApiRest.InitailizeClient();
                Global.SubdistrictList = await ApiProcess.getSubdistrict();
                //Console.WriteLine("Load SubdistrictList Seccess");
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: loadMaster4 ::" + ex.ToString(), "frm_main");

            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            labDate.Text = Utils.getDateTh();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void hoverButton(PictureBox obj, string sts)
        {
            if (sts.Equals("H"))
            {
                obj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            }
            else
            {
                obj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            }
        }
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void changeResizeFrom()
        {
            if (this.WindowState == FormWindowState.Minimized) {
                this.WindowState = FormWindowState.Maximized;
            }
            else if (this.WindowState == FormWindowState.Maximized) {
                this.WindowState = FormWindowState.Normal;
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            changeResizeFrom();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            changeResizeFrom();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frm_dialog d = new frm_dialog("ยืนยันการออกจากระบบ !","");
            d.ShowDialog();

            if (d.DialogResult == DialogResult.OK)
            {
                this.Close();
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            btnhome();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm_logout lg = new frm_logout();
              
            var result = lg.ShowDialog();
            if (result == DialogResult.OK)
            {
                Global.USER = null;
                Global.MENULIST = null;
   
                frm_login login = new frm_login();
                login.ShowDialog();

                if (!Global.USER.user_id.Equals(""))
                {
                    nameUser.Text = Global.USER.user_name + " " + Global.USER.user_last;

                }
                btnhome();
            }
        }

        private void btnhome()
        {
            fc = true;
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }

            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }

            if (frmHome == null)
            {
                frmHome = new frm_home();
                frmHome.TopLevel = false;
                frmHome.AutoScroll = true;
                this.panel3.Controls.Add(frmHome);
                frmHome.Show();
            }






            this.pictureBox1.BackColor  = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor  = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor  = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor  = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor       = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }

        private void btnEmp_Click(object sender, EventArgs e)
        {
            fc = true;
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));

            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmEmp == null)
            {
                frmEmp = new frm_emp();
                frmEmp.TopLevel = false;
                frmEmp.AutoScroll = true;
                this.panel3.Controls.Add(frmEmp);
                frmEmp.Show();
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }
            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }

            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }
        }

        private void btnRegPerson_Click(object sender, EventArgs e)
        {
            fc = true;
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));

            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmCus == null)
            {
                frmCus = new frm_customer(this);
                frmCus.TopLevel = false;
                frmCus.AutoScroll = true;
                this.panel3.Controls.Add(frmCus);
                frmCus.Show();
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }
            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }
        }
        public string PersonCode = "";
        private void btnCheckin_Click(object sender, EventArgs e)
        {
            fc = true;
            if (frmCheckin == null)
            {
                frmCheckin = new frm_checkin(PersonCode);
                PersonCode = "";
                frmCheckin.TopLevel = false;
                frmCheckin.AutoScroll = true;
                this.panel3.Controls.Add(frmCheckin);
                frmCheckin.Show();
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }
            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }

            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }

        private void btnTrainer_Click(object sender, EventArgs e)
        {
            fc = true;
            if (frmTrainer == null)
            {
                frmTrainer = new frm_trainer();
                frmTrainer.TopLevel = false;
                frmTrainer.AutoScroll = true;
                this.panel3.Controls.Add(frmTrainer);
                frmTrainer.Show();
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }


            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }


        private void btnReport_Click(object sender, EventArgs e)
        {
            fc = true;
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }

            if (frmTrainer == null)
            {
                frmReport = new frm_report();
                frmReport.TopLevel = false;
                frmReport.AutoScroll = true;
                this.panel3.Controls.Add(frmReport);
                frmReport.Show();
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }



            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }

        private void btnPackage_Click(object sender, EventArgs e)
        {
            fc = true;
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackegeCus != null)
            {
                frmPackegeCus.Close();
                frmPackegeCus = null;
                this.panel3.Controls.Remove(frmPackegeCus);
            }
            if (frmPackege == null)
            {
                frmPackege = new frm_packege();
                frmPackege.TopLevel = false;
                frmPackege.AutoScroll = true;
                this.panel3.Controls.Add(frmPackege);
                frmPackege.Show();
            }
            if (frmCheckin != null)
            {
                frmCheckin.Close();
                frmCheckin = null;
                this.panel3.Controls.Remove(frmCheckin);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }
            if (frmReport != null)
            {
                frmReport.Close();
                frmReport = null;
                this.panel3.Controls.Remove(frmReport);
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }

            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));

            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }

        public void btnBuyPackage()
        {
            fc = false;
            btnBuyPack_Click(null, null);
        }

        public void btnCheckinPerson()
        {
            fc = false;
            btnCheckin_Click(null, null);
        }

        private void btnBuyPack_Click(object sender, EventArgs e)
        {
            if (frmCus != null)
            {
                frmCus.Close();
                frmCus = null;
                this.panel3.Controls.Remove(frmCus);
            }
            if (frmEmp != null)
            {
                frmEmp.Close();
                frmEmp = null;
                this.panel3.Controls.Remove(frmEmp);
            }
            if (frmPackege != null)
            {
                frmPackege.Close();
                frmPackege = null;
                this.panel3.Controls.Remove(frmPackege);
            }
            if (frmTrainer != null)
            {
                frmTrainer.Close();
                frmTrainer = null;
                this.panel3.Controls.Remove(frmTrainer);
            }
            if (frmPackegeCus == null)
            {
                if (fc)
                {
                    Global.PERSON = new Person();
                }
               
                frmPackegeCus = new frm_packege_cus();
                frmPackegeCus.TopLevel = false;
                frmPackegeCus.AutoScroll = true;
                this.panel3.Controls.Add(frmPackegeCus);
                frmPackegeCus.Show();
            }
            if (frmHome != null)
            {
                frmHome.Close();
                frmHome = null;
                this.panel3.Controls.Remove(frmHome);
            }

            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.picEmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
