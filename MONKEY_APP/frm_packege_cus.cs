﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_packege_cus : Form
    {

        public frm_packege_cus()
        {
            InitializeComponent();
        }

        private DataTable dt;
        //private DataTable dt2;
        //private DataView dv;

        private Invoice invoice;

        private PackagePersons packagePersons;
        private string typeActive; 

        Package PackageMas = new Package();

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or รายละเอียด like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dt.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or รายละเอียด like '%{0}%'", txtSearch.Text);
                dataGridView1.DataSource = dv.ToTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }


        private void frm_customer_Load(object sender, EventArgs e)
        {
            dataGridView2.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridView2.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridView2.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView2.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dataGridView2.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView2.Columns[15].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            if (Global.PERSON != null)
            {
                labName.Text = Global.PERSON.PERSON_NAME;
                labCode.Text = Global.PERSON.PERSON_CODE;
                labNickname.Text = Global.PERSON.PERSON_NICKNAME;
                if (Global.PERSON.PERSON_IMAGE != null && !Global.PERSON.PERSON_IMAGE.Equals(""))
                {
                    Utils.setImage(Global.PERSON.PERSON_IMAGE, picPerson);
                }
                else
                {
                    picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                    picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                }

                if (Global.PERSON.PERSON_STATUS != null && Global.PERSON.PERSON_STATUS.Equals("A"))
                {
                    labStatus.Text = "Member";
                }
                else if (Global.PERSON.PERSON_STATUS != null && Global.PERSON.PERSON_STATUS.Equals("Y"))
                {
                    labStatus.Text = "สมาชิกใหม่";
                }
                else {
                    labStatus.Text = "";
                }
            }
            else
            {
                labName.Text = "";
                labCode.Text = "";
                labNickname.Text = "";
                labStatus.Text = "";

                picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            }
            

            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight =  Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth;
       
            int panelM  = panelw1 / 100 * 75;

            panelList.Width = panelM;
            panelList.Height = scrHeight - 205;


            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("7"))
                {
                    if (code.Equals("AINV"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.folder__2_;
                    }
                    else if (code.Equals("EINV"))
                    {
                        width = 160;
                        img = global::MONKEY_APP.Properties.Resources.folder2;
                    }
                    else if (code.Equals("DINV"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.del;
                    }
                    else if (code.Equals("VINV"))
                    {
                        width = 180;
                        img = global::MONKEY_APP.Properties.Resources.eye2;
                    }

                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));
                    num++;
                }

            }
            tableLayoutPanel.ColumnCount = buttons.Count;

            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }

            txtSearch.Focus();
            getPackage("'Y'");
        }

        private void getPackage(string typeSearch)
        {

            try
            {
                dt = new DataTable();

                dt.Columns.Add("รหัส", typeof(string));
                dt.Columns.Add("ชื่อ", typeof(string));
                dt.Columns.Add("ราคา", typeof(string));
                dt.Columns.Add("รายละเอียด", typeof(string));
                dt.Columns.Add("ประเภทแพคเกจ", typeof(string));
                dt.Columns.Add("ระยะเวลา", typeof(string));
                dt.Columns.Add("หน่วย", typeof(string));
                dt.Columns.Add("อายุการใช้งาน", typeof(string));
                dt.Columns.Add("ภาษีมูลค่าเพิ่ม", typeof(string));
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("notify_num", typeof(string));
                dt.Columns.Add("notify_unit", typeof(string));

                for (int i = 0; i < Global.PackageList.Count(); i++)
                {
                    string masUnit  = "";
                    //string masVat   = "";
                    //string masType   = "";

                    Package package = Global.PackageList[i];

                    for (int y = 0; y < Global.TIME_UNIT.Count; y++)
                    {
                        if (Global.TIME_UNIT[y].DATA_GROUP == "TIME_UNIT" && Global.TIME_UNIT[y].DATA_CODE == package.package_unit)
                        {
                            masUnit = Global.TIME_UNIT[y].DATA_DESC1;
                            break;
                        }
                    }

                    //for (int y = 0; y < Global.VAT_TYPE.Count; y++)
                    //{
                    //    if (Global.VAT_TYPE[y].DATA_GROUP == "VAT_TYPE" && Global.VAT_TYPE[y].DATA_CODE == package.type_vat)
                    //    {
                    //        masVat = Global.VAT_TYPE[y].DATA_DESC2;
                    //        break;
                    //    }
                    //}

                    //if (package.package_type == "MB")
                    //{
                    //    masType = "Member";
                    //}
                    //else if (package.package_type == "PT")
                    //{
                    //    masType = "Personal Trainer";
                    //}


                    string numPrice = String.Format("{0:n}", package.package_price);
                    string numUse = String.Format("{0}", package.num_use);
                    string maxUse = String.Format("{0}", package.max_use);


                    dt.Rows.Add(package.package_code,package.package_name,
                         numPrice, package.package_detail, package.package_type,
                         numUse,masUnit, maxUse,package.type_vat, 
                         package.package_id,package.notify_num, package.notify_unit
                    );
                }

                dataGridView1.DataSource = dt;

                dataGridView1.Columns[0].Width = 75;
                dataGridView1.Columns[1].Width = 205;
                dataGridView1.Columns[2].Width = 95;

                dataGridView1.Columns[3].Visible = false;
                dataGridView1.Columns[4].Visible = false;
                dataGridView1.Columns[5].Visible = false;
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Columns[7].Visible = false;
                dataGridView1.Columns[8].Visible = false;
                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[11].Visible = false;


                dataGridView1.Columns[2].DefaultCellStyle.Alignment     = DataGridViewContentAlignment.MiddleRight;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: getPackage ::" + ex.ToString(), "frm_packege_cus");
            }
        }


        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width) {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

            column.HeaderText = HeaderText;
            column.Name = Name;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.Width = Width;

            return column;
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = menuName;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }

        private void calprice()
        {
            try
            {
                int num = dataGridView2.Rows.Count;

                if (num > 0)
                {
                    button2.Enabled = true;
                    button1.Enabled = true;
                }
                else
                {
                    button2.Enabled = false;
                    button1.Enabled = false;
                }

                decimal price = 0;

                packagePersons = new PackagePersons();
                packagePersons.PackagePersonList = new List<PackagePerson>();
                invoice = new Invoice();

                decimal totalPrice = 0;
                decimal discount = 0;
                decimal vat = 0;

                for (int i = 0; i < num; i++)
                {
                    PackagePerson ps = new PackagePerson();

                    ps.person_code = dataGridView2.Rows[i].Cells[1].Value.ToString();
                    ps.reg_no = dataGridView2.Rows[i].Cells[2].Value.ToString();
                    ps.package_name = dataGridView2.Rows[i].Cells[3].Value.ToString();
                    ps.package_price = dataGridView2.Rows[i].Cells[4].Value.ToString().Replace(",", "");
                    ps.package_num = int.Parse(dataGridView2.Rows[i].Cells[5].Value.ToString());
                    ps.package_price_total = dataGridView2.Rows[i].Cells[6].Value.ToString().Replace(",", "");
                    ps.discount = dataGridView2.Rows[i].Cells[7].Value.ToString().Replace(",", "");
                    //ps.vat = dataGridView2.Rows[i].Cells[8].Value.ToString().Replace(",", "");
                    ps.vat = "0";
                    ps.net_total = dataGridView2.Rows[i].Cells[9].Value.ToString().Replace(",", "");
                    ps.num_use = int.Parse(dataGridView2.Rows[i].Cells[10].Value.ToString());
                    ps.package_unit = dataGridView2.Rows[i].Cells[11].Value.ToString();
                    ps.date_start = dataGridView2.Rows[i].Cells[28].Value.ToString();
                    ps.date_expire = dataGridView2.Rows[i].Cells[29].Value.ToString();
                    ps.max_use = int.Parse(dataGridView2.Rows[i].Cells[14].Value.ToString());
                    ps.type_package = dataGridView2.Rows[i].Cells[15].Value.ToString();
                    ps.trainer_name = dataGridView2.Rows[i].Cells[16].Value.ToString();
                    ps.percent_discount = int.Parse(dataGridView2.Rows[i].Cells[17].Value.ToString());
                    ps.trainer_code = dataGridView2.Rows[i].Cells[18].Value.ToString();
                    ps.id = dataGridView2.Rows[i].Cells[19].Value.ToString();
                    ps.package_detail   = dataGridView2.Rows[i].Cells[20].Value.ToString();
                    ps.package_code     = dataGridView2.Rows[i].Cells[21].Value.ToString();
                    ps.notify_num       = int.Parse(dataGridView2.Rows[i].Cells[22].Value.ToString());
                    ps.notify_unit      = dataGridView2.Rows[i].Cells[23].Value.ToString();
                    ps.type_buy         = dataGridView2.Rows[i].Cells[26].Value.ToString();
                    ps.sale_code        = dataGridView2.Rows[i].Cells[27].Value.ToString();

                    ps.company_code = Global.BRANCH.branch_code;
                    ps.invoice_code  = "";
                    
                    ps.receiptCode  = "";

                    totalPrice  += decimal.Parse(ps.package_price_total);
                    discount    += decimal.Parse(ps.discount);
                    vat         += decimal.Parse(ps.vat);
                    price       += decimal.Parse(ps.net_total);

                    packagePersons.PackagePersonList.Add(ps);
                }

                
                invoice.total_price = String.Format("{0:n}", totalPrice);
                invoice.discount    = String.Format("{0:n}", discount);
                invoice.vat         = String.Format("{0:n}", vat);
                invoice.total_net   = String.Format("{0:n}", price);
                invoice.typeActive  = typeActive;

                labTotal.Text = String.Format("{0:n}", price);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: calprice ::" + ex.ToString(), "frm_packege_cus");
            }
        }

        private void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();

            if (code.Equals("AINV"))
            {
                int num = dataGridView2.Rows.Count;
                if (PackageMas.package_code == null)
                {
                    frm_dialog d = new frm_dialog("กรุณาเลือก Package\r\n", "");
                    d.ShowDialog();
                    txtSearch.Focus();
                }
                else if (num < 3)
                {
                    if (labName.Text.Equals(""))
                    {
                        button4_Click(null, null);
                    }

                    if (!labName.Text.Equals(""))
                    {
                        typeActive = "ADD";
                        PackagePerson ps = new PackagePerson();
                        ps.sale_code = Global.PERSON.EMP_CODE_SALE;
                        frm_manage_inv fmi = new frm_manage_inv("ADD", PackageMas, ps);
                        var result = fmi.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            try
                            {
                                ps = fmi.packagePerson;

                                dataGridView2.Rows.Add(num + 1, ps.person_code, ps.reg_no,
                                    ps.package_name, ps.package_price, ps.package_num,
                                    ps.package_price_total, ps.discount, ps.vat,
                                    ps.net_total, ps.num_use, ps.package_unit,
                                    ps.date_start_txt, ps.date_expire_txt, ps.max_use,
                                    ps.type_package, ps.trainer_name, ps.percent_discount,
                                    ps.trainer_code, ps.id, ps.package_detail,
                                    ps.package_code, ps.notify_num, ps.notify_unit, ps.type_vat, 
                                    ps.package_id,ps.type_buy,ps.sale_code,
                                    ps.date_start, ps.date_expire, ps.type_package);

                                calprice();

                                dataGridView1.ClearSelection();
                                PackageMas = new Package();

                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex);
                                Utils.getErrorToLog(":: buttonClick ::" + ex.ToString(), "frm_packege_cus");
                            }
                        }
                    }
                }
                else
                {
                    frm_dialog d = new frm_dialog("เพิ่มรายการได้เพียง 3 รายการ", "I");
                    d.ShowDialog();
                }

            }
            else if (code.Equals("EINV"))
            {
                if (!labName.Text.Equals(""))
                {
                    //typeActive = "EDIT";
                    int inx = 0;

                    PackagePerson pp = new PackagePerson();

                    if (dataGridView2.CurrentCell != null)
                    {

                        inx = dataGridView2.CurrentCell.RowIndex;

                        pp.person_code = dataGridView2.Rows[inx].Cells[1].Value.ToString();
                        pp.reg_no = dataGridView2.Rows[inx].Cells[2].Value.ToString();
                        pp.package_name = dataGridView2.Rows[inx].Cells[3].Value.ToString();
                        pp.package_price = dataGridView2.Rows[inx].Cells[4].Value.ToString().Replace(",", "");
                        pp.package_num = int.Parse(dataGridView2.Rows[inx].Cells[5].Value.ToString());
                        pp.package_price_total = dataGridView2.Rows[inx].Cells[6].Value.ToString().Replace(",", "");
                        pp.discount = dataGridView2.Rows[inx].Cells[7].Value.ToString().Replace(",", "");
                        //ps.vat = dataGridView2.Rows[i].Cells[8].Value.ToString().Replace(",", "");
                        pp.vat = "0";
                        pp.net_total = dataGridView2.Rows[inx].Cells[9].Value.ToString().Replace(",", "");
                        pp.num_use = int.Parse(dataGridView2.Rows[inx].Cells[10].Value.ToString());
                        pp.package_unit = dataGridView2.Rows[inx].Cells[11].Value.ToString();
                        pp.date_start = dataGridView2.Rows[inx].Cells[28].Value.ToString();
                        pp.date_expire = dataGridView2.Rows[inx].Cells[29].Value.ToString();
                        pp.max_use = int.Parse(dataGridView2.Rows[inx].Cells[14].Value.ToString());
                        pp.type_package = dataGridView2.Rows[inx].Cells[15].Value.ToString();
                        pp.trainer_name = dataGridView2.Rows[inx].Cells[16].Value.ToString();
                        pp.percent_discount = int.Parse(dataGridView2.Rows[inx].Cells[17].Value.ToString());
                        pp.trainer_code = dataGridView2.Rows[inx].Cells[18].Value.ToString();
                        pp.id = dataGridView2.Rows[inx].Cells[19].Value.ToString();
                        pp.package_detail = dataGridView2.Rows[inx].Cells[20].Value.ToString();
                        pp.package_code = dataGridView2.Rows[inx].Cells[21].Value.ToString();
                        pp.notify_num   = int.Parse(dataGridView2.Rows[inx].Cells[22].Value.ToString());
                        pp.notify_unit  = dataGridView2.Rows[inx].Cells[23].Value.ToString();
                        pp.type_vat     = dataGridView2.Rows[inx].Cells[24].Value.ToString();
                        pp.package_id   = dataGridView2.Rows[inx].Cells[25].Value.ToString();
                        pp.type_buy     = dataGridView2.Rows[inx].Cells[26].Value.ToString();
                        pp.sale_code    = dataGridView2.Rows[inx].Cells[27].Value.ToString();
                        string package_type = dataGridView2.Rows[inx].Cells[30].Value.ToString();



                        PackageMas.package_type = package_type;
                        PackageMas.notify_num   = int.Parse(dataGridView2.Rows[inx].Cells[22].Value.ToString());
                        PackageMas.notify_unit  = dataGridView2.Rows[inx].Cells[23].Value.ToString();
                        PackageMas.type_vat     = dataGridView2.Rows[inx].Cells[24].Value.ToString();
                        PackageMas.package_id   = dataGridView2.Rows[inx].Cells[25].Value.ToString();

                    }

                    frm_manage_inv fmi = new frm_manage_inv("EDIT", PackageMas, pp);
                    var result = fmi.ShowDialog();
                    if (result == DialogResult.OK)
                    {

                        PackagePerson ps = fmi.packagePerson;

                        dataGridView2.Rows[inx].Cells[1].Value  = ps.person_code;
                        dataGridView2.Rows[inx].Cells[2].Value  = ps.reg_no;
                        dataGridView2.Rows[inx].Cells[3].Value  = ps.package_name;
                        dataGridView2.Rows[inx].Cells[4].Value  = ps.package_price;
                        dataGridView2.Rows[inx].Cells[5].Value  = ps.package_num;
                        dataGridView2.Rows[inx].Cells[6].Value  = ps.package_price_total;
                        dataGridView2.Rows[inx].Cells[7].Value  = ps.discount;
                        dataGridView2.Rows[inx].Cells[8].Value  = ps.vat;
                        dataGridView2.Rows[inx].Cells[9].Value  = ps.net_total;
                        dataGridView2.Rows[inx].Cells[10].Value = ps.num_use;
                        dataGridView2.Rows[inx].Cells[11].Value = ps.package_unit;
                        dataGridView2.Rows[inx].Cells[12].Value = ps.date_start_txt;
                        dataGridView2.Rows[inx].Cells[13].Value = ps.date_expire_txt;
                        dataGridView2.Rows[inx].Cells[14].Value = ps.max_use;
                        dataGridView2.Rows[inx].Cells[15].Value = ps.type_package;
                        dataGridView2.Rows[inx].Cells[16].Value = ps.trainer_name;
                        dataGridView2.Rows[inx].Cells[17].Value = ps.percent_discount;
                        dataGridView2.Rows[inx].Cells[18].Value = ps.trainer_code;
                        dataGridView2.Rows[inx].Cells[19].Value = ps.id;
                        dataGridView2.Rows[inx].Cells[20].Value = ps.package_detail;
                        dataGridView2.Rows[inx].Cells[21].Value = ps.package_code;
                        dataGridView2.Rows[inx].Cells[22].Value = ps.notify_num;
                        dataGridView2.Rows[inx].Cells[23].Value = ps.notify_unit;
                        dataGridView2.Rows[inx].Cells[24].Value = ps.type_vat;
                        dataGridView2.Rows[inx].Cells[26].Value = ps.type_buy;
                        dataGridView2.Rows[inx].Cells[27].Value = ps.sale_code;
                        dataGridView2.Rows[inx].Cells[28].Value = ps.date_start;
                        dataGridView2.Rows[inx].Cells[29].Value = ps.date_expire;
                        dataGridView2.Rows[inx].Cells[30].Value = ps.type_package;

                        //dataGridView2.Rows.RemoveAt(inx);
                        //int num = dataGridView2.Rows.Count;

                        //PackagePerson ps = fmi.packagePerson;




                        //dataGridView2.Rows.Add(num + 1, ps.person_code, ps.reg_no,
                        //    ps.package_name, ps.package_price, ps.package_num,
                        //    ps.package_price_total, ps.discount, ps.vat,
                        //    ps.net_total, ps.num_use, ps.package_unit,
                        //    Utils.getDateEntoTh(ps.date_start), Utils.getDateEntoTh(ps.date_expire), ps.max_use,
                        //    ps.type_package, ps.trainer_name, ps.percent_discount,
                        //    ps.trainer_code, ps.id, ps.package_detail,
                        //    ps.package_code, ps.notify_num, ps.notify_unit, ps.type_vat);

                        calprice();

                        dataGridView1.ClearSelection();
                        PackageMas = new Package();
                    }
                }
            }
            else if (code.Equals("VINV"))
            {
                try
                {
                    frm_history_inv fmi = new frm_history_inv();
                    var result = fmi.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        ;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                }
                
            }
            else if (code.Equals("DINV"))
            {
                typeActive = "DINV";
                foreach (DataGridViewRow row in dataGridView2.SelectedRows)
                {
                    dataGridView2.Rows.RemoveAt(row.Index);
                    int num = dataGridView2.Rows.Count;
                    for (int i = 0; i < num; i++)
                    {
                        dataGridView2.Rows[i].Cells[0].Value = (i + 1);
                    }
                    calprice();
                    dataGridView1.ClearSelection();
                    PackageMas = new Package();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            if (dataGridView1.CurrentCell != null) {

                int rowindex = dataGridView1.CurrentCell.RowIndex;

                PackageMas.package_id      = dataGridView1.Rows[rowindex].Cells[9].Value.ToString();
                PackageMas.package_code    = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
                PackageMas.package_name    = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                PackageMas.package_price   = Int32.Parse(dataGridView1.Rows[rowindex].Cells[2].Value.ToString().Replace(",", "").Replace(".00", ""));
                PackageMas.package_detail  = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();
                PackageMas.package_type    = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                PackageMas.num_use          = Int32.Parse(dataGridView1.Rows[rowindex].Cells[5].Value.ToString().Replace(",", ""));
                PackageMas.package_unit     = dataGridView1.Rows[rowindex].Cells[6].Value.ToString();
                PackageMas.max_use          = Int32.Parse(dataGridView1.Rows[rowindex].Cells[7].Value.ToString().Replace(",", ""));
                PackageMas.type_vat         = dataGridView1.Rows[rowindex].Cells[8].Value.ToString();
                PackageMas.notify_num       = Int32.Parse(dataGridView1.Rows[rowindex].Cells[10].Value.ToString());
                PackageMas.notify_unit      = dataGridView1.Rows[rowindex].Cells[11].Value.ToString();
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frm_cus fmc = new frm_cus();
            var result = fmc.ShowDialog();
            if (result == DialogResult.OK)
            {
                dataGridView2.Rows.Clear();
                labName.Text = Global.PERSON.PERSON_NAME;
                labCode.Text = Global.PERSON.PERSON_CODE;
                labNickname.Text = Global.PERSON.PERSON_NICKNAME;
                if (Global.PERSON.PERSON_IMAGE != null && !Global.PERSON.PERSON_IMAGE.Equals(""))
                {
                    Utils.setImage(Global.PERSON.PERSON_IMAGE, picPerson);
                }
                else
                {
                    picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                    picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                }

                if (Global.PERSON.PERSON_STATUS.Equals("A"))
                {
                    labStatus.Text = "Member";
                }
                else
                {
                    labStatus.Text = "สมาชิกใหม่";
                }

            }
        }

        private void panelList_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int num = dataGridView2.Rows.Count;
            if (num > 0)
            {
                dataGridView2.Rows.Clear();
                calprice();
            }
            
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }
        int startPage = 0;
        int endPage = 50;
        Boolean PageNext = false;

        private async void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var thread1 = new Thread(showloading);
                thread1.Start();
                calprice();
                try
                {
                    Global.PERSON = await ApiProcess.getPersonById(Global.BRANCH.branch_code, Global.PERSON.PERSON_CODE);
                    invoice.name = Global.PERSON.PERSON_BILL_NAME;
                    if (invoice.name.Equals(""))
                    {
                        invoice.name = Global.PERSON.PERSON_TITLE + Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME;
                    }
                }
                catch (Exception ex)
                {
                    frm_dialog d = new frm_dialog("ไม่สามารถเชื่อมต่อ Server ได้!!", "I");
                    d.ShowDialog();
                    Utils.getErrorToLog(":: button2_Click Service ::" + ex.ToString(), "frm_packege_cus");
                    this.Close();
                }


                string a2 = Global.PERSON.PERSON_BILL_ADDR2;
                if (a2 != null && !a2.Equals(""))
                {
                    a2 = " " + a2;
                }

                invoice.address = Global.PERSON.PERSON_BILL_ADDR1 + a2 +
                                  " " + Global.PERSON.PERSON_BILL_SUBDISTRICT +
                                  Environment.NewLine + Global.PERSON.PERSON_BILL_DISTRICT +
                                  " " + Global.PERSON.PERSON_BILL_PROVINCE +
                                  " " + Global.PERSON.PERSON_BILL_POSTAL;


                invoice.tel = Global.PERSON.PERSON_TEL_OFFICE;
                invoice.fax = Global.PERSON.PERSON_FAX;
                invoice.tax = Global.PERSON.PERSON_BILL_TAXNO;

                if (invoice.address.Equals("")) {
                    string a22 = Global.PERSON.PERSON_HOME1_ADDR2;
                    if (a22 != null && !a22.Equals(""))
                    {
                        a22 = " " + a22;
                    }

                    invoice.address = Global.PERSON.PERSON_HOME1_ADDR1 + a22 +
                                      " " + Global.PERSON.PERSON_HOME1_SUBDISTRICT +
                                      Environment.NewLine + Global.PERSON.PERSON_HOME1_DISTRICT +
                                      " " + Global.PERSON.PERSON_HOME1_PROVINCE +
                                      " " + Global.PERSON.PERSON_HOME1_POSTAL;
                }

                picLoading.Visible = false;

                frm_invoice fmi = new frm_invoice(invoice, packagePersons);
                var result = fmi.ShowDialog();

                bool flag = true;
                string message = "";
                if (result == DialogResult.OK)
                {
                    var thread2 = new Thread(showloading);
                    thread2.Start();
                    Invoice inv = fmi.invoice;
                    string typeVat      = fmi.typeVat;
                    string dateinv      = fmi.dateinv;
                    string flagPrint    = fmi.flagPrint;
                    string typePaymentTxt = fmi.typePaymentTxt;

                    int num = dataGridView2.Rows.Count;
                    
                    try
                    {
                        ApiRest.InitailizeClient();
                        Response res = await ApiProcess.genInvNo(Global.BRANCH.branch_code, Global.BRANCH.branch_id, invoice.invoice_date, invoice.type_vat);
                        invoice.invoice_code = res.message;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        Utils.getErrorToLog(":: genInvNo ::" + ex.ToString(), "frm_invoice");
                    }

                    string invoiceCode = invoice.invoice_code;

                    for (int i = 0; i < num; i++)
                    {
                        PackagePerson ps = new PackagePerson();

                        ps.person_code = dataGridView2.Rows[i].Cells[1].Value.ToString();
                        ps.reg_no = dataGridView2.Rows[i].Cells[2].Value.ToString();
                        ps.package_name = dataGridView2.Rows[i].Cells[3].Value.ToString();
                        ps.package_price = dataGridView2.Rows[i].Cells[4].Value.ToString().Replace(",", "");
                        ps.package_num = int.Parse(dataGridView2.Rows[i].Cells[5].Value.ToString());
                        ps.package_price_total = dataGridView2.Rows[i].Cells[6].Value.ToString().Replace(",", "");
                        ps.discount = dataGridView2.Rows[i].Cells[7].Value.ToString().Replace(",", "");
                        ps.vat = dataGridView2.Rows[i].Cells[8].Value.ToString().Replace(",", "");
                        ps.net_total = dataGridView2.Rows[i].Cells[9].Value.ToString().Replace(",", "");
                        ps.num_use = int.Parse(dataGridView2.Rows[i].Cells[10].Value.ToString());
                        ps.package_unit = dataGridView2.Rows[i].Cells[11].Value.ToString();
                        ps.date_start   = dataGridView2.Rows[i].Cells[28].Value.ToString();
                        ps.date_expire  = dataGridView2.Rows[i].Cells[29].Value.ToString();
                        ps.max_use = int.Parse(dataGridView2.Rows[i].Cells[14].Value.ToString());
                        ps.type_package = dataGridView2.Rows[i].Cells[15].Value.ToString();
                        ps.trainer_name = dataGridView2.Rows[i].Cells[16].Value.ToString();
                        ps.percent_discount = int.Parse(dataGridView2.Rows[i].Cells[17].Value.ToString());
                        ps.trainer_code = dataGridView2.Rows[i].Cells[18].Value.ToString();
                        ps.id = dataGridView2.Rows[i].Cells[19].Value.ToString();
                        ps.package_detail = dataGridView2.Rows[i].Cells[20].Value.ToString();
                        ps.package_code = dataGridView2.Rows[i].Cells[21].Value.ToString();
                        ps.notify_num = int.Parse(dataGridView2.Rows[i].Cells[22].Value.ToString());
                        ps.notify_unit = dataGridView2.Rows[i].Cells[23].Value.ToString();
                        ps.type_vat = dataGridView2.Rows[i].Cells[24].Value.ToString();
                        ps.type_buy = dataGridView2.Rows[i].Cells[26].Value.ToString();
                        ps.sale_code = dataGridView2.Rows[i].Cells[27].Value.ToString();

                        ps.company_code = Global.BRANCH.branch_code;
                        ps.invoice_code = invoiceCode;

                        ps.receiptCode = "";

                        ApiRest.InitailizeClient();
                        Response res = await ApiProcess.managePackageCustomer(typeActive, ps, Global.USER.user_login);
                        if (!res.status)
                        {
                            message = ",managePackageCustomer :" + res.message;
                            flag = false;

                            if (ps.type_package.Equals("MB"))
                            {
                                Global.PersonList = await ApiProcess.getPerson("ACTIVE");
                            }
                            else if (ps.type_package.Equals("PT"))
                            {
                                Global.PersonTrainerList = await ApiProcess.getPerson("TRAINER");
                            }
                            break;
                        }
                        else {
                            if (flagPrint.Equals("PRINT"))
                            {
                                ApiRest.InitailizeClient();
                                Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, ps.person_code);

                                if (ps.type_package.Equals("MB"))
                                {
                                    Global.PersonList = await ApiProcess.getPerson("ACTIVE");
                                    Utils.eformContract(ps, person, invoice.invoice_date, typePaymentTxt);
                                }
                                else if (ps.type_package.Equals("PT"))
                                {
                                    Global.PersonTrainerList = await ApiProcess.getPerson("TRAINER");
                                    Utils.eformContractPT(ps, person, invoice.invoice_date, typePaymentTxt);
                                }
                            }
                        }
                    }
                    ApiRest.InitailizeClient();
                    Response resInvoice = await ApiProcess.manageInvoice(Global.BRANCH.branch_code, invoice.typeActive, invoice, Global.USER.user_login);
                    if (!resInvoice.status)
                    {
                        message = ",manageInvoice :" + resInvoice.message;
                        flag = false;
                    }

                    if (flag)
                    {
                        if (flagPrint.Equals("PRINT"))
                        {
                            Utils.eformReceipt(typeVat, invoice, packagePersons, dateinv);
                        }
                        ApiRest.InitailizeClient();
                        Global.PersonList = await ApiProcess.getPerson("ACTIVE");

                        frm_dialog d = new frm_dialog("บันทึกใบเสร็จ สำเร็จ!!", "I");
                        d.ShowDialog();

                        dataGridView2.Rows.Clear();
                        calprice();
                        labName.Text = "";
                        labCode.Text = "";
                        labNickname.Text = "";
                        labStatus.Text = "";

                        picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                        picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;

                        Global.PERSON = new Person();

                    }
                    else
                    {
                        frm_dialog d = new frm_dialog("ระบบผิดพลาด กรุณาติดต่อผู้ดูแลระบบ!!", "I");

                        Utils.getErrorToLog(":: button2_Click ::" + message, "frm_packege_cus");

                        d.ShowDialog();
                        this.DialogResult = DialogResult.Cancel;
                    }
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                Utils.getErrorToLog(":: button2_Click ::" + ex.ToString(), "frm_packege_cus");
                this.Close();

            }
            finally {
                picLoading.Visible = false;
            }
        }

        private void labTotal_TextChanged(object sender, EventArgs e)
        {
            int total = Int32.Parse(labTotal.Text.Replace(",", "").Replace(".00", ""));
            
        }

        private void frm_packege_cus_Shown(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
            PackageMas = new Package();
        }
    }
}
