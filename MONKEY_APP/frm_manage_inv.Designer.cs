﻿namespace MONKEY_APP
{
    partial class frm_manage_inv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.dateIncome = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.packageCode = new System.Windows.Forms.TextBox();
            this.expDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.alertTxt = new System.Windows.Forms.Label();
            this.numDic = new System.Windows.Forms.NumericUpDown();
            this.packageId = new System.Windows.Forms.TextBox();
            this.comboTrainer = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.packageUnit = new System.Windows.Forms.ComboBox();
            this.numUse = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numPack = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.maxUse = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.packageName = new System.Windows.Forms.Label();
            this.packageDetail = new System.Windows.Forms.Label();
            this.txtRunReg = new System.Windows.Forms.Label();
            this.packagePrice = new System.Windows.Forms.Label();
            this.txtPt = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.Label();
            this.labStatus = new System.Windows.Forms.Label();
            this.comboYear = new System.Windows.Forms.ComboBox();
            this.comboMonth = new System.Windows.Forms.ComboBox();
            this.comboDate = new System.Windows.Forms.ComboBox();
            this.comboYear2 = new System.Windows.Forms.ComboBox();
            this.comboMonth2 = new System.Windows.Forms.ComboBox();
            this.comboDate2 = new System.Windows.Forms.ComboBox();
            this.radNew = new System.Windows.Forms.RadioButton();
            this.radRENew = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.comboEmp = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxUse)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(779, 58);
            this.panel1.TabIndex = 115;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.user__1_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "สร้างรายการ";
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(749, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(65, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 24);
            this.label4.TabIndex = 116;
            this.label4.Text = "เลขที่สมัคร :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(414, 308);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 24);
            this.label6.TabIndex = 116;
            this.label6.Text = "วันหมดอายุ :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(81, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 24);
            this.label18.TabIndex = 116;
            this.label18.Text = "แพคเกจ :";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button4.Enabled = false;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(216, 609);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 44);
            this.button4.TabIndex = 149;
            this.button4.Text = "บันทึก";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button5.Location = new System.Drawing.Point(383, 609);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 44);
            this.button5.TabIndex = 150;
            this.button5.Text = "ยกเลิก";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(81, 305);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 24);
            this.label21.TabIndex = 116;
            this.label21.Text = "วันที่เริ่ม :";
            // 
            // dateIncome
            // 
            this.dateIncome.CalendarFont = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.CustomFormat = "yyyy/MM/dd";
            this.dateIncome.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateIncome.Location = new System.Drawing.Point(298, 303);
            this.dateIncome.Name = "dateIncome";
            this.dateIncome.Size = new System.Drawing.Size(10, 31);
            this.dateIncome.TabIndex = 0;
            this.dateIncome.ValueChanged += new System.EventHandler(this.dateIncome_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(63, 227);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 24);
            this.label11.TabIndex = 116;
            this.label11.Text = "อายุสมาชิก :";
            // 
            // packageCode
            // 
            this.packageCode.BackColor = System.Drawing.Color.LightGray;
            this.packageCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageCode.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageCode.Location = new System.Drawing.Point(154, 86);
            this.packageCode.Multiline = true;
            this.packageCode.Name = "packageCode";
            this.packageCode.ReadOnly = true;
            this.packageCode.Size = new System.Drawing.Size(121, 30);
            this.packageCode.TabIndex = 117;
            // 
            // expDate
            // 
            this.expDate.CalendarFont = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.expDate.CustomFormat = "yyyy/MM/dd";
            this.expDate.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.expDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.expDate.Location = new System.Drawing.Point(331, 303);
            this.expDate.Name = "expDate";
            this.expDate.Size = new System.Drawing.Size(10, 31);
            this.expDate.TabIndex = 0;
            this.expDate.ValueChanged += new System.EventHandler(this.expDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(62, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 24);
            this.label2.TabIndex = 116;
            this.label2.Text = "รายละเอียด :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(58, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 24);
            this.label3.TabIndex = 116;
            this.label3.Text = "ราคา/หน่วย :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(73, 396);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 24);
            this.label10.TabIndex = 116;
            this.label10.Text = "ราคารวม :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(79, 492);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 24);
            this.label12.TabIndex = 116;
            this.label12.Text = "ยอดสุทธิ :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(384, 442);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 24);
            this.label7.TabIndex = 116;
            this.label7.Text = "ผู้สอน/เทรนเนอร์ :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(399, 396);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 24);
            this.label8.TabIndex = 116;
            this.label8.Text = "ส่วนลด (บาท) :";
            // 
            // alertTxt
            // 
            this.alertTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alertTxt.BackColor = System.Drawing.Color.Transparent;
            this.alertTxt.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.alertTxt.ForeColor = System.Drawing.Color.Red;
            this.alertTxt.Location = new System.Drawing.Point(25, 571);
            this.alertTxt.MinimumSize = new System.Drawing.Size(270, 0);
            this.alertTxt.Name = "alertTxt";
            this.alertTxt.Size = new System.Drawing.Size(728, 28);
            this.alertTxt.TabIndex = 153;
            this.alertTxt.Text = "alert";
            this.alertTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.alertTxt.Visible = false;
            // 
            // numDic
            // 
            this.numDic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numDic.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numDic.Location = new System.Drawing.Point(510, 394);
            this.numDic.Margin = new System.Windows.Forms.Padding(5);
            this.numDic.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numDic.Name = "numDic";
            this.numDic.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numDic.Size = new System.Drawing.Size(120, 27);
            this.numDic.TabIndex = 154;
            this.numDic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numDic.ValueChanged += new System.EventHandler(this.numDic_ValueChanged);
            this.numDic.Enter += new System.EventHandler(this.numDic_Enter);
            this.numDic.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numDic_KeyUp);
            // 
            // packageId
            // 
            this.packageId.BackColor = System.Drawing.Color.LightGray;
            this.packageId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageId.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageId.Location = new System.Drawing.Point(478, 178);
            this.packageId.Multiline = true;
            this.packageId.Name = "packageId";
            this.packageId.ReadOnly = true;
            this.packageId.Size = new System.Drawing.Size(196, 30);
            this.packageId.TabIndex = 117;
            this.packageId.Visible = false;
            // 
            // comboTrainer
            // 
            this.comboTrainer.Enabled = false;
            this.comboTrainer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboTrainer.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboTrainer.FormattingEnabled = true;
            this.comboTrainer.Location = new System.Drawing.Point(510, 440);
            this.comboTrainer.Name = "comboTrainer";
            this.comboTrainer.Size = new System.Drawing.Size(208, 32);
            this.comboTrainer.TabIndex = 157;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(400, 492);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 24);
            this.label13.TabIndex = 116;
            this.label13.Text = "สถานะสมาชิก :";
            // 
            // packageUnit
            // 
            this.packageUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.packageUnit.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageUnit.FormattingEnabled = true;
            this.packageUnit.Location = new System.Drawing.Point(292, 226);
            this.packageUnit.Name = "packageUnit";
            this.packageUnit.Size = new System.Drawing.Size(177, 32);
            this.packageUnit.TabIndex = 159;
            this.packageUnit.SelectedIndexChanged += new System.EventHandler(this.packageUnit_SelectedIndexChanged);
            this.packageUnit.SelectedValueChanged += new System.EventHandler(this.packageUnit_SelectedValueChanged);
            // 
            // numUse
            // 
            this.numUse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numUse.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numUse.Location = new System.Drawing.Point(153, 227);
            this.numUse.Margin = new System.Windows.Forms.Padding(5);
            this.numUse.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numUse.Name = "numUse";
            this.numUse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numUse.Size = new System.Drawing.Size(120, 27);
            this.numUse.TabIndex = 160;
            this.numUse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numUse.ValueChanged += new System.EventHandler(this.numUse_ValueChanged);
            this.numUse.KeyDown += new System.Windows.Forms.KeyEventHandler(this.numUse_KeyDown);
            this.numUse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numUse_KeyPress);
            this.numUse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numUse_KeyUp);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(439, 351);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 24);
            this.label15.TabIndex = 116;
            this.label15.Text = "จำนวน :";
            // 
            // numPack
            // 
            this.numPack.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numPack.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.numPack.Location = new System.Drawing.Point(510, 348);
            this.numPack.Margin = new System.Windows.Forms.Padding(5);
            this.numPack.Name = "numPack";
            this.numPack.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numPack.Size = new System.Drawing.Size(120, 27);
            this.numPack.TabIndex = 154;
            this.numPack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPack.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPack.ValueChanged += new System.EventHandler(this.numPack_ValueChanged);
            this.numPack.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numPack_KeyUp);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(86, 442);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 24);
            this.label16.TabIndex = 116;
            this.label16.Text = "ส่วนลด :";
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.LightGray;
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtID.Location = new System.Drawing.Point(478, 227);
            this.txtID.Multiline = true;
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(196, 30);
            this.txtID.TabIndex = 117;
            this.txtID.Visible = false;
            // 
            // maxUse
            // 
            this.maxUse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maxUse.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.maxUse.Location = new System.Drawing.Point(153, 266);
            this.maxUse.Margin = new System.Windows.Forms.Padding(5);
            this.maxUse.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.maxUse.Name = "maxUse";
            this.maxUse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maxUse.Size = new System.Drawing.Size(120, 27);
            this.maxUse.TabIndex = 162;
            this.maxUse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxUse.ValueChanged += new System.EventHandler(this.maxUse_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(19, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 24);
            this.label5.TabIndex = 161;
            this.label5.Text = "จำนวนวันหมดอายุ :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(288, 267);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 24);
            this.label14.TabIndex = 161;
            this.label14.Text = "วัน";
            // 
            // packageName
            // 
            this.packageName.BackColor = System.Drawing.Color.LightGray;
            this.packageName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageName.Location = new System.Drawing.Point(294, 86);
            this.packageName.Name = "packageName";
            this.packageName.Size = new System.Drawing.Size(407, 30);
            this.packageName.TabIndex = 163;
            // 
            // packageDetail
            // 
            this.packageDetail.BackColor = System.Drawing.Color.LightGray;
            this.packageDetail.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packageDetail.Location = new System.Drawing.Point(154, 131);
            this.packageDetail.Name = "packageDetail";
            this.packageDetail.Size = new System.Drawing.Size(547, 30);
            this.packageDetail.TabIndex = 163;
            this.packageDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRunReg
            // 
            this.txtRunReg.BackColor = System.Drawing.Color.LightGray;
            this.txtRunReg.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtRunReg.Location = new System.Drawing.Point(154, 178);
            this.txtRunReg.Name = "txtRunReg";
            this.txtRunReg.Size = new System.Drawing.Size(315, 30);
            this.txtRunReg.TabIndex = 163;
            this.txtRunReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // packagePrice
            // 
            this.packagePrice.BackColor = System.Drawing.Color.LightGray;
            this.packagePrice.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.packagePrice.Location = new System.Drawing.Point(153, 348);
            this.packagePrice.Name = "packagePrice";
            this.packagePrice.Size = new System.Drawing.Size(207, 30);
            this.packagePrice.TabIndex = 163;
            this.packagePrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPt
            // 
            this.txtPt.BackColor = System.Drawing.Color.LightGray;
            this.txtPt.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPt.Location = new System.Drawing.Point(153, 394);
            this.txtPt.Name = "txtPt";
            this.txtPt.Size = new System.Drawing.Size(207, 30);
            this.txtPt.TabIndex = 163;
            this.txtPt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiscount
            // 
            this.txtDiscount.BackColor = System.Drawing.Color.LightGray;
            this.txtDiscount.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtDiscount.Location = new System.Drawing.Point(153, 440);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(207, 30);
            this.txtDiscount.TabIndex = 163;
            this.txtDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.LightGray;
            this.txtTotal.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotal.Location = new System.Drawing.Point(153, 489);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(207, 30);
            this.txtTotal.TabIndex = 163;
            this.txtTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labStatus
            // 
            this.labStatus.BackColor = System.Drawing.Color.LightGray;
            this.labStatus.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labStatus.Location = new System.Drawing.Point(510, 489);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(207, 30);
            this.labStatus.TabIndex = 163;
            this.labStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboYear
            // 
            this.comboYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboYear.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboYear.FormattingEnabled = true;
            this.comboYear.Location = new System.Drawing.Point(296, 303);
            this.comboYear.Name = "comboYear";
            this.comboYear.Size = new System.Drawing.Size(64, 32);
            this.comboYear.TabIndex = 164;
            this.comboYear.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboMonth
            // 
            this.comboMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboMonth.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboMonth.FormattingEnabled = true;
            this.comboMonth.Location = new System.Drawing.Point(205, 303);
            this.comboMonth.Name = "comboMonth";
            this.comboMonth.Size = new System.Drawing.Size(85, 32);
            this.comboMonth.TabIndex = 165;
            this.comboMonth.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboDate
            // 
            this.comboDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboDate.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboDate.FormattingEnabled = true;
            this.comboDate.Location = new System.Drawing.Point(153, 303);
            this.comboDate.Name = "comboDate";
            this.comboDate.Size = new System.Drawing.Size(46, 32);
            this.comboDate.TabIndex = 166;
            this.comboDate.SelectedIndexChanged += new System.EventHandler(this.changeDateIn);
            // 
            // comboYear2
            // 
            this.comboYear2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboYear2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboYear2.FormattingEnabled = true;
            this.comboYear2.Location = new System.Drawing.Point(653, 303);
            this.comboYear2.Name = "comboYear2";
            this.comboYear2.Size = new System.Drawing.Size(64, 32);
            this.comboYear2.TabIndex = 167;
            this.comboYear2.SelectedIndexChanged += new System.EventHandler(this.changeDateEx);
            // 
            // comboMonth2
            // 
            this.comboMonth2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboMonth2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboMonth2.FormattingEnabled = true;
            this.comboMonth2.Location = new System.Drawing.Point(562, 303);
            this.comboMonth2.Name = "comboMonth2";
            this.comboMonth2.Size = new System.Drawing.Size(85, 32);
            this.comboMonth2.TabIndex = 168;
            this.comboMonth2.SelectedIndexChanged += new System.EventHandler(this.changeDateEx);
            // 
            // comboDate2
            // 
            this.comboDate2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboDate2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboDate2.FormattingEnabled = true;
            this.comboDate2.Location = new System.Drawing.Point(510, 303);
            this.comboDate2.Name = "comboDate2";
            this.comboDate2.Size = new System.Drawing.Size(46, 32);
            this.comboDate2.TabIndex = 169;
            this.comboDate2.SelectedIndexChanged += new System.EventHandler(this.changeDateEx);
            // 
            // radNew
            // 
            this.radNew.AutoSize = true;
            this.radNew.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radNew.Location = new System.Drawing.Point(154, 534);
            this.radNew.Name = "radNew";
            this.radNew.Size = new System.Drawing.Size(58, 32);
            this.radNew.TabIndex = 174;
            this.radNew.TabStop = true;
            this.radNew.Text = "NEW";
            this.radNew.UseVisualStyleBackColor = true;
            // 
            // radRENew
            // 
            this.radRENew.AutoSize = true;
            this.radRENew.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radRENew.Location = new System.Drawing.Point(230, 534);
            this.radRENew.Name = "radRENew";
            this.radRENew.Size = new System.Drawing.Size(73, 32);
            this.radRENew.TabIndex = 175;
            this.radRENew.TabStop = true;
            this.radRENew.Text = "RENEW";
            this.radRENew.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(43, 536);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 24);
            this.label9.TabIndex = 176;
            this.label9.Text = "ประเภทการซื้อ :";
            // 
            // comboEmp
            // 
            this.comboEmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboEmp.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboEmp.FormattingEnabled = true;
            this.comboEmp.Location = new System.Drawing.Point(509, 533);
            this.comboEmp.Name = "comboEmp";
            this.comboEmp.Size = new System.Drawing.Size(208, 32);
            this.comboEmp.TabIndex = 178;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(448, 535);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 24);
            this.label17.TabIndex = 177;
            this.label17.Text = "ผู้ขาย :";
            // 
            // frm_manage_inv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(779, 666);
            this.ControlBox = false;
            this.Controls.Add(this.comboEmp);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.radRENew);
            this.Controls.Add(this.radNew);
            this.Controls.Add(this.comboYear);
            this.Controls.Add(this.comboMonth);
            this.Controls.Add(this.comboYear2);
            this.Controls.Add(this.comboMonth2);
            this.Controls.Add(this.comboDate2);
            this.Controls.Add(this.comboDate);
            this.Controls.Add(this.labStatus);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtDiscount);
            this.Controls.Add(this.txtPt);
            this.Controls.Add(this.packagePrice);
            this.Controls.Add(this.txtRunReg);
            this.Controls.Add(this.packageDetail);
            this.Controls.Add(this.packageName);
            this.Controls.Add(this.maxUse);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numUse);
            this.Controls.Add(this.packageUnit);
            this.Controls.Add(this.comboTrainer);
            this.Controls.Add(this.numPack);
            this.Controls.Add(this.numDic);
            this.Controls.Add(this.alertTxt);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.packageId);
            this.Controls.Add(this.packageCode);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dateIncome);
            this.Controls.Add(this.expDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_manage_inv";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_manage_cus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxUse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dateIncome;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox packageCode;
        private System.Windows.Forms.DateTimePicker expDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label alertTxt;
        private System.Windows.Forms.NumericUpDown numDic;
        private System.Windows.Forms.TextBox packageId;
        private System.Windows.Forms.ComboBox comboTrainer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox packageUnit;
        private System.Windows.Forms.NumericUpDown numUse;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numPack;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.NumericUpDown maxUse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label packageName;
        private System.Windows.Forms.Label packageDetail;
        private System.Windows.Forms.Label txtRunReg;
        private System.Windows.Forms.Label packagePrice;
        private System.Windows.Forms.Label txtPt;
        private System.Windows.Forms.Label txtDiscount;
        private System.Windows.Forms.Label txtTotal;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.ComboBox comboYear;
        private System.Windows.Forms.ComboBox comboMonth;
        private System.Windows.Forms.ComboBox comboDate;
        private System.Windows.Forms.ComboBox comboYear2;
        private System.Windows.Forms.ComboBox comboMonth2;
        private System.Windows.Forms.ComboBox comboDate2;
        private System.Windows.Forms.RadioButton radNew;
        private System.Windows.Forms.RadioButton radRENew;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboEmp;
        private System.Windows.Forms.Label label17;
    }
}