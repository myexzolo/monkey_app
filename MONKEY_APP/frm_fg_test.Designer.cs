﻿namespace MONKEY_APP
{
    partial class frm_fg_test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_fg_test));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TextSensorCount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TextSensorIndex = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TextSensorSN = new System.Windows.Forms.TextBox();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.OptionBmp = new System.Windows.Forms.RadioButton();
            this.OptionJpg = new System.Windows.Forms.RadioButton();
            this.cmdSaveImage = new System.Windows.Forms.Button();
            this.cmdInit = new System.Windows.Forms.Button();
            this.ZKFPEngX1 = new AxZKFPEngXControl.AxZKFPEngX();
            this.cmdBeep = new System.Windows.Forms.Button();
            this.cmdGreen = new System.Windows.Forms.Button();
            this.cmdRED = new System.Windows.Forms.Button();
            this.cmdEnroll = new System.Windows.Forms.Button();
            this.cmdVerify = new System.Windows.Forms.Button();
            this.cmdIdentify = new System.Windows.Forms.Button();
            this.TextFingerName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.StatusBar = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.TextSensorCount);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.TextSensorIndex);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(471, 89);
            this.panel1.TabIndex = 116;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.fingerprint_scanning2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            this.labName.Location = new System.Drawing.Point(65, 50);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(41, 27);
            this.labName.TabIndex = 116;
            this.labName.Text = "text";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "ทดสอบลายนิ้วมือ";
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(441, 10);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(302, 10);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 126;
            this.label6.Text = "จำนวนหัวอ่าน";
            this.label6.Visible = false;
            // 
            // TextSensorCount
            // 
            this.TextSensorCount.AcceptsReturn = true;
            this.TextSensorCount.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorCount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorCount.Enabled = false;
            this.TextSensorCount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorCount.Location = new System.Drawing.Point(374, 10);
            this.TextSensorCount.MaxLength = 0;
            this.TextSensorCount.Name = "TextSensorCount";
            this.TextSensorCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorCount.Size = new System.Drawing.Size(65, 20);
            this.TextSensorCount.TabIndex = 123;
            this.TextSensorCount.Visible = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(326, 33);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(49, 17);
            this.label7.TabIndex = 125;
            this.label7.Text = "หัวอ่าน:";
            this.label7.Visible = false;
            // 
            // TextSensorIndex
            // 
            this.TextSensorIndex.AcceptsReturn = true;
            this.TextSensorIndex.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorIndex.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorIndex.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorIndex.Location = new System.Drawing.Point(374, 33);
            this.TextSensorIndex.MaxLength = 0;
            this.TextSensorIndex.Name = "TextSensorIndex";
            this.TextSensorIndex.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorIndex.Size = new System.Drawing.Size(57, 20);
            this.TextSensorIndex.TabIndex = 122;
            this.TextSensorIndex.Visible = false;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(302, 64);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(89, 25);
            this.label8.TabIndex = 124;
            this.label8.Text = "Sensor SN";
            this.label8.Visible = false;
            // 
            // TextSensorSN
            // 
            this.TextSensorSN.AcceptsReturn = true;
            this.TextSensorSN.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorSN.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorSN.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorSN.Location = new System.Drawing.Point(305, 84);
            this.TextSensorSN.MaxLength = 0;
            this.TextSensorSN.Name = "TextSensorSN";
            this.TextSensorSN.ReadOnly = true;
            this.TextSensorSN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorSN.Size = new System.Drawing.Size(134, 20);
            this.TextSensorSN.TabIndex = 121;
            this.TextSensorSN.Visible = false;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.OptionBmp);
            this.Frame2.Controls.Add(this.OptionJpg);
            this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(329, 235);
            this.Frame2.Name = "Frame2";
            this.Frame2.Padding = new System.Windows.Forms.Padding(0);
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(125, 41);
            this.Frame2.TabIndex = 128;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "ชนิดของรูป";
            this.Frame2.Visible = false;
            // 
            // OptionBmp
            // 
            this.OptionBmp.BackColor = System.Drawing.SystemColors.Control;
            this.OptionBmp.Checked = true;
            this.OptionBmp.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionBmp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OptionBmp.Location = new System.Drawing.Point(8, 16);
            this.OptionBmp.Name = "OptionBmp";
            this.OptionBmp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionBmp.Size = new System.Drawing.Size(49, 17);
            this.OptionBmp.TabIndex = 4;
            this.OptionBmp.TabStop = true;
            this.OptionBmp.Text = "BMP";
            this.OptionBmp.UseVisualStyleBackColor = false;
            // 
            // OptionJpg
            // 
            this.OptionJpg.BackColor = System.Drawing.SystemColors.Control;
            this.OptionJpg.Cursor = System.Windows.Forms.Cursors.Default;
            this.OptionJpg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OptionJpg.Location = new System.Drawing.Point(63, 16);
            this.OptionJpg.Name = "OptionJpg";
            this.OptionJpg.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OptionJpg.Size = new System.Drawing.Size(57, 17);
            this.OptionJpg.TabIndex = 3;
            this.OptionJpg.TabStop = true;
            this.OptionJpg.Text = "JPEG";
            this.OptionJpg.UseVisualStyleBackColor = false;
            // 
            // cmdSaveImage
            // 
            this.cmdSaveImage.BackColor = System.Drawing.SystemColors.Control;
            this.cmdSaveImage.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdSaveImage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdSaveImage.Location = new System.Drawing.Point(13, 69);
            this.cmdSaveImage.Name = "cmdSaveImage";
            this.cmdSaveImage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdSaveImage.Size = new System.Drawing.Size(113, 25);
            this.cmdSaveImage.TabIndex = 127;
            this.cmdSaveImage.Text = "บันทึกรูปลายนิ้วมือ";
            this.cmdSaveImage.UseVisualStyleBackColor = false;
            this.cmdSaveImage.Visible = false;
            this.cmdSaveImage.Click += new System.EventHandler(this.cmdSaveImage_Click);
            // 
            // cmdInit
            // 
            this.cmdInit.BackColor = System.Drawing.SystemColors.Control;
            this.cmdInit.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdInit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdInit.Location = new System.Drawing.Point(13, 125);
            this.cmdInit.Name = "cmdInit";
            this.cmdInit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdInit.Size = new System.Drawing.Size(113, 25);
            this.cmdInit.TabIndex = 129;
            this.cmdInit.Text = "เชื่อมต่อระบบ";
            this.cmdInit.UseVisualStyleBackColor = false;
            this.cmdInit.Visible = false;
            this.cmdInit.Click += new System.EventHandler(this.cmdInit_Click);
            // 
            // ZKFPEngX1
            // 
            this.ZKFPEngX1.Enabled = true;
            this.ZKFPEngX1.Location = new System.Drawing.Point(12, 199);
            this.ZKFPEngX1.Name = "ZKFPEngX1";
            this.ZKFPEngX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("ZKFPEngX1.OcxState")));
            this.ZKFPEngX1.Size = new System.Drawing.Size(24, 24);
            this.ZKFPEngX1.TabIndex = 119;
            this.ZKFPEngX1.OnFeatureInfo += new AxZKFPEngXControl.IZKFPEngXEvents_OnFeatureInfoEventHandler(this.ZKFPEngX1_OnFeatureInfo);
            this.ZKFPEngX1.OnImageReceived += new AxZKFPEngXControl.IZKFPEngXEvents_OnImageReceivedEventHandler(this.ZKFPEngX1_OnImageReceived);
            this.ZKFPEngX1.OnEnroll += new AxZKFPEngXControl.IZKFPEngXEvents_OnEnrollEventHandler(this.ZKFPEngX1_OnEnroll);
            this.ZKFPEngX1.OnCapture += new AxZKFPEngXControl.IZKFPEngXEvents_OnCaptureEventHandler(this.ZKFPEngX1_OnCapture);
            this.ZKFPEngX1.OnFingerTouching += new System.EventHandler(this.ZKFPEngX1_OnFingerTouching);
            // 
            // cmdBeep
            // 
            this.cmdBeep.Location = new System.Drawing.Point(388, 280);
            this.cmdBeep.Name = "cmdBeep";
            this.cmdBeep.Size = new System.Drawing.Size(83, 26);
            this.cmdBeep.TabIndex = 137;
            this.cmdBeep.Text = "Beep";
            this.cmdBeep.UseVisualStyleBackColor = true;
            this.cmdBeep.Visible = false;
            this.cmdBeep.Click += new System.EventHandler(this.cmdBeep_Click);
            // 
            // cmdGreen
            // 
            this.cmdGreen.Location = new System.Drawing.Point(299, 281);
            this.cmdGreen.Name = "cmdGreen";
            this.cmdGreen.Size = new System.Drawing.Size(83, 26);
            this.cmdGreen.TabIndex = 136;
            this.cmdGreen.Text = "Green LED";
            this.cmdGreen.UseVisualStyleBackColor = true;
            this.cmdGreen.Visible = false;
            this.cmdGreen.Click += new System.EventHandler(this.cmdGreen_Click);
            // 
            // cmdRED
            // 
            this.cmdRED.Location = new System.Drawing.Point(208, 289);
            this.cmdRED.Name = "cmdRED";
            this.cmdRED.Size = new System.Drawing.Size(83, 26);
            this.cmdRED.TabIndex = 135;
            this.cmdRED.Text = "Red LED";
            this.cmdRED.UseVisualStyleBackColor = true;
            this.cmdRED.Visible = false;
            this.cmdRED.Click += new System.EventHandler(this.cmdRED_Click);
            // 
            // cmdEnroll
            // 
            this.cmdEnroll.BackColor = System.Drawing.SystemColors.Control;
            this.cmdEnroll.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdEnroll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdEnroll.Location = new System.Drawing.Point(13, 168);
            this.cmdEnroll.Name = "cmdEnroll";
            this.cmdEnroll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdEnroll.Size = new System.Drawing.Size(113, 25);
            this.cmdEnroll.TabIndex = 133;
            this.cmdEnroll.Text = "เก็บลายนิ้วมือ";
            this.cmdEnroll.UseVisualStyleBackColor = false;
            this.cmdEnroll.Visible = false;
            this.cmdEnroll.Click += new System.EventHandler(this.cmdEnroll_Click);
            // 
            // cmdVerify
            // 
            this.cmdVerify.BackColor = System.Drawing.SystemColors.Control;
            this.cmdVerify.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdVerify.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdVerify.Location = new System.Drawing.Point(12, 281);
            this.cmdVerify.Name = "cmdVerify";
            this.cmdVerify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdVerify.Size = new System.Drawing.Size(96, 25);
            this.cmdVerify.TabIndex = 132;
            this.cmdVerify.Text = "ค้นหาแบบ (1:1)";
            this.cmdVerify.UseVisualStyleBackColor = false;
            this.cmdVerify.Visible = false;
            this.cmdVerify.Click += new System.EventHandler(this.cmdVerify_Click);
            // 
            // cmdIdentify
            // 
            this.cmdIdentify.BackColor = System.Drawing.SystemColors.Control;
            this.cmdIdentify.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdIdentify.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdIdentify.Location = new System.Drawing.Point(109, 289);
            this.cmdIdentify.Name = "cmdIdentify";
            this.cmdIdentify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdIdentify.Size = new System.Drawing.Size(93, 25);
            this.cmdIdentify.TabIndex = 131;
            this.cmdIdentify.Text = "ค้นหาแบบ (1:N)";
            this.cmdIdentify.UseVisualStyleBackColor = false;
            this.cmdIdentify.Visible = false;
            this.cmdIdentify.Click += new System.EventHandler(this.cmdIdentify_Click);
            // 
            // TextFingerName
            // 
            this.TextFingerName.AcceptsReturn = true;
            this.TextFingerName.BackColor = System.Drawing.SystemColors.Window;
            this.TextFingerName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextFingerName.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextFingerName.Location = new System.Drawing.Point(334, 120);
            this.TextFingerName.MaxLength = 0;
            this.TextFingerName.Name = "TextFingerName";
            this.TextFingerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextFingerName.Size = new System.Drawing.Size(105, 20);
            this.TextFingerName.TabIndex = 130;
            this.TextFingerName.Text = "fingerprint1";
            this.TextFingerName.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(278, 123);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 134;
            this.label9.Text = "ชื่อนิ้ว :";
            this.label9.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox5);
            this.panel2.Location = new System.Drawing.Point(145, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(183, 200);
            this.panel2.TabIndex = 139;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Image = global::MONKEY_APP.Properties.Resources.fingerprint__1_;
            this.pictureBox5.Location = new System.Drawing.Point(25, 21);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(133, 148);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 120;
            this.pictureBox5.TabStop = false;
            // 
            // StatusBar
            // 
            this.StatusBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.StatusBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.StatusBar.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.StatusBar.ForeColor = System.Drawing.Color.Yellow;
            this.StatusBar.Location = new System.Drawing.Point(0, 319);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StatusBar.Size = new System.Drawing.Size(490, 32);
            this.StatusBar.TabIndex = 138;
            this.StatusBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frm_fg_test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 351);
            this.ControlBox = false;
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cmdBeep);
            this.Controls.Add(this.cmdGreen);
            this.Controls.Add(this.cmdRED);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmdEnroll);
            this.Controls.Add(this.cmdVerify);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.TextSensorSN);
            this.Controls.Add(this.cmdIdentify);
            this.Controls.Add(this.TextFingerName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmdSaveImage);
            this.Controls.Add(this.cmdInit);
            this.Controls.Add(this.ZKFPEngX1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_fg_test";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_fg_cus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button7;
        public AxZKFPEngXControl.AxZKFPEngX ZKFPEngX1;
        internal System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.TextBox TextSensorCount;
        public System.Windows.Forms.TextBox TextSensorIndex;
        public System.Windows.Forms.TextBox TextSensorSN;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Button cmdBeep;
        internal System.Windows.Forms.Button cmdGreen;
        internal System.Windows.Forms.Button cmdRED;
        public System.Windows.Forms.Button cmdEnroll;
        public System.Windows.Forms.Button cmdVerify;
        public System.Windows.Forms.Button cmdIdentify;
        public System.Windows.Forms.TextBox TextFingerName;
        public System.Windows.Forms.Button cmdInit;
        public System.Windows.Forms.GroupBox Frame2;
        public System.Windows.Forms.RadioButton OptionBmp;
        public System.Windows.Forms.RadioButton OptionJpg;
        public System.Windows.Forms.Button cmdSaveImage;
        public System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labName;
        public System.Windows.Forms.Label StatusBar;
    }
}