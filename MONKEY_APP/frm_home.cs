﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_home : Form
    {
        public frm_home()
        {
            InitializeComponent();
        }
        private DataTable dtm;
        private int Mon = 0;
        private int Tue = 0;
        private int Wed = 0;
        private int Thu = 0;
        private int Fri = 0;
        private int Sat = 0;
        private int Sun = 0;

        DateTime dateStart;
        DateTime dateEnd;

        CultureInfo thCulture = new CultureInfo("th-TH");
        //CultureInfo usCulture = new CultureInfo("en-US");


        private void frm_home_Load(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight = Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth;

            int panelM = panelw1 / 100 * 55;

            panelClass.Width = panelw1 / 100 * 43;
            panelClass.Height = scrHeight - 219;

            //int panelpakX = panelM + 60;
            //this.panelClass.Location = new System.Drawing.Point(panelpakX, 82);
            

            //ประกาศ DateTime เพื่อมาเป็นเวลาปัจจุบัน
            DateTime dt = DateTime.Now;

            //แสดงแค่ปี พ.ศ. กับ ค.ศ.
            Console.WriteLine(dt.ToString("yyyy/MM/dd HH:mm", thCulture));

            dt = Convert.ToDateTime(dt.ToString("yyyy/MM/dd HH:mm"));

            checkExpired();
            reserveList();
            setDay(dt);
            checkPackageExpiration();
        }

        private async void reserveList()
        {
            try
            {
                List<ReserveClass> reserveClassList = await ApiProcess.getReserveClassList("");

                Bitmap Image = global::MONKEY_APP.Properties.Resources.printer_tool30;
                Bitmap Image2 = global::MONKEY_APP.Properties.Resources.error30;
                Bitmap Image3 = global::MONKEY_APP.Properties.Resources.error30_d;
                Bitmap panding = global::MONKEY_APP.Properties.Resources.line;
                Bitmap checkin = MONKEY_APP.Properties.Resources.point;

                byte[] printImage;
                byte[] cancelImage = ImageToByte(Image2);

                dtm = new DataTable();
                dtm.Columns.Add("   ", typeof(byte[]));//0
                dtm.Columns.Add("สถานะ", typeof(string));//1
                dtm.Columns.Add("วันที่", typeof(string));//2
                dtm.Columns.Add("เวลา", typeof(string));//3
                dtm.Columns.Add("รายการ", typeof(string));//4
                dtm.Columns.Add("ชื่อ - สกุล", typeof(string));//5
                dtm.Columns.Add("ชื่อเล่น", typeof(string));//6
                dtm.Columns.Add("เบอร์โทร", typeof(string));//7
                dtm.Columns.Add("วันที่ทำรายการ", typeof(string));//8
                dtm.Columns.Add("หมดอายุ", typeof(string));//9
                dtm.Columns.Add("reserve_id", typeof(string));//10
                dtm.Columns.Add("ยกเลิก", typeof(byte[]));//11
                dtm.Columns.Add("schedule_day_id", typeof(string));//12
                dtm.Columns.Add("date_class", typeof(string));//13
                dtm.Columns.Add("time_start", typeof(string));//14
                dtm.Columns.Add("time_end", typeof(string));//15
                dtm.Columns.Add("EMP_NICKNAME", typeof(string));//16
                dtm.Columns.Add("PERSON_CODE", typeof(string));//17
                dtm.Columns.Add("status", typeof(string));//18
                dtm.Columns.Add("unit", typeof(string));//19
                dtm.Columns.Add("join_seq", typeof(string));//20

                string status = "";
                if (reserveClassList != null)
                {
                    for (int i = 0; i < reserveClassList.Count(); i++)
                    {
                        ReserveClass rs = reserveClassList[i];
                        if (rs.status.Equals("S"))
                        {
                            status = "จอง";
                            printImage = ImageToByte(checkin);
                            cancelImage = ImageToByte(Image2);
                        }
                        else if (rs.status.Equals("P"))
                        {
                            status = "Panding";
                            printImage = ImageToByte(panding);
                            cancelImage = ImageToByte(Image2);
                        }
                        else if (rs.status.Equals("E"))
                        {
                            status = "Expired";
                            printImage = ImageToByte(panding);
                            cancelImage = ImageToByte(Image3);
                        }
                        else
                        {
                            printImage = ImageToByte(Image);
                            status = "Check In";
                            cancelImage = ImageToByte(Image3);
                        }
                        dtm.Rows.Add(
                            printImage, status, rs.date_class_txt, rs.time_start + " - " + rs.time_end, rs.name_class, rs.person_name,rs.PERSON_NICKNAME, rs.PERSON_TEL_MOBILE,
                            rs.date_reserve_txt, rs.expire_date_txt, rs.reserve_id, cancelImage, rs.schedule_day_id,
                            rs.date_class, rs.time_start, rs.time_end, rs.EMP_NICKNAME, rs.PERSON_CODE, rs.status, rs.unit, rs.join_seq);

                    }
                }

                //dtm.Columns.Add("   ", typeof(byte[]));0
                //dtm.Columns.Add("สถานะ", typeof(byte[]));1
                //dtm.Columns.Add("วันที่", typeof(string));2
                //dtm.Columns.Add("เวลา", typeof(string));3
                //dtm.Columns.Add("รายการ", typeof(string));4
                //dtm.Columns.Add("ชื่อ - สกุล", typeof(string));5
                //dtm.Columns.Add("ชื่อเล่น", typeof(string));6
                //dtm.Columns.Add("วันที่ทำรายการ", typeof(string));7
                //dtm.Columns.Add("หมดอายุ", typeof(string));8
                //dtm.Columns.Add("reserve_id", typeof(string));9
                //dtm.Columns.Add("ยกเลิก", typeof(byte[]));10
                //dtm.Columns.Add("schedule_day_id", typeof(string));11
                //dtm.Columns.Add("date_class", typeof(string));12
                //dtm.Columns.Add("time_start", typeof(string));13
                //dtm.Columns.Add("time_end", typeof(string));14
                //dtm.Columns.Add("EMP_NICKNAME", typeof(string));15
                //dtm.Columns.Add("PERSON_CODE", typeof(string));16

                dataGridView1.DataSource = dtm;

                dataGridView1.Columns[0].Width = 70;
                dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[1].Width = 80;
                dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[2].Width = 90;
                dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView1.Columns[3].Width = 100;
                dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;


                dataGridView1.Columns[4].Width = 120;
                dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[5].Width = 200;
                dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[6].Width = 100;
                dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[7].Width = 140;
                dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[8].Width = 140;
                dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[9].Width = 140;
                dataGridView1.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[12].Visible = false;
                dataGridView1.Columns[13].Visible = false;
                dataGridView1.Columns[14].Visible = false;
                dataGridView1.Columns[15].Visible = false;
                dataGridView1.Columns[16].Visible = false;
                dataGridView1.Columns[17].Visible = false;
                dataGridView1.Columns[18].Visible = false;
                dataGridView1.Columns[19].Visible = false;
                dataGridView1.Columns[20].Visible = false;

                dataGridView1.Columns[11].Width = 70;
                dataGridView1.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch {

            }
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        private void setDay(DateTime dt)
        {
            tableLayoutPanel3.Visible = true;
            GC.Collect();
            GC.WaitForPendingFinalizers();

            tableLayoutPanel1.Controls.Clear();

            if (dt.DayOfWeek == DayOfWeek.Monday)
            {
                Mon = 0;
                Tue = 1;
                Wed = 2;
                Thu = 3;
                Fri = 4;
                Sat = 5;
                Sun = 6;
            }
            else if (dt.DayOfWeek == DayOfWeek.Tuesday)
            {
                Mon = -1;
                Tue = 0;
                Wed = 1;
                Thu = 2;
                Fri = 3;
                Sat = 4;
                Sun = 5;
            }
            else if (dt.DayOfWeek == DayOfWeek.Wednesday)
            {
                Mon = -2;
                Tue = -1;
                Wed = 0;
                Thu = 1;
                Fri = 2;
                Sat = 3;
                Sun = 4;
            }
            else if (dt.DayOfWeek == DayOfWeek.Thursday)
            {
                Mon = -3;
                Tue = -2;
                Wed = -1;
                Thu = 0;
                Fri = 1;
                Sat = 2;
                Sun = 3;
            }
            else if (dt.DayOfWeek == DayOfWeek.Friday)
            {
                Mon = -4;
                Tue = -3;
                Wed = -2;
                Thu = -1;
                Fri = 0;
                Sat = 1;
                Sun = 2;
            }
            else if (dt.DayOfWeek == DayOfWeek.Saturday)
            {
                Mon = -5;
                Tue = -4;
                Wed = -3;
                Thu = -2;
                Fri = -1;
                Sat = 0;
                Sun = 1;
            }
            else if (dt.DayOfWeek == DayOfWeek.Sunday)
            {
                Mon = -6;
                Tue = -5;
                Wed = -4;
                Thu = -3;
                Fri = -2;
                Sat = -1;
                Sun = 0;
            }
            dateStart   = dt.AddDays(Mon);
            dateEnd     = dt.AddDays(Sun);
            String dateStartTxt   = dateStart.ToString("dd/MM/yyyy", thCulture);
            String dateEndTxt     = dateEnd.ToString("dd/MM/yyyy", thCulture);
            labDateBetween.Text = "ตั้งแต่วันที่ " + dateStartTxt + " ถึงวันที่ " + dateEndTxt;
            labDay1.Text = dateStart.ToString("dddd dd/MM");
            labDay2.Text = dt.AddDays(Tue).ToString("dddd dd/MM");
            labDay3.Text = dt.AddDays(Wed).ToString("dddd dd/MM");
            labDay4.Text = dt.AddDays(Thu).ToString("dddd dd/MM");
            labDay5.Text = dt.AddDays(Fri).ToString("dddd dd/MM");
            labDay6.Text = dt.AddDays(Sat).ToString("dddd dd/MM");
            labDay7.Text = dateEnd.ToString("dddd dd/MM");

            getClassList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            dateStart = dateStart.AddDays(-1);
            setDay(dateStart);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            dateEnd = dateEnd.AddDays(1);
            setDay(dateEnd);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            setDay(DateTime.Today);
        }

        private async void checkPackageExpiration() {
            try
            {
                List<PackagePerson> packagePersonList = await ApiProcess.checkPackageExpiration();

                DataTable dt = new DataTable();
                dt.Columns.Add("ชื่อ - สกุล", typeof(string));
                dt.Columns.Add("ชื่อเล่น", typeof(string));
                dt.Columns.Add("รายการ", typeof(string));
                dt.Columns.Add("หมดอายุ", typeof(string));
                dt.Columns.Add("คงเหลือ", typeof(string));
                dt.Columns.Add("หน่วย", typeof(string));
                //dt.Columns.Add("ผู้ดูแล", typeof(string));

                for (int i = 0; i < packagePersonList.Count(); i++)
                {
                    PackagePerson ps = packagePersonList[i];
                    //dt.Rows.Add(
                    //       ps.person_name, ps.person_nickname, ps.package_name,Utils.getDateEntoTh(ps.date_expire),
                    //       ps.use_package+"/"+ps.num_use,ps.package_unit);
                }
                dataGridView2.DataSource = dt;

                dataGridView2.Columns[0].Width = 180;
                dataGridView2.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[1].Width = 100;
                dataGridView2.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[2].Width = 218;
                dataGridView2.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView2.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[3].Width = 90;
                dataGridView2.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView2.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView2.Columns[4].Width = 80;
                dataGridView2.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView2.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

                //dataGridView2.Columns[5].Width = 80;
                //dataGridView2.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                //dataGridView2.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;

            }
            catch (Exception e)
            {

            }
            
        }

        private async void getClassList()
        {
            try
            {
                List<ScheduleClass> scheduleClassList = await ApiProcess.getScheduleClassList(Global.BRANCH.branch_code, dateStart.ToString("yyyy/MM/dd"), dateEnd.ToString("yyyy/MM/dd"));
                DateTime dateNow = DateTime.Now;
                dateNow = Convert.ToDateTime(dateNow.ToString("yyyy/MM/dd HH:mm"));

                for (int i = 0; i < scheduleClassList.Count(); i++)
                {
                    ScheduleClass scheduleClass = scheduleClassList[i];

                    
                    Panel panel = new Panel();
                    //Label labClassName = new Label();
                    Label labTime = new Label();
                    Label labTrain = new Label();
                    Label labUnit = new Label();
                    Label labStatus = new Label();
                    PictureBox pic = new PictureBox(); 

                    //labClassName.AutoEllipsis = true;
                    //labClassName.Location = new System.Drawing.Point(3, 0);
                    //labClassName.Size = new System.Drawing.Size(136, 25);
                    //labClassName.Text = scheduleClass.name_class;
                    //labClassName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;


                    labTime.Font = new System.Drawing.Font("TH SarabunPSK", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
                    labTime.Location = new System.Drawing.Point(3, 78);
                    labTime.Size = new System.Drawing.Size(141, 22);
                    labTime.Text = scheduleClass.time_start + " - " + scheduleClass.time_end;
                    labTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;


                    labTrain.AutoEllipsis = true;
                    labTrain.Font = new System.Drawing.Font("TH SarabunPSK", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
                    labTrain.Location = new System.Drawing.Point(3, 100);
                    labTrain.Size = new System.Drawing.Size(88, 25);
                    labTrain.Text = scheduleClass.EMP_NICKNAME;
                    labTrain.ForeColor = System.Drawing.Color.Navy;
                    labTrain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;


                    labUnit.AutoEllipsis = true;
                    labUnit.Font = new System.Drawing.Font("TH SarabunPSK", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
                    labUnit.Location = new System.Drawing.Point(97, 100);
                    labUnit.Size = new System.Drawing.Size(50, 25);
                    labUnit.ForeColor = System.Drawing.Color.Navy;
                    labUnit.Text = scheduleClass.person_join + "/" + scheduleClass.unit;
                    labUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;

                    labStatus.BackColor = System.Drawing.Color.Silver;
                    labStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    labStatus.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
                    labStatus.ForeColor = System.Drawing.Color.SaddleBrown;
                    labStatus.Location = new System.Drawing.Point(46, 0);
                    labStatus.Size = new System.Drawing.Size(104, 32);
                    labStatus.TabIndex = 2;
                    labStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                    labStatus.Visible = false;

                    
                    DateTime date = Convert.ToDateTime(scheduleClass.date_class + " " + scheduleClass.time_end);

                    pic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
                    pic.Location = new System.Drawing.Point(21, -8);
                    pic.Size = new System.Drawing.Size(100, 100);
                    pic.TabIndex = 0;
                    pic.TabStop = false;


                    if (!scheduleClass.image_class.Equals(""))
                    {
                        Utils.setImage(scheduleClass.image_class, pic);
                    }
                    else
                    {
                        pic.BackgroundImage = MONKEY_APP.Properties.Resources.picture;
                    }


                    Console.WriteLine(date + " > " + dateNow);

                    if (date > dateNow)
                    {
                        ReserveClass reserveClass = new ReserveClass();

                        reserveClass.schedule_day_id = scheduleClass.id;
                        reserveClass.name_class = scheduleClass.name_class;
                        reserveClass.date_class = scheduleClass.date_class;
                        reserveClass.time_start = scheduleClass.time_start;
                        reserveClass.time_end = scheduleClass.time_end;
                        reserveClass.expire_date = scheduleClass.date_class + " " + scheduleClass.time_end + ":00";
                        reserveClass.EMP_NICKNAME = scheduleClass.EMP_NICKNAME;
                        reserveClass.person_join = scheduleClass.person_join;
                        reserveClass.unit = scheduleClass.unit;
                        reserveClass.EMP_CODE = scheduleClass.EMP_CODE;
                        reserveClass.image_class = scheduleClass.image_class;

                        if (reserveClass.person_join < reserveClass.unit)
                        {
                            labStatus.Visible = false;

                            //labClassName.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            pic.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labTime.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labTrain.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labUnit.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
                            panel.Tag = scheduleClass.id + "|" + scheduleClass.name_class + "|" + scheduleClass.date_class + "|" + scheduleClass.time_start + "|" + scheduleClass.time_end;
                        }
                        else
                        {
                            labStatus.Visible = true;
                            labStatus.Text = "เต็ม";
                            labStatus.BackColor = System.Drawing.Color.Red;
                            labStatus.ForeColor = System.Drawing.Color.WhiteSmoke;

                            pic.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labTime.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labTrain.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            labUnit.Click += new EventHandler((sender, e) => saveClass(sender, e, reserveClass));
                            panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
                            panel.Tag = scheduleClass.id + "|" + scheduleClass.name_class + "|" + scheduleClass.date_class + "|" + scheduleClass.time_start + "|" + scheduleClass.time_end;
                        }
                        
                    }
                    else
                    {
                        labStatus.Visible = true;
                        if (scheduleClass.sign_emp.Equals("Y"))
                        {
                            labStatus.Text = "บันทึกแล้ว";
                            labStatus.BackColor = System.Drawing.Color.PaleTurquoise;
                        }
                        else if (scheduleClass.sign_emp.Equals("C"))
                        {
                            labStatus.Text = "ยกเลิก Class";
                            labStatus.BackColor = System.Drawing.Color.Aquamarine;
                        }
                        else {
                            labStatus.Text = "รอการบันทึก";
                        }
                        labStatus.Click += new EventHandler((sender, e) => viewClass(sender, e, scheduleClass));
                        pic.Click += new EventHandler((sender, e) => viewClass(sender, e, scheduleClass));
                        labTime.Click += new EventHandler((sender, e) => viewClass(sender, e, scheduleClass));
                        labTrain.Click += new EventHandler((sender, e) => viewClass(sender, e, scheduleClass));
                        labUnit.Click += new EventHandler((sender, e) => viewClass(sender, e, scheduleClass));
                      
                        //panel.BackColor = System.Drawing.SystemColors.ButtonShadow;
                    }
                    //panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                    panel.Controls.Add(labStatus);
                    //panel.Controls.Add(labClassName);
                    panel.Controls.Add(labTime);
                    panel.Controls.Add(labTrain);
                    panel.Controls.Add(labUnit);
                    panel.Controls.Add(pic);
                    panel.Size = new System.Drawing.Size(150, 130);
                    panel.Margin = new System.Windows.Forms.Padding(0);
                    //panel.Dock = System.Windows.Forms.DockStyle.Fill;


                    int column = (scheduleClass.day - 1);
                    int row = (scheduleClass.row - 1);

                    tableLayoutPanel1.Controls.Add(panel, column, row);
                }
            }
            catch (Exception e)
            {


            }
            finally {
                tableLayoutPanel3.Visible = false;
                resetTimer();
                picLoading.Visible = false;
            }
        }

        private async void saveClass(object sender, EventArgs e, ReserveClass reserveClass)
        {
            frm_reserve_class fmc = new frm_reserve_class(reserveClass);
            var result = fmc.ShowDialog();
            if (result == DialogResult.OK)
            {
                reserveClass = fmc.reserveClass;
                reserveClass.person_join += 1;

                

                ApiRest.InitailizeClient();
                Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "ADD", reserveClass);
                if (ress != null & ress.status){
                    if (reserveClass.status.Equals("C"))
                    {
                        reserveClass.join_seq = int.Parse(ress.data);
                        Utils.eformReserveClass(reserveClass);
                    }
                    reserveList();
                    setDay(dateStart);
                }
            }
            else
            {
                if (fmc.active.Equals("CHANGE"))
                {
                    reserveList();
                    setDay(dateStart);
                }
                
            }
        }

        private void viewClass(object sender, EventArgs e, ScheduleClass sc)
        {
            frm_checkout_class fmc = new frm_checkout_class(sc);
            var result = fmc.ShowDialog();
            if (result == DialogResult.OK)
            {
                setDay(dateStart);
            }
        }


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("[ชื่อ - สกุล] like '%{0}%' or วันที่ like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("[ชื่อ - สกุล] like '%{0}%' or วันที่ like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            reserveList();
        }



        private async void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowindex = dataGridView1.CurrentCell.RowIndex;

                ReserveClass reserveClass = new ReserveClass();

                reserveClass.reserve_id         = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                reserveClass.schedule_day_id    = dataGridView1.Rows[rowindex].Cells[12].Value.ToString();
                reserveClass.name_class         = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                reserveClass.date_class         = dataGridView1.Rows[rowindex].Cells[13].Value.ToString();
                reserveClass.time_start         = dataGridView1.Rows[rowindex].Cells[14].Value.ToString();
                reserveClass.time_end           = dataGridView1.Rows[rowindex].Cells[15].Value.ToString();
                reserveClass.EMP_NICKNAME       = dataGridView1.Rows[rowindex].Cells[16].Value.ToString();
                reserveClass.PERSON_CODE        = dataGridView1.Rows[rowindex].Cells[17].Value.ToString();
                reserveClass.person_name        = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                reserveClass.status             = dataGridView1.Rows[rowindex].Cells[18].Value.ToString();
                reserveClass.unit               = int.Parse(dataGridView1.Rows[rowindex].Cells[19].Value.ToString());
                reserveClass.join_seq           = int.Parse(dataGridView1.Rows[rowindex].Cells[20].Value.ToString());

                //dtm.Columns.Add("   ", typeof(byte[]));0
                //dtm.Columns.Add("สถานะ", typeof(byte[]));1
                //dtm.Columns.Add("วันที่", typeof(string));2
                //dtm.Columns.Add("เวลา", typeof(string));3
                //dtm.Columns.Add("รายการ", typeof(string));4
                //dtm.Columns.Add("ชื่อ - สกุล", typeof(string));5
                //dtm.Columns.Add("ชื่อเล่น", typeof(string));6
                //dtm.Columns.Add("วันที่ทำรายการ", typeof(string));7
                //dtm.Columns.Add("หมดอายุ", typeof(string));8
                //dtm.Columns.Add("reserve_id", typeof(string));9
                //dtm.Columns.Add("ยกเลิก", typeof(byte[]));10
                //dtm.Columns.Add("schedule_day_id", typeof(string));11
                //dtm.Columns.Add("date_class", typeof(string));12
                //dtm.Columns.Add("time_start", typeof(string));13
                //dtm.Columns.Add("time_end", typeof(string));14
                //dtm.Columns.Add("EMP_NICKNAME", typeof(string));15
                //dtm.Columns.Add("PERSON_CODE", typeof(string));16

                if (e.ColumnIndex == dataGridView1.Columns["   "].Index)
                {
                    picLoading.Visible = true;
                    if (reserveClass.status.Equals("S"))
                    {
                        ApiRest.InitailizeClient();
                        Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "CHANGE", reserveClass);

                        Bitmap Image = MONKEY_APP.Properties.Resources.printer_tool30;

                        dataGridView1.Rows[rowindex].Cells[0].Value = ImageToByte(Image);
                        dataGridView1.Rows[rowindex].Cells[1].Value = "Check In";

                        reserveClass.join_seq = int.Parse(ress.data);
                        reserveList();

                    }
                    Utils.eformReserveClass(reserveClass);

                }

                if (dataGridView1.Columns["ยกเลิก"] != null && e.ColumnIndex == dataGridView1.Columns["ยกเลิก"].Index)
                {
                    if (!reserveClass.status.Equals("C") && !reserveClass.status.Equals("E")) {
                        frm_dialog d = new frm_dialog("ยืนยันการลบรายการ ! \r\nสมาชิก : " + reserveClass.person_name + "\r\nรายการ : " + reserveClass.name_class, "");
                        d.ShowDialog();
                        if (d.DialogResult == DialogResult.OK)
                        {
                            picLoading.Visible = true;
                            ApiRest.InitailizeClient();
                            Response ress = await ApiProcess.managerReserveClass(Global.BRANCH.branch_code, "DEL", reserveClass);
                            if (ress.status)
                            {
                                reserveList();
                                setDay(dateStart);
                            }
                        }

                    }
                    
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_CellClick ::" + ex.ToString(), "frm_home");
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            checkExpired();
            reserveList();
            setDay(dateStart);
            
        }

        private async void checkExpired()
        {
            try
            {
                ApiRest.InitailizeClient();
                Response ress = await ApiProcess.checkExpired();
               
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: checkExpired ::" + ex.ToString(), "frm_home");
            }
        }

            private void resetTimer() {
            timer1.Stop();
            timer1.Start();
        }
    }
}
