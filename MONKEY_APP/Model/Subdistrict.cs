﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Subdistrict
    {
        public string PROVINCE_CODE { get; set; }
        public string DISTRICT_CODE { get; set; }
        public string SUBDISTRICT_CODE { get; set; }
        public string SUBDISTRICT_NAME { get; set; }
        public string SUBDISTRICT_POSTAL { get; set; }
    }

    public class SubdistrictList
    {
        public List<Subdistrict> Subdistricts { get; set; }
    }
}
