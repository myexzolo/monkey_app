﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class PersonCheckin
    {
        public string id { get; set; }
        public string checkin_date { get; set; }
        public string company_code { get; set; }
        public string person_code { get; set; }
        public string package_person_id { get; set; }
        public string sign_person { get; set; }
        public string sign_emp { get; set; }
        public string sign_manager { get; set; }
        public string sign_person_date { get; set; }
        public string sign_emp_date { get; set; }
        public string sign_manager_date { get; set; }
        public string staus_checkin { get; set; }
        public string trainerCode { get; set; }
        public string managerCode { get; set; }
        public string pk_use { get; set; }
        public string use_pack { get; set; }
        //trans_package_person
        public string package_code { get; set; }
        public string package_name { get; set; }
        public string package_detail { get; set; }
        public string reg_no { get; set; }
        public string use_package { get; set; }
        public string num_use { get; set; }
        public string package_unit { get; set; }
        public string date_start { get; set; }
        public string date_expire { get; set; }
        public string max_use { get; set; }
        public string package_price { get; set; }
        public string package_num { get; set; }
        public string package_price_total { get; set; }
        public string percent_discount { get; set; }
        public string discount { get; set; }
        public string vat { get; set; }
        public string type_vat { get; set; }
        public string net_total { get; set; }
        public string notify_num { get; set; }
        public string notify_unit { get; set; }
        public string type_package { get; set; }
        public string status_notify { get; set; }
        public string trainer_code { get; set; }
        public string trainer_name { get; set; }
        public string invoice_code { get; set; }
        public string receipt_code { get; set; }
        public string status { get; set; }
        //person
        public string COMPANY_CODE { get; set; }
        public string PERSON_RUNNO { get; set; }
        public string PERSON_CODE { get; set; }
        public string PERSON_CODE_INT { get; set; }
        public string PERSON_CODE_OLD { get; set; }
        public string PERSON_TITLE { get; set; }
        public string PERSON_NICKNAME { get; set; }
        public string PERSON_NAME { get; set; }
        public string PERSON_LASTNAME { get; set; }
        public string PERSON_NAME_MIDDLE { get; set; }
        public string PERSON_TITLE_ENG { get; set; }
        public string PERSON_NAME_ENG { get; set; }
        public string PERSON_LASTNAME_ENG { get; set; }
        public string PERSON_SEX { get; set; }
        public string PERSON_BIRTH_DATE { get; set; }
        public string PERSON_CARD_ID { get; set; }
        public string PERSON_CARD_SDATE { get; set; }
        public string PERSON_CARD_EDATE { get; set; }
        public string PERSON_CARD_CREATE_BY { get; set; }
        public string PERSON_TEL_HOME { get; set; }
        public string PERSON_TEL_MOBILE { get; set; }
        public string PERSON_TEL_MOBILE2 { get; set; }
        public string PERSON_TEL_OFFICE { get; set; }
        public string PERSON_FAX { get; set; }
        public string PERSON_EMAIL { get; set; }
        public string PERSON_PRIORITY { get; set; }
        public string PERSON_TYPE { get; set; }
        public string PERSON_GROUP { get; set; }
        public string PERSON_STATUS { get; set; }
        public string PERSON_REGISTER_DATE { get; set; }
        public string PERSON_EXPIRE_DATE { get; set; }
        public string PERSON_NOTE { get; set; }
        public string PERSON_IMAGE { get; set; }
        // t_branch
        public string cname { get; set; }
    }
}
