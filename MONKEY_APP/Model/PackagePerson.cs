﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class PackagePerson
    {
        public string id { get; set; }
        public string company_code { get; set; }
        public string person_code { get; set; }
        public string package_id { get; set; }
        public string package_code { get; set; }
        public string package_name { get; set; }
        public string package_detail { get; set; }
        public string reg_no { get; set; }
        public int use_package { get; set; }
        public int num_use { get; set; }
        public string package_unit { get; set; }
        public string type_package { get; set; }
        private string dateStart;
        public string date_start
        {
            get { return dateStart; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        date_start_txt = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        date_start_txt = "";
                    }
                }
                dateStart = value;
            }

        }
        public string date_start_txt { get; set; }

        private string dateExpire { get; set; }
        public string date_expire
        {
            get { return dateExpire; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        date_expire_txt = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        date_expire_txt = "";
                    }
                }
                dateExpire = value;
            }

        }
        public string date_expire_txt { get; set; }
        public int max_use { get; set; }
        public string package_price { get; set; }
        public int package_num { get; set; }
        public string package_price_total { get; set; }
        public int percent_discount { get; set; }
        public string discount { get; set; }
        public string vat { get; set; }
        public string net_total { get; set; }
        public string trainer_code { get; set; }
        public string trainer_name { get; set; }
        public string invoice_code { get; set; }
        public string invoiceDateTxt { get; set; }
        private string invoiceDate;
        public string invoice_date
        {
            get { return invoiceDate; }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                else
                {
                    DateTime dDate;
                    if (DateTime.TryParse(value, out dDate))
                    {
                        string dm = dDate.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(dDate.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        invoiceDateTxt = dm + "/" + yy.ToString();
                    }
                    else
                    {
                        invoiceDateTxt = "";
                    }
                }
                invoiceDate = value;
            }

        }
        public string receiptCode { get; set; }
        public int notify_num { get; set; }
        public string notify_unit { get; set; }
        public string type_vat { get; set; }
        public string status { get; set; }
        public string create_by { get; set; }
        public int num_checkin { get; set; }
        public string person_name { get; set; }
        public string person_nickname { get; set; }
        public string emp_nickname { get; set; }
        public string type_buy { get; set; }
        public string sale_code { get; set; }

    }

    public class PackagePersons
    {
        public List<PackagePerson> PackagePersonList { get; set; }
    }
}
