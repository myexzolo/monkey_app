﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Province
    {
        public string PROVINCE_CODE { get; set; }
        public string PROVINCE_NAME { get; set; }
        public string PROVINCE_PART { get; set; }
    }

    public class ProvinceList
    {
        public List<Province> Provinces { get; set; }
    }
}
