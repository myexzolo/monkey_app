﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class MASTER
    {
        public string DATA_GROUP { get; set; }
        public string DATA_CODE { get; set; }
        public string DATA_DESC1 { get; set; }
        public string DATA_DESC2 { get; set; }
        public string DATA_TYPE { get; set; }
        public string DATA_SEQ { get; set; }
    }

    public class MASTERList
    {
        public List<MASTER> MASTERS { get; set; }
    }
}
