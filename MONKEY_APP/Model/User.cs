﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class User
    {
        public bool status  { get; set; }
        public string user_id { get; set; }
        public string user_login { get; set; }
        public string user_name { get; set; }
        public string user_last { get; set; }
        public string role_list { get; set; }
        public string user_img { get; set; }
        public string user_email { get; set; }
        public string login_id { get; set; }
        public string date_login{ get; set; }
        public string department_name { get; set; }
        public string role_access { get; set; }
        public string role_name { get; set; }
        public string emp_position { get; set; }
        public string EMP_IS_STAFF { get; set; }
        public string EMP_IS_TRAINER { get; set; }
        public string EMP_IS_SALE { get; set; }
        public string EMP_IS_INSTRUCTOR { get; set; }
    }

    public class UserList
    {
        public List<User> Users { get; set; }
    }
}
