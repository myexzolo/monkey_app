﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP.Model
{
    public class Package
    {
        public string package_id { get; set; }
        public string package_code { get; set; }
        public string package_name { get; set; }
        public string package_detail { get; set; }
        public string package_type { get; set; }
        public int num_use { get; set; }
        public string package_unit { get; set; }
        public int max_use { get; set; }
        public int package_price { get; set; }
        public string type_vat { get; set; }
        public string package_status { get; set; }
        public int notify_num { get; set; }
        public string notify_unit { get; set; }
        public int seq { get; set; }
        public string branch1 { get; set; }
        public string branch2 { get; set; }
        public string branch3 { get; set; }
        public string branch4 { get; set; }
        public string branch5 { get; set; }
    }
}
