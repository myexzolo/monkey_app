﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_transfers : Form
    {
        private ThaiIDCard idcard;
        public string typeActive;

        private string picCus = "";
        bool flagMB = true;

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_transfers()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private async void getCustomerById(string cus_code)
        {
            try
            {
                ApiRest.InitailizeClient();
                Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, cus_code);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                
            }

        }




        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
            return byteArr;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void frm_manage_transfers_Load(object sender, EventArgs e)
        {
            clearLab();
            labCode.Text = Global.PERSON.PERSON_CODE;
            labName.Text = Global.PERSON.PERSON_NAME + Global.PERSON.PERSON_NAME;
            labNickName.Text = Global.PERSON.PERSON_NICKNAME;
            labStatus.Text = Global.PERSON.PERSON_STATUS;

            if (Global.PERSON.PERSON_IMAGE != null && !Global.PERSON.PERSON_IMAGE.Equals(""))
            {
                Utils.setImage(Global.PERSON.PERSON_IMAGE, picPerson);
            }
            else
            {
                picCus = "";
                picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                picPerson.Size = new System.Drawing.Size(148, 178);
            }

            setPersonPackage(Global.PERSON.PERSON_CODE, dataGridView1, "T");
        }


        private async void setPersonPackage(string personCode,DataGridView dataGridView, string type)
        {

            dataGridView.Rows.Clear();
            try
            {
                ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPackageByPersonCode(personCode);

                DateTime dDate;

                flagMB = false;

                if (Global.USER.EMP_IS_STAFF.Equals("Y") && !flagMB)
                {
                    flagMB = true;
                }

                for (var i = 0; i < psList.Count; i++)
                {
                    PackagePerson ps = psList[i];

                    //string dstart = Utils.getDateEntoTh(ps.date_start);
                    //string dexp = Utils.getDateEntoTh(ps.date_expire);

                    string dstart = "";
                    string dexp = "";

                    if ((ps.num_checkin >= ps.num_use && ps.package_unit.Equals("TIMES")) || (float.Parse(ps.net_total) == 0 && !ps.type_package.Equals("MB")))
                    {
                        continue;
                    }

                    if (DateTime.TryParse(ps.date_start, out dDate))
                    {

                        DateTime ds = Convert.ToDateTime(ps.date_start);
                        string dm = ds.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(ds.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }

                        dstart = dm + "/" + yy.ToString();
                    }

                    if (DateTime.TryParse(ps.date_expire, out dDate))
                    {
                        DateTime de = Convert.ToDateTime(ps.date_expire);
                        string dm = de.ToString("dd/MM", new CultureInfo("th-TH"));
                        int yy = int.Parse(de.ToString("yyyy", new CultureInfo("th-TH")));

                        if (yy > 3000)
                        {
                            yy -= 543;
                        }
                        dexp = dm + "/" + yy.ToString();
                    }
                    //string dexpP    = Utils.getDateEntoTh(ps.date_expire_package);

                    bool chk = false;


                    if (ps.use_package > 0)
                    {
                        if (type.Equals("R"))
                        {
                            dataGridView.Rows.Add(chk, ps.package_name, dstart, dexp,
                            ps.use_package + "/" + ps.num_use, ps.package_unit, ps.reg_no, ps.invoice_code, ps.status, ps.package_detail, ps.invoice_date, ps.package_code, ps.id, ps.type_package, ps.use_package + "/" + ps.num_use);
                        }
                        else
                        {
                            dataGridView.Rows.Add(chk, ps.package_name, dstart, dexp,
                            ps.use_package, ps.use_package + "/" + ps.num_use, ps.package_unit, ps.reg_no, ps.invoice_code, ps.status, ps.package_detail, ps.invoice_date, ps.package_code, ps.id, ps.type_package);
                        }

                        if (ps.type_package.Equals("MB") && !flagMB)
                        {
                            flagMB = true;
                        }
                    }
                    dataGridView.Rows[i].Cells[0].ReadOnly = true;
                }

                if (flagMB && type.Equals("R")) {
                    int num1 = dataGridView1.Rows.Count;

                    for (int i = 0; i < num1; i++)
                    {
                        if (dataGridView1.Rows[i].Cells[14].Value.ToString().Equals("MB"))
                        {
                            dataGridView1.Rows[i].Cells[0].Value = false;
                            if (Global.USER.EMP_IS_STAFF.Equals("Y"))
                            {
                                dataGridView1.Rows[i].Cells[0].ReadOnly = false;
                            }
                            else
                            {
                                dataGridView1.Rows[i].Cells[0].ReadOnly = true;
                            }
                            
                        }
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonPackage ::" + ex.ToString(), "frm_manage_checkin");

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!textCode.Text.Equals("") && !textCode.Text.Equals(labCode.Text)) {
                clearLab();
                setPerson(textCode.Text);
            }
        }

        private void clearLab()
        {
            
            labNameR.Text = "";
            labNickNameR.Text = "";
            labStatusR.Text = "";

            picPersonR.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            picPersonR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            picPersonR.Size = new System.Drawing.Size(148, 178);
            
        }

        private async void setPerson(string personCode)
        {
            try
            {
                btnN.Enabled = false;
                btnP.Enabled = false;
                ApiRest.InitailizeClient();
                Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, personCode);
                if (person != null) {

                    btnN.Enabled = true;
                    btnP.Enabled = true;

                    labNameR.Text = person.PERSON_NAME + person.PERSON_NAME;
                    labNickNameR.Text = person.PERSON_NICKNAME;


                    if (person.PERSON_REGISTER_DATE != null && person.PERSON_EXPIRE_DATE != null && !person.PERSON_EXPIRE_DATE.Equals(""))
                    {
                        DateTime ds = DateTime.Parse(person.PERSON_REGISTER_DATE);
                        DateTime de = DateTime.Parse(person.PERSON_EXPIRE_DATE);
                        if (de > ds)
                        {
                            //this.labStatus.ForeColor = System.Drawing.Color.GreenYellow;
                            labStatusR.Text = "ACTIVE";
                        }
                        else
                        {
                            //this.labStatus.ForeColor = System.Drawing.Color.Red;
                            labStatusR.Text = "EXPIRE";
                        }
                    }
                    else
                    {
                        //this.labStatus.ForeColor = System.Drawing.Color.Yellow;
                        labStatusR.Text = "INACTIVE";
                    }

                    if (person.PERSON_IMAGE != null && !person.PERSON_IMAGE.Equals(""))
                    {
                        Utils.setImage(person.PERSON_IMAGE, picPersonR);
                    }
                    else
                    {
                        picPersonR.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                        picPersonR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                        picPersonR.Size = new System.Drawing.Size(148, 178);
                    }

                    setPersonPackage(person.PERSON_CODE, dataGridView2,"R");
                }

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonPackage ::" + ex.ToString(), "frm_manage_transfers");

            }
        }

        private void btnN_Click(object sender, EventArgs e)
        {
            int num1 = dataGridView1.Rows.Count;
            int num2 = dataGridView2.Rows.Count;

            int[] arrIdx = new int[num1];
            int idx = 0;

            for (int i = 0; i < num1; i++)
            {

                bool chk            = (bool)dataGridView1.Rows[i].Cells[0].Value;
                string package_name = dataGridView1.Rows[i].Cells[1].Value.ToString();
                string dstart       = dataGridView1.Rows[i].Cells[2].Value.ToString();
                string dexp         = dataGridView1.Rows[i].Cells[3].Value.ToString();
                string transfer     = dataGridView1.Rows[i].Cells[4].Value.ToString();
                string use          = dataGridView1.Rows[i].Cells[5].Value.ToString();
                string package_unit = dataGridView1.Rows[i].Cells[6].Value.ToString();
                string reg_no       = dataGridView1.Rows[i].Cells[7].Value.ToString();
                string invoice_code = dataGridView1.Rows[i].Cells[8].Value.ToString();
                string status       = dataGridView1.Rows[i].Cells[9].Value.ToString();
                string package_detail = dataGridView1.Rows[i].Cells[10].Value.ToString();
                string invoice_date     = dataGridView1.Rows[i].Cells[11].Value.ToString();
                string package_code     = dataGridView1.Rows[i].Cells[12].Value.ToString();
                string id               = dataGridView1.Rows[i].Cells[13].Value.ToString();
                string type             = dataGridView1.Rows[i].Cells[14].Value.ToString();

                if (chk)
                {
                    dataGridView2.Rows.Add(true, package_name, dstart, dexp,
                    transfer, package_unit, reg_no, invoice_code, status, package_detail, invoice_date, package_code, id, type, use);

                    arrIdx[idx] = (i+1);
                    idx++;

                }
            }

            if (arrIdx != null && arrIdx.Count() > 0) {
                Array.Sort(arrIdx);
                Array.Reverse(arrIdx);
                foreach (int value in arrIdx)
                {
                    if (value > 0)
                    {
                        dataGridView1.Rows.RemoveAt((value - 1));
                    }
                    
                }
            }
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            int num1 = dataGridView1.Rows.Count;
            int num2 = dataGridView2.Rows.Count;

            int[] arrIdx = new int[num2];
            int idx = 0;

            for (int i = 0; i < num2; i++)
            {

                bool chk = (bool)dataGridView2.Rows[i].Cells[0].Value;
                string package_name = dataGridView2.Rows[i].Cells[1].Value.ToString();
                string dstart = dataGridView2.Rows[i].Cells[2].Value.ToString();
                string dexp = dataGridView2.Rows[i].Cells[3].Value.ToString();
                string transfer = dataGridView2.Rows[i].Cells[4].Value.ToString();
                string use = dataGridView2.Rows[i].Cells[14].Value.ToString();
                string package_unit = dataGridView2.Rows[i].Cells[5].Value.ToString();
                string reg_no = dataGridView2.Rows[i].Cells[6].Value.ToString();
                string invoice_code = dataGridView2.Rows[i].Cells[7].Value.ToString();
                string status = dataGridView2.Rows[i].Cells[8].Value.ToString();
                string package_detail = dataGridView2.Rows[i].Cells[9].Value.ToString();
                string invoice_date = dataGridView2.Rows[i].Cells[10].Value.ToString();
                string package_code = dataGridView2.Rows[i].Cells[11].Value.ToString();
                string id = dataGridView2.Rows[i].Cells[12].Value.ToString();
                string type = dataGridView2.Rows[i].Cells[13].Value.ToString();

                if (chk)
                {
                    dataGridView1.Rows.Add(false, package_name, dstart, dexp,
                    transfer,use, package_unit, reg_no, invoice_code, status, package_detail, invoice_date, package_code, id, type);

                    arrIdx[idx] = (i + 1);
                    idx++;

                }
            }

            if (arrIdx != null && arrIdx.Count() > 0)
            {
                Array.Sort(arrIdx);
                Array.Reverse(arrIdx);

                foreach (int value in arrIdx)
                {
                    if (value > 0)
                    {
                        dataGridView2.Rows.RemoveAt((value - 1));
                    }

                }
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            try
            {

                String memberCode = labCode.Text;
                int num = dataGridView2.Rows.Count;

                for (int i = 0; i < num; i++)
                {
                    bool chk = (bool)dataGridView2.Rows[i].Cells[0].Value;
                    string id = dataGridView2.Rows[i].Cells[12].Value.ToString();
                    if (chk)
                    {
                        ApiRest.InitailizeClient();
                        Response res = await ApiProcess.manageTransferPackge(labCode.Text, id, textCode.Text, Global.USER.user_login);
                        string x = res.message;
                    }
                }
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button4_Click ::" + ex.ToString(), "frm_manage_transfers");
            }
            
        }
    }
}
