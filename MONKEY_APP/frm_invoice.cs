﻿using iTextSharp.text.pdf;
using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_invoice : Form
    {
        public Invoice invoice;
        public PackagePersons packagePersons;
        public string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();

        public string printerName = System.Configuration.ConfigurationManager.AppSettings["printerName"];


        public string typeVat = "INV";
        public string dateinv = "";
        public string flagPrint = "";
        public string typePaymentTxt = "";
        private bool chk = true;

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_invoice(Invoice invoice, PackagePersons packagePersons)
        {
            InitializeComponent();
            this.invoice        = invoice;
            this.packagePersons = packagePersons;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void invoice_Load(object sender, EventArgs e)
        {
            txtTotal.Text   = invoice.total_price;
            txtDis.Text     = invoice.discount;
            txtVat.Text     = invoice.vat;
            txtNet.Text     = invoice.total_net;

            txtName.Text        = invoice.name;
            txtAddress.Text     = invoice.address;
            txtTelOffice.Text   = invoice.tel;
            txtFax.Text         = invoice.fax;
            txtTexNo.Text       = invoice.tax;


            txtcash.Text        = invoice.cash.ToString("0.00");
            txttransfer.Text    = invoice.transfer.ToString("0.00");
            txtcredit.Text      = invoice.credit.ToString("0.00");
            txtcheque.Text      = invoice.cheque.ToString("0.00");

            labEmpName.Text     = Global.USER.user_name + " " + Global.USER.user_last;


            for (int x = 1; x <= 31; x++)
            {
                comboDate.Items.Add(new Item(x.ToString("00"), x.ToString("00")));
            }

            DateTime dateNow =  DateTime.Now;

            for (int monthNo = 1; monthNo <= 12; monthNo++)
            {
                var bar = new DateTime(dateNow.Year, monthNo, 1);
                comboMonth.Items.Add(new Item(bar.ToString("MMM", new CultureInfo("th-TH")), monthNo.ToString("00")));
            }

            int ey = int.Parse(dateNow.ToString("yyyy", new CultureInfo("th-TH"))) + 2;
            int sy = (ey - 4);
            for (int x = sy; x < ey; x++)
            {
                int yearEn = (x - 543);
                comboYear.Items.Add(new Item(x.ToString(), yearEn.ToString("0000")));
            }

            txtDate.Text = dateNow.ToString("yyyy/MM/dd");
            if (radioButton5.Checked)
            {
                btnSap.Enabled = false;
                btnSave.Enabled = false;

                typeVat = "INV";

                //Double totalPrice = Double.Parse(invoice.total_price);
                //Double discount = Double.Parse(invoice.discount);
                //Double totalpriceV = totalPrice / 1.07;
                //Double vat = totalPrice * 0.07;
                //Double total_net = (totalpriceV + vat) - discount;


                //txtTotal.Text = totalpriceV.ToString("#,###.00");
                //txtVat.Text = vat.ToString("#,###.00");
                //txtNet.Text = total_net.ToString("#,###.00");


                Double totalPrice   = Double.Parse(invoice.total_price);
                Double discount     = Double.Parse(invoice.discount);
                Double total        = (totalPrice - discount);
                Double totalpriceV = total / 1.07;
                Double vat       = (total*7)/107;
                Double total_net = (totalpriceV + vat);


                txtTotal.Text = totalpriceV.ToString("#,###.00");
                txtVat.Text = vat.ToString("#,###.00");
                txtNet.Text = total_net.ToString("#,###.00");

                genInvNo();
            }
            else
            {
                btnSap.Enabled = false;
                btnSave.Enabled = false;

                typeVat = "GM";

                txtTotal.Text = invoice.total_price;
                txtVat.Text = invoice.vat;
                txtNet.Text = invoice.total_net;

                genInvNo();
            }
        }

        private void changeDate(object sender, EventArgs e)
        {
            try
            {
                DateTime dN = Convert.ToDateTime(txtDate.Value.ToString());
                int yy = int.Parse(dN.ToString("yyyy"));
                string mm = dN.ToString("MM");
                string dd = dN.ToString("dd");

                if (comboDate.SelectedItem != null)
                {
                    dd = (comboDate.SelectedItem as Item).Value.ToString();
                }
                if (comboMonth.SelectedItem != null)
                {
                    mm = (comboMonth.SelectedItem as Item).Value.ToString();
                }

                if (comboYear.SelectedItem != null)
                {
                    yy = int.Parse(comboYear.Text);
                }

                string D = yy.ToString("0000") + "/" + mm + "/" + dd;
                txtDate.Text = D;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private async void genInvNo()
        {
            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.genInvNo(Global.BRANCH.branch_code, Global.BRANCH.branch_id, txtDate.Value.ToString("yyyy-MM-dd"), typeVat);
                invCode.Text = res.message;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: genInvNo ::" + ex.ToString(), "frm_invoice");
            }
            finally {
                btnSap.Enabled = true;
                btnSave.Enabled = true;
            }
        }

        private void txtRegDate_ValueChanged(object sender, EventArgs e)
        {
            genInvNo();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private float setBal() {

            float net = float.Parse(txtNet.Text.Replace(",", ""));
            float bal = 0;
            try
            {
                if (checkBox1.Checked)
                {
                    if (!txtcash.Text.Equals(""))
                    {
                        float cash = float.Parse(txtcash.Text.Replace(",", ""));
                        bal += cash;
                    }  
                }

                if (checkBox2.Checked)
                {
                    if (!txttransfer.Text.Equals(""))
                    {
                        float transfer = float.Parse(txttransfer.Text.Replace(",", ""));
                        bal += transfer;
                    }
                }

                if (checkBox3.Checked)
                {
                    if (!txtcredit.Text.Equals(""))
                    {
                        float credit = float.Parse(txtcredit.Text.Replace(",", ""));
                        bal += credit;
                    }
                }

                if (checkBox4.Checked)
                {
                    if (!txtcredit.Text.Equals(""))
                    {
                        float cheque = float.Parse(txtcheque.Text.Replace(",", ""));
                        bal += cheque;
                    }
                }
                if (bal == net) {
                    textBal.ForeColor = System.Drawing.Color.Black;
                }
                else {
                    textBal.ForeColor = System.Drawing.Color.Red;
                }
                textBal.Text = bal.ToString("#,##0.00");
                return bal;
            }
            catch (Exception ex)
            { 
                Utils.getErrorToLog(":: setBal ::" + ex.ToString(), "frm_invoice");
                return bal;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                float bal = setBal();
                float net = float.Parse(txtNet.Text.Replace(",", ""));

                if (bal == net)
                {
                    setInvoice();
                    alertTxt.Visible = false;
                    alertTxt.Text = "";
                    //Utils.eformReceipt(typeVat, invoice, packagePersons, txtDate.Value.ToString("dd/MM/yyyy"));

                    

                    if (invoice.cash > 0)
                    {
                        typePaymentTxt = "เงินสด";
                    }

                    if (invoice.transfer > 0)
                    {
                        if (typePaymentTxt.Equals(""))
                        {
                            typePaymentTxt = "โอนเงิน";
                        }
                        else
                        {
                            typePaymentTxt += ", โอนเงิน";
                        }
                    }

                    if (invoice.credit > 0)
                    {
                        if (typePaymentTxt.Equals(""))
                        {
                            typePaymentTxt = "เครดิต";
                        }
                        else
                        {
                            typePaymentTxt += ", เครดิต";
                        }
                    }

                    if (invoice.cheque > 0)
                    {
                        if (typePaymentTxt.Equals(""))
                        {
                            typePaymentTxt = "เช็คเงินสด";
                        }
                        else
                        {
                            typePaymentTxt += ", เช็คเงินสด";
                        }
                    }

                    //for (int i = 0; i < packagePersons.PackagePersonList.Count; i++)
                    //{
                    //    PackagePerson ps = packagePersons.PackagePersonList[i];
                    //
                    //    ApiRest.InitailizeClient();
                    //    Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, ps.person_code);

                    //    if (ps.type_package.Equals("MB"))
                    //    {
                    //        Utils.eformContract(ps, person, invoice.invoice_date, typePaymentTxt);
                    //    }
                    //    else if (ps.type_package.Equals("PT"))
                    //    {
                    //        Utils.eformContractPT(ps, person, invoice.invoice_date, typePaymentTxt);
                    //   }
                    //}
                    flagPrint = "PRINT";
                    dateinv = txtDate.Value.ToString("dd/MM/yyyy");
                    this.DialogResult = DialogResult.OK;
                }
                else if (bal == 0)
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = "กรอกจำนวนเงินตามประเภทการจ่ายเงิน !!";
                }
                else
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = "ยอดเงินประเภทการจ่ายเงินไม่เท่ากับยอดเงินสุทธิ !!";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: button4_Click ::" + ex.ToString(), "frm_invoice");
                this.DialogResult = DialogResult.Cancel;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            float bal = setBal();
            float net = float.Parse(txtNet.Text.Replace(",", ""));


            if (bal == net)
            {
                setInvoice();
                alertTxt.Visible = false;
                alertTxt.Text = "";
                flagPrint = "SAVE";
                dateinv = txtDate.Value.ToString("dd/MM/yyyy");
                this.DialogResult = DialogResult.OK;
            }
            else if (bal == 0)
            {
                alertTxt.Visible = true;
                alertTxt.Text = "กรอกจำนวนเงินตามประเภทการจ่ายเงิน !!";
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = "ยอดเงินประเภทการจ่ายเงินไม่เท่ากับยอดเงินสุทธิ !!";
            }
            
        }




        private void setInvoice() {

            invoice.total_price = txtTotal.Text;
            invoice.discount    = txtDis.Text;
            invoice.vat         = txtVat.Text;
            invoice.total_net   = txtNet.Text;

            invoice.invoice_code    = invCode.Text;
            invoice.invoice_date    = txtDate.Value.ToString("yyyy-MM-dd");
            invoice.name            = txtName.Text;
            invoice.address         = txtAddress.Text;
            invoice.tel             = txtTelOffice.Text;
            invoice.fax             = txtFax.Text;
            invoice.tax             = txtTexNo.Text;
            invoice.cash            = float.Parse(txtcash.Text);
            invoice.transfer        = float.Parse(txttransfer.Text);
            invoice.credit          = float.Parse(txtcredit.Text);
            invoice.cheque          = float.Parse(txtcheque.Text);



            if (radioButton6.Checked)
            {
                invoice.type_vat = radioButton6.Tag.ToString();
                invoice.type_payment = "GM";
            }
            if (radioButton5.Checked)
            {
                invoice.type_vat = radioButton5.Tag.ToString();
                invoice.type_payment = "INV";
            }
        }

        private void radioButton6_Click(object sender, EventArgs e)
        {
            btnSap.Enabled  = false;
            btnSave.Enabled = false;

            typeVat = "GM";

            txtTotal.Text   = invoice.total_price;
            txtVat.Text     = invoice.vat;
            txtNet.Text     = invoice.total_net;

            genInvNo();
        }

        private void radioButton5_Click(object sender, EventArgs e)
        {
            btnSap.Enabled = false;
            btnSave.Enabled = false;

            typeVat = "INV";

            Double totalPrice   = Double.Parse(invoice.total_price);
            Double discount     = Double.Parse(invoice.discount);
            Double totalpriceV  = totalPrice / 1.07;
            Double vat          = totalpriceV * 0.07;
            Double total_net    = (totalpriceV + vat) - discount;

            txtTotal.Text   = totalpriceV.ToString("#,###.00");
            txtVat.Text     = vat.ToString("#,###.00");
            txtNet.Text     = total_net.ToString("#,###.00");

            genInvNo();
        }


        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            setBal();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtcash.Enabled = true;
                txtcash.BackColor = System.Drawing.Color.White;
                if (chk)
                {
                    float bal = setBal();
                    float net = float.Parse(txtNet.Text.Replace(",", ""));
                    chk = false;
                    txtcash.Text = net.ToString("0.00");
                }
                else
                {
                    txtcash.Text = "";
                }
            }
            else
            {
                float bal = setBal();
                if (bal==0)
                {
                    chk = true;
                }
                txtcash.Enabled = false;
                txtcash.BackColor = System.Drawing.Color.LightGray;
                txtcash.Text    = "0.00";
            }
            txtcash.Focus();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                txttransfer.Enabled = true;
                txttransfer.BackColor = System.Drawing.Color.White;
                if (chk)
                {
                    chk = false;
                    txttransfer.Text = invoice.total_net;
                }
                else
                {
                    txttransfer.Text = "";
                }
            }
            else
            {
                float bal = setBal();
                if (bal == 0)
                {
                    chk = true;
                }
                txttransfer.Enabled = false;
                txttransfer.BackColor = System.Drawing.Color.LightGray;
                txttransfer.Text = "0.00";
            }
            txttransfer.Focus();
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                txtcredit.Enabled = true;
                txtcredit.BackColor = System.Drawing.Color.White;

                if (chk)
                {
                    chk = false;
                    txtcredit.Text = invoice.total_net;
                }
                else
                {
                    txtcredit.Text = "";
                }
            }
            else
            {
                float bal = setBal();
                if (bal == 0)
                {
                    chk = true;
                }
                txtcredit.Enabled = false;
                txtcredit.BackColor = System.Drawing.Color.LightGray;
                txtcredit.Text = "0.00";
            }
            txtcredit.Focus();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                txtcheque.Enabled = true;
                txtcheque.BackColor = System.Drawing.Color.White;

                if (chk)
                {
                    chk = false;
                    txtcheque.Text = invoice.total_net;
                }
                else
                {
                    txtcheque.Text = "";
                }
            }
            else
            {
                float bal = setBal();
                if (bal == 0)
                {
                    chk = true;
                }
                txtcheque.Enabled = false;
                txtcheque.BackColor = System.Drawing.Color.LightGray;
                txtcheque.Text = "0.00";
            }
            txtcheque.Focus();
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void txttransfer_TextChanged(object sender, EventArgs e)
        {
            setBal();
        }

        private void txtDate_ValueChanged(object sender, EventArgs e)
        {
            DateTime dN = Convert.ToDateTime(txtDate.Value.ToString());
            comboDate.Text = dN.ToString("dd");
            comboMonth.Text = dN.ToString("MMM", new CultureInfo("th-TH"));
            int yy = int.Parse(dN.ToString("yyyy", new CultureInfo("th-TH")));

            if (yy > 3000)
            {
                yy -= 543;
            }
            if (yy > 2400)
            {
                yy -= 543;
            }

            comboYear.Text = yy.ToString("0000");
            genInvNo();
        }
    }
}
