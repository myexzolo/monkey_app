﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP
{
    public class ApiRest
    {
        public static HttpClient ApiClient { get; set; } = new HttpClient();
        //public static string BaseAddress  = "http://localhost/gym/service/";
        public static string BaseAddress    = "https://gymmonkeybkk.com/admin/service/";
        //public static string BaseAddress  = "http://gymmonkey.onesittichok.co.th/service/";
        //public static string BaseAddress  = "http://onesittichok.com/service/";

        public static void InitailizeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        } 
    }
}
