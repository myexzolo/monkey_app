﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetFwTypeLib;

namespace MONKEY_APP
{
    public class Ftpfile
    {
        private static string firewallid = "{304CE942-6E39-40D8-943A-B913C40C9CD4}"; 

        public static void downloadFile(string localDestnDir)
        {
            

            try
            {
                string firewallStatus = firewallstatus();

                string server = "ftp://gymmonkeybkk.com:21/software";
                string fileName = "GYM Monkey.exe";
                string ftpUserName = "gymmon";
                string ftpPassWord = "8z2xHYu99z";
                

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}/{1}", server, fileName)));

                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(ftpUserName, ftpPassWord);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();
                string path = localDestnDir + "\\temp\\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string pathFile = path + fileName;
                if (File.Exists(pathFile))
                {
                    File.Delete(pathFile);
                }

                FileStream writeStream = new FileStream(pathFile, FileMode.CreateNew);
                int Length = 2048;
                Byte[] buffer = new Byte[Length];
                int bytesRead = responseStream.Read(buffer, 0, Length);
                while (bytesRead > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                    bytesRead = responseStream.Read(buffer, 0, Length);
                }
                writeStream.Close();
                response.Close();

                String programUpdateVersion = localDestnDir + @"\UPDATE_APP.exe";
                Process.Start(programUpdateVersion);

                string RunningProcess = Process.GetCurrentProcess().ProcessName;
                foreach (Process proc in Process.GetProcessesByName(RunningProcess))
                {
                    proc.Kill();
                }
            }
            catch (WebException wEx)
            {
                //addFirewall("GYM Monkey App", "2121");
                MessageBox.Show(wEx.Message +" ::: "+ ((FtpWebResponse)wEx.Response).StatusDescription, "Download Error wEx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Download Error ex");
            }
        }

        public static void addFirewall(string ruleName, string port) {

            try
            {
                if (ruleName != null && !ruleName.Equals("")) {
                    Type tNetFwPolicy2 = Type.GetTypeFromProgID("HNetCfg.FwPolicy2");
                    INetFwPolicy2 fwPolicy2 = (INetFwPolicy2)Activator.CreateInstance(tNetFwPolicy2);
                    var currentProfiles = fwPolicy2.CurrentProfileTypes;

                    //create new rule
                    INetFwRule2 inboundRule = (INetFwRule2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                    inboundRule.Enabled = true;
                    //Allow Through firewall
                    inboundRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                    //setup Protocal.... TCP in this instance
                    inboundRule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
                    //setup Port
                    inboundRule.LocalPorts = port;
                    //setup name
                    inboundRule.Name = ruleName;
                    //profile
                    inboundRule.Profiles = currentProfiles;
                    //New add the rule
                    INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                    firewallPolicy.Rules.Add(inboundRule);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static NetFwTypeLib.INetFwMgr FirewallManager()
        {
            Type objectType = Type.GetTypeFromCLSID(new Guid(firewallid));
            return Activator.CreateInstance(objectType) as NetFwTypeLib.INetFwMgr;
        }

        public static string firewallstatus()
        {
            INetFwMgr manager = FirewallManager();
            bool isFirewallEnabled = manager.LocalPolicy.CurrentProfile.FirewallEnabled;
            if (isFirewallEnabled)
                return "ON";
            else
                return "OFF";
        }

    }
}
