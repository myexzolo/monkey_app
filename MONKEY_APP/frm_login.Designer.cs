﻿namespace MONKEY_APP
{
    partial class frm_login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_login));
            this.passwordText = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.alertTxt = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labVersions = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TextSensorSN = new System.Windows.Forms.TextBox();
            this.TextSensorIndex = new System.Windows.Forms.TextBox();
            this.TextSensorCount = new System.Windows.Forms.TextBox();
            this.ZKFPEngX1 = new AxZKFPEngXControl.AxZKFPEngX();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.branchName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.acceptBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // passwordText
            // 
            this.passwordText.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.passwordText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passwordText.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.passwordText.Location = new System.Drawing.Point(307, 273);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.Size = new System.Drawing.Size(290, 32);
            this.passwordText.TabIndex = 2;
            this.passwordText.TextChanged += new System.EventHandler(this.passwordText_TextChanged);
            // 
            // username
            // 
            this.username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.username.Font = new System.Drawing.Font("TH Sarabun New", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.username.Location = new System.Drawing.Point(307, 191);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(290, 32);
            this.username.TabIndex = 1;
            this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(265, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 27);
            this.label2.TabIndex = 99;
            this.label2.Text = "รหัสผ่าน";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(265, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 27);
            this.label1.TabIndex = 98;
            this.label1.Text = "ชื่อผู้ใช้งาน ";
            // 
            // alertTxt
            // 
            this.alertTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alertTxt.BackColor = System.Drawing.Color.Transparent;
            this.alertTxt.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.alertTxt.ForeColor = System.Drawing.Color.Red;
            this.alertTxt.Location = new System.Drawing.Point(265, 320);
            this.alertTxt.MinimumSize = new System.Drawing.Size(270, 0);
            this.alertTxt.Name = "alertTxt";
            this.alertTxt.Size = new System.Drawing.Size(343, 28);
            this.alertTxt.TabIndex = 104;
            this.alertTxt.Text = "alert";
            this.alertTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.alertTxt.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(874, 84);
            this.panel1.TabIndex = 105;
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.Transparent;
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(842, 12);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 113;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.Logo_yms;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 55);
            this.pictureBox1.TabIndex = 105;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Controls.Add(this.labVersions);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 549);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(874, 62);
            this.panel2.TabIndex = 106;
            // 
            // labVersions
            // 
            this.labVersions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labVersions.AutoSize = true;
            this.labVersions.BackColor = System.Drawing.Color.Transparent;
            this.labVersions.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labVersions.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labVersions.Location = new System.Drawing.Point(816, 23);
            this.labVersions.Name = "labVersions";
            this.labVersions.Size = new System.Drawing.Size(48, 22);
            this.labVersions.TabIndex = 98;
            this.labVersions.Text = "V.1.0.1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TextSensorSN);
            this.panel3.Controls.Add(this.TextSensorIndex);
            this.panel3.Controls.Add(this.TextSensorCount);
            this.panel3.Controls.Add(this.ZKFPEngX1);
            this.panel3.Controls.Add(this.picLoading);
            this.panel3.Controls.Add(this.passwordText);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.username);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.branchName);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.alertTxt);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.acceptBtn);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(874, 465);
            this.panel3.TabIndex = 107;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Image = global::MONKEY_APP.Properties.Resources.fingerprint__1_;
            this.pictureBox5.Location = new System.Drawing.Point(613, 172);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(133, 148);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 133;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(573, 96);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 132;
            this.label4.Text = "จำนวนหัวอ่าน";
            this.label4.Visible = false;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(554, 120);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(89, 25);
            this.label8.TabIndex = 130;
            this.label8.Text = "Sensor SN";
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(754, 96);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(49, 17);
            this.label9.TabIndex = 131;
            this.label9.Text = "หัวอ่าน:";
            this.label9.Visible = false;
            // 
            // TextSensorSN
            // 
            this.TextSensorSN.AcceptsReturn = true;
            this.TextSensorSN.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorSN.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorSN.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorSN.Location = new System.Drawing.Point(649, 120);
            this.TextSensorSN.MaxLength = 0;
            this.TextSensorSN.Name = "TextSensorSN";
            this.TextSensorSN.ReadOnly = true;
            this.TextSensorSN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorSN.Size = new System.Drawing.Size(217, 20);
            this.TextSensorSN.TabIndex = 127;
            this.TextSensorSN.Visible = false;
            // 
            // TextSensorIndex
            // 
            this.TextSensorIndex.AcceptsReturn = true;
            this.TextSensorIndex.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorIndex.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorIndex.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorIndex.Location = new System.Drawing.Point(809, 96);
            this.TextSensorIndex.MaxLength = 0;
            this.TextSensorIndex.Name = "TextSensorIndex";
            this.TextSensorIndex.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorIndex.Size = new System.Drawing.Size(57, 20);
            this.TextSensorIndex.TabIndex = 128;
            this.TextSensorIndex.Visible = false;
            // 
            // TextSensorCount
            // 
            this.TextSensorCount.AcceptsReturn = true;
            this.TextSensorCount.BackColor = System.Drawing.SystemColors.Window;
            this.TextSensorCount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSensorCount.Enabled = false;
            this.TextSensorCount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TextSensorCount.Location = new System.Drawing.Point(649, 96);
            this.TextSensorCount.MaxLength = 0;
            this.TextSensorCount.Name = "TextSensorCount";
            this.TextSensorCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextSensorCount.Size = new System.Drawing.Size(65, 20);
            this.TextSensorCount.TabIndex = 129;
            this.TextSensorCount.Visible = false;
            // 
            // ZKFPEngX1
            // 
            this.ZKFPEngX1.Enabled = true;
            this.ZKFPEngX1.Location = new System.Drawing.Point(838, 17);
            this.ZKFPEngX1.Name = "ZKFPEngX1";
            this.ZKFPEngX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("ZKFPEngX1.OcxState")));
            this.ZKFPEngX1.Size = new System.Drawing.Size(24, 24);
            this.ZKFPEngX1.TabIndex = 125;
            this.ZKFPEngX1.OnImageReceived += new AxZKFPEngXControl.IZKFPEngXEvents_OnImageReceivedEventHandler(this.ZKFPEngX1_OnImageReceived);
            this.ZKFPEngX1.OnCapture += new AxZKFPEngXControl.IZKFPEngXEvents_OnCaptureEventHandler(this.ZKFPEngX1_OnCapture);
            // 
            // picLoading
            // 
            this.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picLoading.BackColor = System.Drawing.Color.White;
            this.picLoading.Image = global::MONKEY_APP.Properties.Resources.loading2;
            this.picLoading.Location = new System.Drawing.Point(383, 184);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(109, 96);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoading.TabIndex = 124;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Image = global::MONKEY_APP.Properties.Resources.lock1;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(265, 268);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(343, 43);
            this.label6.TabIndex = 106;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Image = global::MONKEY_APP.Properties.Resources.user1;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(265, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(343, 43);
            this.label5.TabIndex = 105;
            // 
            // branchName
            // 
            this.branchName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.branchName.BackColor = System.Drawing.Color.Transparent;
            this.branchName.Font = new System.Drawing.Font("TH SarabunPSK", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.branchName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.branchName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.branchName.Location = new System.Drawing.Point(3, 96);
            this.branchName.Name = "branchName";
            this.branchName.Size = new System.Drawing.Size(868, 41);
            this.branchName.TabIndex = 98;
            this.branchName.Text = "GYM MONKEY";
            this.branchName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(265, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(343, 41);
            this.label3.TabIndex = 98;
            this.label3.Text = "โปรแกรมบริการจัดการฟิตเนส";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::MONKEY_APP.Properties.Resources.security1;
            this.button1.Location = new System.Drawing.Point(440, 369);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 54);
            this.button1.TabIndex = 102;
            this.button1.Text = "สแกนนิ้ว";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // acceptBtn
            // 
            this.acceptBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.acceptBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            this.acceptBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.acceptBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.acceptBtn.FlatAppearance.BorderSize = 0;
            this.acceptBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acceptBtn.Font = new System.Drawing.Font("TH SarabunPSK", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.acceptBtn.ForeColor = System.Drawing.Color.White;
            this.acceptBtn.Image = global::MONKEY_APP.Properties.Resources.security2;
            this.acceptBtn.Location = new System.Drawing.Point(265, 369);
            this.acceptBtn.Name = "acceptBtn";
            this.acceptBtn.Size = new System.Drawing.Size(166, 54);
            this.acceptBtn.TabIndex = 102;
            this.acceptBtn.Text = "เข้าสู่ระบบ";
            this.acceptBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.acceptBtn.UseVisualStyleBackColor = false;
            this.acceptBtn.Click += new System.EventHandler(this.acceptBtn_Click);
            // 
            // frm_login
            // 
            this.AcceptButton = this.acceptBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(874, 611);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_login";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_login_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZKFPEngX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button acceptBtn;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label alertTxt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label branchName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labVersions;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox picLoading;
        public AxZKFPEngXControl.AxZKFPEngX ZKFPEngX1;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox TextSensorSN;
        public System.Windows.Forms.TextBox TextSensorIndex;
        public System.Windows.Forms.TextBox TextSensorCount;
        internal System.Windows.Forms.PictureBox pictureBox5;
    }
}