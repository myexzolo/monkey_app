﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class Frm_bill_invoice : Form
    {
        public Frm_bill_invoice()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void label45_Click(object sender, EventArgs e)
        {

        }

        void PrintImage(object o, PrintPageEventArgs e)
        {
            int width = this.Width;
            int height = this.Height;

            //int width = 1240;
            //int height = 1754;

            Graphics g1 = this.CreateGraphics();

            int newW = Convert.ToInt32(width * 0.68);
            int newH = Convert.ToInt32(height * 0.68);

            Rectangle bounds = new Rectangle(0, 0, width, height);
            Rectangle Newbounds = new Rectangle(0, 0, newW, newH);
            Bitmap img = new Bitmap(width, height, g1);
            this.DrawToBitmap(img, bounds);

            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.DrawImage(img, Newbounds, 0, 0, Width, Height, System.Drawing.GraphicsUnit.Pixel);
        }



        private void Frm_bill_invoice_Shown(object sender, EventArgs e)
        {
            PrintDocument print = new PrintDocument();
            print.PrintPage += new PrintPageEventHandler(PrintImage);
            print.Print();
            this.Close();



        }

        private void label15_Click(object sender, EventArgs e)
        {

        }
    }
}
