﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_package : Form
    {
        public string typeActive;

        public string branchCode = System.Configuration.ConfigurationManager.AppSettings["BranchCode"];

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_package(string typeActive)
        {
            InitializeComponent();
            this.typeActive = typeActive;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void setEnable(bool flag)
        {
            txtName.Enabled = flag;
            txtDetail.Enabled = flag;
            numMaxUse.Enabled = flag;
            comboUnit.Enabled = flag;
            numMaxday.Enabled = flag;
            numPrice.Enabled = flag;
            numericNotify.Enabled = flag;
            comboUnit2.Enabled = flag;
            numSeq.Enabled = flag;
            comboStatus.Enabled = flag;
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            Refresh();

            var thread1 = new Thread(showloading);
            thread1.Start();
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else {
                picLoading.Visible = true;
            }
            
        }



        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void setAlertMessage(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
            }
        }

        private void frm_manage_package_Load(object sender, EventArgs e)
        {
            
            for (int x = 0; x < Global.TIME_UNIT.Count; x++) {
                MASTER master = Global.TIME_UNIT[x];
                comboUnit.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                comboUnit2.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
            }


            comboStatus.Items.Add(new Item("ใช้งาน", "Y"));
            comboStatus.Items.Add(new Item("ไม่ใช้งาน", "N"));


            //comboTypePackage.Items.Add(new Item("Member", "MB"));
            //comboTypePackage.Items.Add(new Item("Personal Trainer", "PT"));

            if (typeActive.Equals("ADD"))
            {
                //comboTypePackage.SelectedIndex = 0;
                setEnable(true);
                comboStatus.SelectedIndex = 0;

                if (branchCode.Equals("GYMMK01"))
                {
                    chkbranch1.Checked = true;
                    chkbranch2.Checked = true;
                }
                else if (branchCode.Equals("GYMMK02"))
                {
                    chkbranch2.Checked = true;
                }
                else if (branchCode.Equals("GYMMK03"))
                {
                    //chkbranch3.Checked = true;
                }
                else if (branchCode.Equals("GYMMK04"))
                {
                    //chkbranch4.Checked = true;
                }
                else if (branchCode.Equals("GYMMK05"))
                {
                    //chkbranch5.Checked = true;
                }
                //comboTypePackage.Enabled = true;
            }
            else if (typeActive.Equals("EDIT"))
            {
                setEnable(true);
                TypePackagePT.Enabled = false;
                TypePackageMem.Enabled = false; 
                label1.Text = "แก้ไขแพคเกจ/สินค้า";
                getPackageById(Global.PACKAGE.package_id);
            }
        }

        private async void getPackageById(string id)
        {
            try
            {
                ApiRest.InitailizeClient();
                Package package = await ApiProcess.getPackageById(id);


                
                packageId.Text = package.package_id;
                packageCode.Text = package.package_code;
                txtName.Text = package.package_name;
                txtDetail.Text = package.package_detail;              
                numPrice.Value = (int)package.package_price;
                numericNotify.Value = package.notify_num;
                numSeq.Value      = (int)package.seq;



                if (package.branch1.Equals("Y"))
                {
                    chkbranch1.Checked = true;
                }

                if (package.branch2.Equals("Y"))
                {
                    chkbranch2.Checked = true;
                }

                if (package.branch3.Equals("Y"))
                {
                    //chkbranch3.Checked = true;
                }

                if (package.branch4.Equals("Y"))
                {
                    //chkbranch4.Checked = true;
                }

                if (package.branch5.Equals("Y"))
                {
                    //chkbranch5.Checked = true;
                }


                if (package.package_status.Equals("Y")) {
                    comboStatus.SelectedIndex = 0;

                }
                else if (package.package_status.Equals("N"))
                {
                    comboStatus.SelectedIndex = 1;
                }

                if (package.package_type.Equals("MB"))
                {
                    //comboTypePackage.SelectedIndex = 0;
                    TypePackageMem.Checked = true;

                }
                else if (package.package_type.Equals("PT"))
                {
                    //comboTypePackage.SelectedIndex = 1;
                    TypePackagePT.Checked = true;
                }

                for (int x = 0; x < Global.TIME_UNIT.Count; x++)
                {
                    MASTER master = Global.TIME_UNIT[x];
                    if (package.package_unit == master.DATA_CODE)
                    {
                        comboUnit.SelectedIndex = x;
                    }
                    if (package.notify_unit == master.DATA_CODE)
                    {
                        comboUnit2.SelectedIndex = x;
                    }
                }

                if ((int)package.max_use > 0)
                {
                    numMaxday.Value = (int)package.max_use;
                }
                
                numMaxUse.Value = (int)package.num_use;

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPackageById ::" + ex.ToString(), "frm_manage_package");
            }

        }

        private async void genPackageCode(String package_type)
        {

            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.genPackageCode(package_type);
                packageCode.Text = res.message;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void comboTypePackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            Item item = (Item)cmb.SelectedItem;

            if (typeActive.Equals("ADD"))
            {
                genPackageCode(item.Value);
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var thread1 = new Thread(showloading);

                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                if (!TypePackagePT.Checked && !TypePackageMem.Checked)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ ประเภทเพคเกจ **");
                }
                else if (txtName.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อ **");
                    txtName.Focus();
                }
                else if (numMaxUse.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอก จำนวนการใช้งาน **");
                    numMaxUse.Focus();
                }
                else if (comboUnit.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ หน่วย **");
                    comboUnit.Focus();
                }
                else if (numMaxday.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอก อายุการใช้งาน **");
                    numMaxday.Focus();
                }
                else if (numPrice.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ ราคา **");
                    numPrice.Focus();
                }
                else if (comboStatus.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอก สถานะการใช้งาน **");
                    comboStatus.Focus();
                }
                else if (numericNotify.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอก จำนวนการแจ้งเตือน **");
                    numericNotify.Focus();
                }
                else if (comboUnit2.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ หน่วย **");
                    comboUnit2.Focus();
                }
                else if (!chkbranch1.Checked && !chkbranch2.Checked)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาเลือกสาขา **");
                }
                else
                {
                    Package package = new Package();
                    thread1.Start();
                    string packageType = "";
                    if (TypePackageMem.Checked)
                    {
                        packageType = "MB";
                    }
                    else if (TypePackagePT.Checked)
                    {
                        packageType = "PT";
                    }

                    string branch1 = "";
                    string branch2 = "";
                    string branch3 = "";
                    string branch4 = "";
                    string branch5 = "";
                    if (chkbranch1.Checked)
                    {
                        branch1 = "Y";
                    }
                    if (chkbranch2.Checked)
                    {
                        branch2 = "Y";
                    }

                    package.package_id      = packageId.Text;
                    package.package_code    = packageCode.Text;
                    package.package_name    = txtName.Text;
                    package.package_detail  = txtDetail.Text;
                    package.package_type    = packageType;
                    package.num_use         = (int)numMaxUse.Value;
                    package.package_unit    = (comboUnit.SelectedItem as Item).Value.ToString();
                    package.max_use         = (int)numMaxday.Value;
                    package.package_price   = (int)numPrice.Value;
                    package.type_vat        = "";
                    package.package_status  = (comboStatus.SelectedItem as Item).Value.ToString();
                    package.notify_num      = (int)numericNotify.Value;
                    package.notify_unit     = (comboUnit2.SelectedItem as Item).Value.ToString();
                    package.seq             = (int)numSeq.Value;
                    package.branch1         = branch1;
                    package.branch2         = branch2;
                    package.branch3         = branch3;
                    package.branch4         = branch4;
                    package.branch5         = branch5;
                    ApiRest.InitailizeClient();
                    Response res = await ApiProcess.managePackage(typeActive, package, Global.USER.user_login);
                    if (res.status)
                    {
                        this.DialogResult = DialogResult.OK;
                    }

                }
            }
            catch
            {
                alertTxt.Visible = true;
                alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
                picLoading.Visible = false;
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void alertTxt_Click(object sender, EventArgs e)
        {

        }

        private void comboUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CalnumMaxday();
        }

        private void CalnumMaxday()
        {
            try
            {
                string val = "";
                if ((comboUnit.SelectedItem as Item).Value != null) {
                    val = (comboUnit.SelectedItem as Item).Value.ToString();
                }
                
                if (val.Equals("") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = numMaxUse.Value;
                }
                else if (val.Equals("DAYS") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = numMaxUse.Value;
                }
                else if (val.Equals("YEAR") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = (numMaxUse.Value * 365);
                }
                else if (val.Equals("MONTHS") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = (numMaxUse.Value * 30);
                }
                else if (val.Equals("TIMES") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = (numMaxUse.Value * 2);
                }
                else if (val.Equals("VIP") && numMaxUse.Value > 0)
                {
                    numMaxday.Value = 9999;
                }
                else
                {
                    numMaxday.Value = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void numMaxUse_ValueChanged(object sender, EventArgs e)
        {
            //CalnumMaxday();
        }

        private void TypePackageMem_Click(object sender, EventArgs e)
        {
            genPackageCode("MB");
            setComboUnit("MB");
            setEnable(true);
        }

        private void setComboUnit(string type)
        {
            comboUnit.Items.Clear();
            for (int x = 0; x < Global.TIME_UNIT.Count; x++)
            {
                MASTER master = Global.TIME_UNIT[x];
                if (master.DATA_CODE.Equals("TIMES") && type.Equals("PT"))
                {
                    comboUnit.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }
                else  if(type.Equals("MB"))
                {
                    comboUnit.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }
            }
        }

        private void TypePackagePT_Click(object sender, EventArgs e)
        {
            genPackageCode("PT");
            setComboUnit("PT");
            setEnable(true);
        }
    }
}
