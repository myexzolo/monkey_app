﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_checkin_trainer : Form
    {
        public string typeActive;

        private PersonCheckin pc;
        private string picCus = "";

        private string sPerson = "";
        private string sEmp = "";
        private string sManager = "";

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_checkin_trainer(PersonCheckin pc)
        {
            this.pc = pc;
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        private async void TrainerList(string trainerCode)
        {
            ApiRest.InitailizeClient();

            if (Global.TrainerList == null)
            {
                Global.TrainerList = await ApiProcess.getTrainers();

            }
            comboTrainer.Items.Add(new Item("", ""));
            for (int x = 0; x < Global.TrainerList.Count; x++)
            {
                Employee Trainer = Global.TrainerList[x];
                comboTrainer.Items.Add(new Item(Trainer.EMP_NAME + " " + Trainer.EMP_LASTNAME, Trainer.EMP_CODE));
                if (trainerCode == Trainer.EMP_CODE)
                {
                    comboTrainer.SelectedIndex = (x + 1);
                }
            }
        }

        private async void ManagerList(string managerCode)
        {
            ApiRest.InitailizeClient();

            if (Global.ManagerList == null)
            {
                Global.ManagerList = await ApiProcess.getManagers();
            }
            comboManager.Items.Add(new Item("", ""));
            for (int x = 0; x < Global.ManagerList.Count; x++)
            {
                Employee Manager = Global.ManagerList[x];
                comboManager.Items.Add(new Item(Manager.EMP_NAME + " " + Manager.EMP_LASTNAME, Manager.EMP_CODE));
                if (managerCode == Manager.EMP_CODE)
                {
                    comboManager.SelectedIndex = (x+1);
                }
            }
        }



        private void frm_manage_package_Load(object sender, EventArgs e)
        {
            try
            {
                dateCheckIn.Text = pc.checkin_date;
                personCode.Text = pc.PERSON_CODE;
                name.Text = pc.PERSON_NAME;
                packageName.Text = pc.package_name;

                //pc.sign_emp;
                // pc.sign_manager;
                //pc.status;
                use_package.Text = pc.use_package;
                pk_use.Text = (int.Parse(pc.use_package) - 1).ToString();
                num_use.Text = pc.num_use;
                //pc.package_unit;
                string trainer_code = Utils.ckeckNull(pc.trainer_code);
                string manager_code = Utils.ckeckNull(pc.managerCode);

                string sex = pc.PERSON_SEX;

                trainerCode.Text = trainer_code;
                TrainerList(trainer_code);
                ManagerList(manager_code);

                if (sex.Equals("หญิง"))
                {
                    this.pictureBox3.BackgroundImage = global::MONKEY_APP.Properties.Resources.fitness__3_;
                }
                else
                {
                    this.pictureBox3.BackgroundImage = global::MONKEY_APP.Properties.Resources.fitness__2_;
                }



                if (pc.sign_person.ToLower().Equals("true") || pc.sign_person.ToLower().Equals("y"))
                {
                    pc.sign_person = "true";
                    this.pictureBox2.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                    label21.Visible = true;
                    button1.Enabled = false;
                    button1.ForeColor = System.Drawing.Color.Gray;
                    button1.Image = global::MONKEY_APP.Properties.Resources.security11;

                    button4.Enabled = true;
                }
                else
                {
                    this.pictureBox2.BackgroundImage = global::MONKEY_APP.Properties.Resources.error1;
                    label21.Visible = false;
                    button1.Enabled = true;
                    button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                    button1.Image = global::MONKEY_APP.Properties.Resources.security1;
                }


                if (pc.sign_emp.ToLower().Equals("true") || pc.sign_emp.ToLower().Equals("y"))
                {
                    pc.sign_emp = "true";
                    this.pictureBox4.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                    label22.Visible = true;
                    comboTrainer.Enabled = false;
                    button2.Enabled = false;
                    button2.ForeColor = System.Drawing.Color.Gray;
                    button2.Image = global::MONKEY_APP.Properties.Resources.security11;

                    button4.Enabled = true;

                }
                else
                {
                    this.pictureBox4.BackgroundImage = global::MONKEY_APP.Properties.Resources.error1;
                    label22.Visible = false;
                    if (pc.sign_person.ToLower().Equals("true") || pc.sign_person.ToLower().Equals("y"))
                    {
                        comboTrainer.Enabled = true;
                        button2.Enabled = true;
                        button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                        button2.Image = global::MONKEY_APP.Properties.Resources.security1;
                    }
                }

                if (pc.sign_manager.ToLower().Equals("true") || pc.sign_manager.ToLower().Equals("y"))
                {
                    pc.sign_manager = "true";
                    this.pictureBox5.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                    label23.Visible = true;
                    button3.Enabled = false;
                    button3.ForeColor = System.Drawing.Color.Gray;
                    button3.Image = global::MONKEY_APP.Properties.Resources.security11;

                    button4.Enabled = true;
                }
                else
                {
                    this.pictureBox5.BackgroundImage = global::MONKEY_APP.Properties.Resources.error1;
                    label23.Visible = false;
                    if (pc.sign_emp.ToLower().Equals("true") || pc.sign_emp.ToLower().Equals("y"))
                    {
                        comboManager.Enabled = true;
                        button3.Enabled = true;
                        button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                        button3.Image = global::MONKEY_APP.Properties.Resources.security1;
                    }
                }
            }
            catch (Exception ex) {

                Console.WriteLine(ex);
            }
        }


        private async void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string status = "";

                if (pc.sign_person.ToLower().Equals("true") && pc.sign_emp.ToLower().Equals("true") && pc.sign_manager.ToLower().Equals("true")) {
                    status = "Y"; 
                }

                string trainer_code = "";
                string manager_code = "";
                Item item = (Item)comboTrainer.SelectedItem;
                if (item != null) 
                {
                    trainer_code = item.Value;
                }
                

                Item item2 = (Item)comboManager.SelectedItem;
                if (item2 != null)
                {
                    manager_code = item2.Value;
                }
                
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.manageCheckin("EDIT", pc.id, "", pc.package_person_id, sPerson, sEmp, sManager, status, pk_use.Text, trainer_code, manager_code);
                if (res.status)
                {
                    if (status == "Y") {
                        this.DialogResult = DialogResult.Abort;
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                    
                }
                else
                {
                    this.DialogResult = DialogResult.Cancel;
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button4_Click ::" + ex.ToString(), "frm_manage_checkin_trainer");
            }
            finally {
               
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Item item = (Item)comboManager.SelectedItem;

            if (item != null && !item.Value.Equals(""))
            {
                var thread1 = new Thread(showloading);
                thread1.Start();
                frm_fg_checkin fgc = new frm_fg_checkin(item.Value, item.Name, "EMP");
                var result = fgc.ShowDialog();
                picLoading.Visible = false;
                if (result == DialogResult.OK)
                {
                    sManager = "Y";
                    pc.sign_manager = "True";

                    this.pictureBox5.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                    label23.Visible = true;
                    button3.Enabled = false;
                    button3.ForeColor = System.Drawing.Color.Gray;
                    button3.Image = global::MONKEY_APP.Properties.Resources.security11;
                    button4.Enabled = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Item item = (Item)comboTrainer.SelectedItem;

            if (item != null && !item.Value.Equals("")) {
                var thread1 = new Thread(showloading);
                thread1.Start();
                frm_fg_checkin fgc = new frm_fg_checkin(item.Value, item.Name, "EMP");
                var result = fgc.ShowDialog();
                picLoading.Visible = false;
                if (result == DialogResult.OK)
                {
                    sEmp = "Y";
                    pc.sign_emp = "True";

                    this.pictureBox4.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                    label22.Visible = true;
                    button2.Enabled = false;
                    button2.ForeColor = System.Drawing.Color.Gray;
                    button2.Image = global::MONKEY_APP.Properties.Resources.security11;

                    comboManager.Enabled = true;
                    button3.Enabled = true;
                    button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                    button3.Image = global::MONKEY_APP.Properties.Resources.security1;
                    button4.Enabled = true;
                }
            }
            
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var thread1 = new Thread(showloading);
            thread1.Start();
            frm_fg_checkin fgc = new frm_fg_checkin(personCode.Text, name.Text, "PERSON");
            var result = fgc.ShowDialog();
            picLoading.Visible = false;
            if (result == DialogResult.OK)
            {
                sPerson = "Y";
                pc.sign_person = "True";

                this.pictureBox2.BackgroundImage = global::MONKEY_APP.Properties.Resources._checked;
                label21.Visible = true;
                button1.Enabled = false;
                button1.ForeColor = System.Drawing.Color.Gray;
                button1.Image = global::MONKEY_APP.Properties.Resources.security11;

                comboTrainer.Enabled = true;
                button2.Enabled = true;
                button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                button2.Image = global::MONKEY_APP.Properties.Resources.security1;

                button4.Enabled = true;
            }
        }

        private void comboTrainer_SelectedIndexChanged(object sender, EventArgs e)
        {
            Item item = (Item)comboTrainer.SelectedItem;
            trainerCode.Text = item.Value;
        }

        private void comboManager_SelectedIndexChanged(object sender, EventArgs e)
        {
            Item item = (Item)comboManager.SelectedItem;
            managerCode.Text = item.Value;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }

}
