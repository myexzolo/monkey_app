﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{

    public partial class frm_login : Form
    {
        //object FRegTemplate;
        int FingerCount;
        int fpcHandle;
        string[] FFingerNames;
        short FMatchType;

        //bool deviceOut;

        List<FingerScan> fingerScanList = null;

        public string branchCode = System.Configuration.ConfigurationManager.AppSettings["BranchCode"];

        public frm_login()
        {
            InitializeComponent();
        }
       
        private void frm_login_Load(object sender, EventArgs e)
        {

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            labVersions.Text = fvi.FileVersion;


            if (Global.BRANCH.status) {
                branchName.Text = Global.BRANCH.cname;
            }

            //username.Text = "20001";
            //passwordText.Text = "20001";
            //acceptBtn_Click(null, null);

        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            
        }

        private void setAlertMessage(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                    alertTxt.ForeColor = System.Drawing.Color.Red;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
                alertTxt.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void setAlertMessage2(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                    alertTxt.ForeColor = System.Drawing.Color.Blue;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
                alertTxt.ForeColor = System.Drawing.Color.Blue;
            }
        }

        private async void acceptBtn_Click(object sender, EventArgs e)
        {
            try
            {
                var thread1 = new Thread(showloading);

                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                if (username.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อผู้ใช้งาน **");
                    username.Focus();
                }
                else if (passwordText.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกรหัสผ่าน **");
                    passwordText.Focus();
                }
                else
                {
                    
                    thread1.Start();

                    string username = this.username.Text;
                    string password = this.passwordText.Text;

                    ApiRest.InitailizeClient();

                    Global.USER = await ApiProcess.Login(username, password, branchCode);
                    if (Global.USER.status)
                    {
                        Global.MENULIST     = await ApiProcess.MenuList(Global.USER.role_list);

                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        alertTxt.Visible = true;
                        setAlertMessage("** ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง **");
                        this.username.Text = "";
                        this.passwordText.Text = "";
                        this.username.Focus();

                    }
                }
            }
            catch 
            {
                alertTxt.Visible = true;
                alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
            }
            finally
            {
                picLoading.Visible = false;
            }

        }




        private async void loginByFingerId(string id)
        {
            try
            {
                var thread1 = new Thread(showloading);

                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                thread1.Start();
                ApiRest.InitailizeClient();
                var user = await ApiProcess.LoginByFingerId(id, Global.BRANCH.branch_code, "EMP");
                if (user.status)
                {
                    Global.USER = user;
                    Global.MENULIST         = await ApiProcess.MenuList(Global.USER.role_list);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง **");
                }

            }
            catch
            {
                alertTxt.Visible = true;
                alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
            }
            finally
            {
                picLoading.Visible = false;
            }

        }


        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //this.mainForm.CloseNotifyIcon();
                string RunningProcess = Process.GetCurrentProcess().ProcessName;
                foreach (Process proc in Process.GetProcessesByName(RunningProcess))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button1_Click ::" + ex.ToString(), "frm_login");
            }
        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                string RunningProcess = Process.GetCurrentProcess().ProcessName;
                foreach (Process proc in Process.GetProcessesByName(RunningProcess))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: pictureBox2_Click ::" + ex.ToString(), "frm_login");
            }
        }

        private void hoverButton(PictureBox obj,string sts)
        {
            if (sts.Equals("H")) {
                obj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(150)))), ((int)(((byte)(121)))));
            }
            else
            {
                obj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            }
        }

        private void username_TextChanged(object sender, EventArgs e)
        {
            if (username.Text.Length > 1) {
                alertTxt.Text = "";
                alertTxt.Visible = false;
            } 
        }

        private void passwordText_TextChanged(object sender, EventArgs e)
        {
            if (passwordText.Text.Length > 1)
            {
                alertTxt.Text = "";
                alertTxt.Visible = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                string RunningProcess = Process.GetCurrentProcess().ProcessName;
                foreach (Process proc in Process.GetProcessesByName(RunningProcess))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button7_Click ::" + ex.ToString(), "frm_login");
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmdInit()
        {
            try
            {
                ZKFPEngX1.SensorIndex = 0;
                //ทำการค้นหาหัวอ่านที่อยู่ในเครื่อง

                if (ZKFPEngX1.InitEngine() == 0)
                {

                    ZKFPEngX1.FPEngineVersion = "10"; //ใช้รูปแบบค้นหาลายนิ้วมือแบบเวอร์ชั่น 10

                    fpcHandle = ZKFPEngX1.CreateFPCacheDBEx();
                    //ประกาศให้มีการสร้าง Cache ฐานข้อมูลใน Memory แบบใช้เวอร์ชั่น 9 และ 10 ซึ่งจะมีการ
                    //เรียกใช้ข้อมูลของเวอร์ชั่น 9 และ 10 ในเวลาเดียวกัน การใช้งานในรูปแบบ อย่างใดอย่างหนึ่ง โปรดศึกษาด้วยตนเองเพิ่มเติม
                    setAlertMessage2("พร้อมสแกนนิ้ว");
                    //MessageBox.Show("การเชื่อมต่อหัวอ่านสำเร็จ", "สำเร็จ");

                    //StatusBar.Text = "Sensor เชื่อมต่อแล้ว";
                    //TextSensorCount.Text = ZKFPEngX1.SensorCount + "";
                    //นับหัวอ่าน
                    TextSensorIndex.Text = ZKFPEngX1.SensorIndex + "";
                    //เลือกใช้ตัวไหน
                    TextSensorSN.Text = ZKFPEngX1.SensorSN;
                    //ค่า Serial Number ของหัวอ่าน

                    ZKFPEngX1.EnrollCount = 3;
                    //กำหนดให้การเก็บลายนิ้วมือต้นฉบับต้อง วางนิ้ว 3 ครั้ง
                    //โหลดลายนิ้วมือเข้าระบบจากฐานข้อมูลที่ถูกเก็บไว้ก่อนหน้านี้()

                    FingerCount = 1;
                    LoadDB();

                    //deviceOut = true;

                    if (ZKFPEngX1.IsRegister)
                    {
                        ZKFPEngX1.CancelEnroll();
                        //ยกเลิกการเก็บ
                    }
                    FMatchType = 2;

                }
                else
                {
                    setAlertMessage("ระบบ ลายนิ้วมือผิดพลาด");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: cmdInit ::" + ex.ToString(), "frm_login");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
             cmdInit();   
        }

        private void ZKFPEngX1_OnImageReceived(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnImageReceivedEvent e)
        {
            Graphics g = pictureBox5.CreateGraphics();
            Bitmap bmp = new Bitmap(pictureBox5.Width, pictureBox5.Height);
            g = Graphics.FromImage(bmp);
            int dc = g.GetHdc().ToInt32();
            ZKFPEngX1.PrintImageAt(dc, 0, 0, bmp.Width, bmp.Height);
            g.Dispose();
            pictureBox5.Image = bmp;
            //กำหนดความกว้าง ยาวและตำแหน่งที่จะให้แสดงบน PictureBox สามารถกำหนดค่าให้ได้ตรงกับต้องการ
        }

        private void ZKFPEngX1_OnCapture(object sender, AxZKFPEngXControl.IZKFPEngXEvents_OnCaptureEvent e)
        {
            long fi = 0;
            int Score = new int();
            int ProcessNum = new int();
            string sTemp = null;

            if (FMatchType == 2)
            {
                Score = 8;
                sTemp = ZKFPEngX1.GetTemplateAsString();
                fi = ZKFPEngX1.IdentificationFromStrInFPCacheDB(fpcHandle, sTemp, ref Score, ref ProcessNum);
                //ทำการเปรียบเทียบ แบบ 1 ต่อ N
                if (fi == -1)
                {
                    //MsgBox "ค้นหาไม่เจอ", vbCritical, "ผิดพลาด"
                    setAlertMessage2("ค้นหาไม่เจอ");
                }
                else
                {
                    //ส่วนสำคัญที่จะนำไปใช้ในการเขียนโปรแกรมต่อ
                    //MsgBox "การค้นหาสำเร็จ ชื่อ =" & FFingerNames(fi) & " Score = " & Score & " Processed Number = " & ProcessNum, vbMsgBoxRight, "ผ่าน"  'คืนค่าการค้นหาออกมาให้ ซึ่งตรงนี้ สามารถเอาไปเรียกข้อมูลอื่นๆ มาใช้งานต่อ <- จุดสำคัญที่จะนำเอาไปใช้งาน
                    //setAlertMessage2("การค้นหาสำเร็จ " + FFingerNames[fi] + " Score = " + Score + " Processed Number = " + ProcessNum);
                    loginByFingerId(FFingerNames[fi]);

                }
            }
        }


        



        private async void LoadDB()
        {
            try
            {
                string sTemp;
                //ตัวแปรลายนิ้วมือเวอร์ชั้่น 9
                string sTempV10;
                //ตัวแปรลายนิ้วมือเวอร์ชั้่น 10

                ApiRest.InitailizeClient();
                fingerScanList = await ApiProcess.getFingerScan("All", "EMP", Global.BRANCH.branch_code);

                if (fingerScanList != null && fingerScanList.Count > 0)
                {
                    ZKFPEngX1.RemoveRegTemplateFromFPCacheDBEx(fpcHandle, FingerCount);

                    for (int i = 0; i < fingerScanList.Count; i++)
                    {
                        FingerScan fs = fingerScanList[i];

                        sTemp = Convert.ToString(fs.finger_str);

                        sTempV10 = Convert.ToString(fs.finger_v10);

                        //ฐานข้อมูลนิ้วเก็บในตรงนี้
                        ZKFPEngX1.AddRegTemplateStrToFPCacheDBEx(fpcHandle, FingerCount, sTemp, sTempV10);
                        //เพิ่ม ลายนิ้วมือเข้าไป โดยมี FingerCount เป็นตัวนับนิ้ว รูปแบบลายนิ้วมือที่ loop เข้าเป็นแบบ string
                        Array.Resize(ref FFingerNames, FingerCount + 1);
                        //สร้าง Array ของตัวแปร เพื่อเก็บชื่อ ของนิ้ว เอาไว้เรียกมาแสดงตอนแสดงผล
                        FFingerNames[FingerCount] = Convert.ToString(fs.finger_id);
                        //เอาข้อมูลเข้า
                        FingerCount = FingerCount + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: LoadDB ::" + ex.ToString(), "frm_login");
            }

           
        }
    }
}
