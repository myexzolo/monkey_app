﻿namespace MONKEY_APP
{
    partial class frm_manage_emp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.xx = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLname = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.user_code = new System.Windows.Forms.TextBox();
            this.nameUser = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboSex = new System.Windows.Forms.ComboBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.empCode = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bod = new System.Windows.Forms.DateTimePicker();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboTitle = new System.Windows.Forms.ComboBox();
            this.comboPosition = new System.Windows.Forms.ComboBox();
            this.comboDepartment = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labPov = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textTel = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtZip = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textaddrAmphur = new System.Windows.Forms.ComboBox();
            this.txtaddrProvince = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dateIncome = new System.Windows.Forms.DateTimePicker();
            this.textaddrTambol = new System.Windows.Forms.ComboBox();
            this.alertTxt = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.checkTrainer = new System.Windows.Forms.CheckBox();
            this.checkSale = new System.Windows.Forms.CheckBox();
            this.checkManager = new System.Windows.Forms.CheckBox();
            this.checkInstructor = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtWage = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.chkGm1 = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkGm2 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1203, 58);
            this.panel1.TabIndex = 115;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::MONKEY_APP.Properties.Resources.user__1_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 37);
            this.pictureBox1.TabIndex = 117;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(58, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 27);
            this.label1.TabIndex = 116;
            this.label1.Text = "เพิ่มทะเบียนพนักงาน";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::MONKEY_APP.Properties.Resources.id_card;
            this.button1.Location = new System.Drawing.Point(878, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 40);
            this.button1.TabIndex = 115;
            this.button1.Text = "  อ่านข้อมูลจากบัตรประชาชน";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            this.button7.BackgroundImage = global::MONKEY_APP.Properties.Resources.close__1_;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(1173, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(20, 20);
            this.button7.TabIndex = 114;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(79, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 24);
            this.label4.TabIndex = 116;
            this.label4.Text = "ชื่อ :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(389, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 24);
            this.label5.TabIndex = 116;
            this.label5.Text = "นามสกุล :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(681, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 24);
            this.label6.TabIndex = 116;
            this.label6.Text = "วันเกิด (คศ.) :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(15, 231);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 24);
            this.label7.TabIndex = 116;
            this.label7.Text = "บัตรประชาชน :";
            // 
            // xx
            // 
            this.xx.AutoSize = true;
            this.xx.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.xx.Location = new System.Drawing.Point(53, 544);
            this.xx.Name = "xx";
            this.xx.Size = new System.Drawing.Size(62, 24);
            this.xx.TabIndex = 116;
            this.xx.Text = "ผู้บันทึก :";
            this.xx.Click += new System.EventHandler(this.xx_Click);
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtName.Location = new System.Drawing.Point(121, 179);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(207, 30);
            this.txtName.TabIndex = 117;
            // 
            // txtLname
            // 
            this.txtLname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLname.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtLname.Location = new System.Drawing.Point(463, 179);
            this.txtLname.Multiline = true;
            this.txtLname.Name = "txtLname";
            this.txtLname.Size = new System.Drawing.Size(207, 30);
            this.txtLname.TabIndex = 117;
            // 
            // txtId
            // 
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtId.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtId.Location = new System.Drawing.Point(121, 228);
            this.txtId.Multiline = true;
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(207, 30);
            this.txtId.TabIndex = 117;
            // 
            // user_code
            // 
            this.user_code.BackColor = System.Drawing.Color.Gainsboro;
            this.user_code.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.user_code.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.user_code.Location = new System.Drawing.Point(121, 544);
            this.user_code.Multiline = true;
            this.user_code.Name = "user_code";
            this.user_code.ReadOnly = true;
            this.user_code.Size = new System.Drawing.Size(157, 30);
            this.user_code.TabIndex = 117;
            // 
            // nameUser
            // 
            this.nameUser.BackColor = System.Drawing.Color.Gainsboro;
            this.nameUser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nameUser.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.nameUser.Location = new System.Drawing.Point(302, 544);
            this.nameUser.Multiline = true;
            this.nameUser.Name = "nameUser";
            this.nameUser.ReadOnly = true;
            this.nameUser.Size = new System.Drawing.Size(282, 30);
            this.nameUser.TabIndex = 117;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(400, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 24);
            this.label9.TabIndex = 116;
            this.label9.Text = "ชื่อเล่น :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(731, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 24);
            this.label10.TabIndex = 116;
            this.label10.Text = "เพศ :";
            // 
            // comboSex
            // 
            this.comboSex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboSex.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboSex.FormattingEnabled = true;
            this.comboSex.Location = new System.Drawing.Point(778, 130);
            this.comboSex.Name = "comboSex";
            this.comboSex.Size = new System.Drawing.Size(207, 32);
            this.comboSex.TabIndex = 118;
            // 
            // txtNickName
            // 
            this.txtNickName.AcceptsReturn = true;
            this.txtNickName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNickName.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtNickName.Location = new System.Drawing.Point(463, 130);
            this.txtNickName.Multiline = true;
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(207, 30);
            this.txtNickName.TabIndex = 117;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(22, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 24);
            this.label18.TabIndex = 116;
            this.label18.Text = "รหัสพนักงาน :";
            // 
            // empCode
            // 
            this.empCode.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.empCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.empCode.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.empCode.Location = new System.Drawing.Point(121, 86);
            this.empCode.Multiline = true;
            this.empCode.Name = "empCode";
            this.empCode.ReadOnly = true;
            this.empCode.Size = new System.Drawing.Size(207, 30);
            this.empCode.TabIndex = 117;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(1063, 128);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 24);
            this.label19.TabIndex = 116;
            this.label19.Text = "รูปภาพ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bod);
            this.panel2.Location = new System.Drawing.Point(778, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(207, 29);
            this.panel2.TabIndex = 122;
            // 
            // bod
            // 
            this.bod.CalendarFont = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bod.CustomFormat = "dd MMM yyyy";
            this.bod.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bod.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bod.Location = new System.Drawing.Point(-1, -1);
            this.bod.Name = "bod";
            this.bod.Size = new System.Drawing.Size(210, 31);
            this.bod.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(1025, 173);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 180);
            this.pictureBox2.TabIndex = 121;
            this.pictureBox2.TabStop = false;
            // 
            // picLoading
            // 
            this.picLoading.BackColor = System.Drawing.Color.White;
            this.picLoading.Image = global::MONKEY_APP.Properties.Resources.loading2;
            this.picLoading.Location = new System.Drawing.Point(561, 241);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(109, 96);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLoading.TabIndex = 123;
            this.picLoading.TabStop = false;
            this.picLoading.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label20.Location = new System.Drawing.Point(23, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 24);
            this.label20.TabIndex = 116;
            this.label20.Text = "คำนำหน้าชื่อ :";
            // 
            // comboTitle
            // 
            this.comboTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboTitle.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboTitle.FormattingEnabled = true;
            this.comboTitle.Location = new System.Drawing.Point(121, 130);
            this.comboTitle.Name = "comboTitle";
            this.comboTitle.Size = new System.Drawing.Size(207, 32);
            this.comboTitle.TabIndex = 118;
            // 
            // comboPosition
            // 
            this.comboPosition.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboPosition.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboPosition.FormattingEnabled = true;
            this.comboPosition.Location = new System.Drawing.Point(121, 277);
            this.comboPosition.Name = "comboPosition";
            this.comboPosition.Size = new System.Drawing.Size(207, 32);
            this.comboPosition.TabIndex = 118;
            // 
            // comboDepartment
            // 
            this.comboDepartment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboDepartment.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboDepartment.FormattingEnabled = true;
            this.comboDepartment.Location = new System.Drawing.Point(121, 326);
            this.comboDepartment.Name = "comboDepartment";
            this.comboDepartment.Size = new System.Drawing.Size(207, 32);
            this.comboDepartment.TabIndex = 118;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(690, 329);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 24);
            this.label11.TabIndex = 133;
            this.label11.Text = "เขต/อำเภอ :";
            // 
            // txtTel
            // 
            this.txtTel.AutoSize = true;
            this.txtTel.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTel.Location = new System.Drawing.Point(63, 378);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(52, 24);
            this.txtTel.TabIndex = 134;
            this.txtTel.Text = "มือถือ :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(368, 378);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 24);
            this.label13.TabIndex = 135;
            this.label13.Text = "แขวง/ตำบล :";
            // 
            // labPov
            // 
            this.labPov.AutoSize = true;
            this.labPov.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.labPov.Location = new System.Drawing.Point(398, 329);
            this.labPov.Name = "labPov";
            this.labPov.Size = new System.Drawing.Size(59, 24);
            this.labPov.TabIndex = 136;
            this.labPov.Text = "จังหวัด :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(413, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 24);
            this.label2.TabIndex = 137;
            this.label2.Text = "ที่อยู่ :";
            // 
            // textTel
            // 
            this.textTel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textTel.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textTel.Location = new System.Drawing.Point(121, 375);
            this.textTel.Multiline = true;
            this.textTel.Name = "textTel";
            this.textTel.Size = new System.Drawing.Size(207, 30);
            this.textTel.TabIndex = 138;
            // 
            // txtAddress2
            // 
            this.txtAddress2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAddress2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAddress2.Location = new System.Drawing.Point(463, 277);
            this.txtAddress2.Multiline = true;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(522, 30);
            this.txtAddress2.TabIndex = 143;
            // 
            // txtAddress1
            // 
            this.txtAddress1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAddress1.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAddress1.Location = new System.Drawing.Point(463, 228);
            this.txtAddress1.Multiline = true;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(524, 30);
            this.txtAddress1.TabIndex = 144;
            // 
            // txtZip
            // 
            this.txtZip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtZip.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtZip.Location = new System.Drawing.Point(778, 375);
            this.txtZip.Multiline = true;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(207, 30);
            this.txtZip.TabIndex = 139;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(703, 379);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 24);
            this.label14.TabIndex = 145;
            this.label14.Text = "ไปรษณีย์ :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(400, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 24);
            this.label3.TabIndex = 146;
            this.label3.Text = "ที่อยู่ 2 :";
            // 
            // textaddrAmphur
            // 
            this.textaddrAmphur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrAmphur.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrAmphur.FormattingEnabled = true;
            this.textaddrAmphur.Location = new System.Drawing.Point(777, 326);
            this.textaddrAmphur.Name = "textaddrAmphur";
            this.textaddrAmphur.Size = new System.Drawing.Size(208, 32);
            this.textaddrAmphur.TabIndex = 147;
            this.textaddrAmphur.SelectedIndexChanged += new System.EventHandler(this.textaddrAmphur_SelectedIndexChanged);
            // 
            // txtaddrProvince
            // 
            this.txtaddrProvince.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtaddrProvince.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtaddrProvince.FormattingEnabled = true;
            this.txtaddrProvince.Location = new System.Drawing.Point(463, 326);
            this.txtaddrProvince.Name = "txtaddrProvince";
            this.txtaddrProvince.Size = new System.Drawing.Size(208, 32);
            this.txtaddrProvince.TabIndex = 147;
            this.txtaddrProvince.SelectedIndexChanged += new System.EventHandler(this.txtaddrProvince_SelectedIndexChanged);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 2;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(831, 523);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 51);
            this.button4.TabIndex = 149;
            this.button4.Text = "บันทึก";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(3)))));
            this.button5.Location = new System.Drawing.Point(998, 523);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(125, 51);
            this.button5.TabIndex = 150;
            this.button5.Text = "ยกเลิก";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(48, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 116;
            this.label8.Text = "ตำแหน่ง :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(63, 330);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 24);
            this.label16.TabIndex = 116;
            this.label16.Text = "แผนก :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(15, 426);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 24);
            this.label21.TabIndex = 116;
            this.label21.Text = "วันที่เริ่ม (คศ.) :";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dateIncome);
            this.panel3.Location = new System.Drawing.Point(121, 423);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(207, 29);
            this.panel3.TabIndex = 122;
            // 
            // dateIncome
            // 
            this.dateIncome.CalendarFont = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.CustomFormat = "dd MMM yyyy";
            this.dateIncome.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dateIncome.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateIncome.Location = new System.Drawing.Point(-1, -1);
            this.dateIncome.Name = "dateIncome";
            this.dateIncome.Size = new System.Drawing.Size(210, 31);
            this.dateIncome.TabIndex = 0;
            // 
            // textaddrTambol
            // 
            this.textaddrTambol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textaddrTambol.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textaddrTambol.FormattingEnabled = true;
            this.textaddrTambol.Location = new System.Drawing.Point(462, 375);
            this.textaddrTambol.Name = "textaddrTambol";
            this.textaddrTambol.Size = new System.Drawing.Size(208, 32);
            this.textaddrTambol.TabIndex = 147;
            this.textaddrTambol.SelectedIndexChanged += new System.EventHandler(this.textaddrTambol_SelectedIndexChanged);
            // 
            // alertTxt
            // 
            this.alertTxt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alertTxt.BackColor = System.Drawing.Color.Transparent;
            this.alertTxt.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.alertTxt.ForeColor = System.Drawing.Color.Red;
            this.alertTxt.Location = new System.Drawing.Point(148, 504);
            this.alertTxt.MinimumSize = new System.Drawing.Size(270, 0);
            this.alertTxt.Name = "alertTxt";
            this.alertTxt.Size = new System.Drawing.Size(342, 28);
            this.alertTxt.TabIndex = 151;
            this.alertTxt.Text = "alert";
            this.alertTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.alertTxt.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(15, 470);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 24);
            this.label12.TabIndex = 135;
            this.label12.Text = "สิทธิพนักงาน :";
            // 
            // checkTrainer
            // 
            this.checkTrainer.AutoSize = true;
            this.checkTrainer.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.checkTrainer.Location = new System.Drawing.Point(114, 470);
            this.checkTrainer.Name = "checkTrainer";
            this.checkTrainer.Size = new System.Drawing.Size(72, 28);
            this.checkTrainer.TabIndex = 152;
            this.checkTrainer.Text = "Trainer";
            this.checkTrainer.UseVisualStyleBackColor = true;
            // 
            // checkSale
            // 
            this.checkSale.AutoSize = true;
            this.checkSale.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.checkSale.Location = new System.Drawing.Point(287, 470);
            this.checkSale.Name = "checkSale";
            this.checkSale.Size = new System.Drawing.Size(55, 28);
            this.checkSale.TabIndex = 152;
            this.checkSale.Text = "Sale";
            this.checkSale.UseVisualStyleBackColor = true;
            // 
            // checkManager
            // 
            this.checkManager.AutoSize = true;
            this.checkManager.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.checkManager.Location = new System.Drawing.Point(346, 470);
            this.checkManager.Name = "checkManager";
            this.checkManager.Size = new System.Drawing.Size(69, 28);
            this.checkManager.TabIndex = 152;
            this.checkManager.Text = "Admin";
            this.checkManager.UseVisualStyleBackColor = true;
            // 
            // checkInstructor
            // 
            this.checkInstructor.AutoSize = true;
            this.checkInstructor.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.checkInstructor.Location = new System.Drawing.Point(192, 470);
            this.checkInstructor.Name = "checkInstructor";
            this.checkInstructor.Size = new System.Drawing.Size(89, 28);
            this.checkInstructor.TabIndex = 153;
            this.checkInstructor.Text = "Instructor";
            this.checkInstructor.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(336, 428);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 24);
            this.label15.TabIndex = 154;
            this.label15.Text = "ค่าจ้าง Class/ครั้ง :";
            // 
            // txtWage
            // 
            this.txtWage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWage.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtWage.Location = new System.Drawing.Point(462, 425);
            this.txtWage.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.txtWage.Name = "txtWage";
            this.txtWage.Size = new System.Drawing.Size(208, 31);
            this.txtWage.TabIndex = 155;
            this.txtWage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("TH SarabunPSK", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button2.Location = new System.Drawing.Point(1025, 362);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 29);
            this.button2.TabIndex = 156;
            this.button2.Text = "Upload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chkGm1
            // 
            this.chkGm1.AutoSize = true;
            this.chkGm1.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chkGm1.Location = new System.Drawing.Point(534, 470);
            this.chkGm1.Name = "chkGm1";
            this.chkGm1.Size = new System.Drawing.Size(155, 28);
            this.chkGm1.TabIndex = 152;
            this.chkGm1.Tag = "branch1";
            this.chkGm1.Text = "The Circle ราชพฤกษ์";
            this.chkGm1.UseVisualStyleBackColor = true;
            this.chkGm1.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.Location = new System.Drawing.Point(478, 470);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 24);
            this.label17.TabIndex = 157;
            this.label17.Text = "สาขา :";
            this.label17.Visible = false;
            // 
            // chkGm2
            // 
            this.chkGm2.AutoSize = true;
            this.chkGm2.Font = new System.Drawing.Font("TH SarabunPSK", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chkGm2.Location = new System.Drawing.Point(697, 470);
            this.chkGm2.Name = "chkGm2";
            this.chkGm2.Size = new System.Drawing.Size(182, 28);
            this.chkGm2.TabIndex = 152;
            this.chkGm2.Tag = "branch2";
            this.chkGm2.Text = "HomePro จรัญสนิทวงศ์18";
            this.chkGm2.UseVisualStyleBackColor = true;
            this.chkGm2.Visible = false;
            // 
            // frm_manage_emp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1203, 589);
            this.ControlBox = false;
            this.Controls.Add(this.label17);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtWage);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.checkInstructor);
            this.Controls.Add(this.checkManager);
            this.Controls.Add(this.checkSale);
            this.Controls.Add(this.chkGm2);
            this.Controls.Add(this.chkGm1);
            this.Controls.Add(this.checkTrainer);
            this.Controls.Add(this.alertTxt);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.picLoading);
            this.Controls.Add(this.textaddrTambol);
            this.Controls.Add(this.txtaddrProvince);
            this.Controls.Add(this.textaddrAmphur);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.labPov);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textTel);
            this.Controls.Add(this.txtZip);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.comboSex);
            this.Controls.Add(this.comboDepartment);
            this.Controls.Add(this.comboPosition);
            this.Controls.Add(this.comboTitle);
            this.Controls.Add(this.nameUser);
            this.Controls.Add(this.user_code);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.txtLname);
            this.Controls.Add(this.txtNickName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.empCode);
            this.Controls.Add(this.xx);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_manage_emp";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frm_manage_cus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtWage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label xx;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLname;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox user_code;
        private System.Windows.Forms.TextBox nameUser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboSex;
        private System.Windows.Forms.TextBox txtNickName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox empCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker bod;
        private System.Windows.Forms.PictureBox picLoading;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboTitle;
        private System.Windows.Forms.ComboBox comboPosition;
        private System.Windows.Forms.ComboBox comboDepartment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label txtTel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labPov;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textTel;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtZip;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox textaddrAmphur;
        private System.Windows.Forms.ComboBox txtaddrProvince;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker dateIncome;
        private System.Windows.Forms.ComboBox textaddrTambol;
        private System.Windows.Forms.Label alertTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkTrainer;
        private System.Windows.Forms.CheckBox checkSale;
        private System.Windows.Forms.CheckBox checkManager;
        private System.Windows.Forms.CheckBox checkInstructor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown txtWage;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkGm1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkGm2;
    }
}