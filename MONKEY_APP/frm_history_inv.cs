﻿using iTextSharp.text.pdf;
using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_history_inv : Form
    {

        private DataTable dtm;
        public PackagePersons packagePersons;
        public string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();

        public string printerName       = System.Configuration.ConfigurationManager.AppSettings["printerName"];
        public string TopMostDisplay    = System.Configuration.ConfigurationManager.AppSettings["TopMost"];

        private void bs_Click(object sender, EventArgs e)
        {

        }

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_history_inv()
        {
            InitializeComponent();
            if (TopMostDisplay.Equals("Y"))
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }



        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            getInvoices();
        }

        public byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        private async void getInvoices()
        {
            try
            {
                string typePayment = "";
                if (radioButton6.Checked)
                {
                    typePayment = radioButton6.Tag.ToString();
                }
                if (radioButton5.Checked)
                {
                    typePayment = radioButton5.Tag.ToString();
                }

                string startDate = txtStartDate.Value.ToString("yyy/MM/dd");
                string endDate = txtEndDate.Value.ToString("yyy/MM/dd");
                ApiRest.InitailizeClient();
                Global.InvoiceList = await ApiProcess.getHistoryInv(typePayment, startDate, endDate);


                dtm = new DataTable();

                dtm.Columns.Add("วันที่ออกใบเสร็จ", typeof(string));
                dtm.Columns.Add("เลขที่ใบเสร็จ", typeof(string));
                dtm.Columns.Add("ชื่อ", typeof(string));
                dtm.Columns.Add("ประเภทใบเสร็จ", typeof(string));
                dtm.Columns.Add("ราคา", typeof(string));
                dtm.Columns.Add("ส่วนลด", typeof(string));
                dtm.Columns.Add("ภาษี", typeof(string));
                dtm.Columns.Add("ราคาสุทธิ", typeof(string));
                dtm.Columns.Add("เงินสด", typeof(string));
                dtm.Columns.Add("โอนเงิน", typeof(string));
                dtm.Columns.Add("เครดิต", typeof(string));
                dtm.Columns.Add("เช็ค", typeof(string));
                dtm.Columns.Add("status", typeof(string));
                dtm.Columns.Add("add", typeof(string));
                dtm.Columns.Add("tel", typeof(string));
                dtm.Columns.Add("fax", typeof(string));
                dtm.Columns.Add("tax", typeof(string));
                dtm.Columns.Add("printnum", typeof(string));
                dtm.Columns.Add("id", typeof(string));
                dtm.Columns.Add("พิมพ์ INV", typeof(byte[]));
                dtm.Columns.Add("พิมพ์", typeof(byte[]));
                dtm.Columns.Add("ยกเลิก", typeof(byte[]));
                dtm.Columns.Add("typePay", typeof(string));
                dtm.Columns.Add("invDate", typeof(string));

                Bitmap Image = global::MONKEY_APP.Properties.Resources.printer_tool30;
                Bitmap Image2 = global::MONKEY_APP.Properties.Resources.error30;
                Bitmap Image3 = global::MONKEY_APP.Properties.Resources.error30_d;

                byte[] printImage   = ImageToByte(Image);
                byte[] cancelImage  = ImageToByte(Image3);

                if (Global.USER.EMP_IS_STAFF != null && Global.USER.EMP_IS_STAFF.Equals("Y"))
                {
                    cancelImage = ImageToByte(Image2);
                }

                //dataGridView1.Rows.Clear();
                for (int i = 0; i < Global.InvoiceList.Count(); i++)
                {
                    Invoice invoice = Global.InvoiceList[i];

                    string typePaymentTxt = "";

                    if (invoice.cash > 0) {
                        typePaymentTxt = "เงินสด";
                    }

                    if (invoice.transfer > 0)
                    {
                        if (typePaymentTxt.Equals("")) {
                            typePaymentTxt = "โอนเงิน";
                        }
                        else
                        {
                            typePaymentTxt += ", โอนเงิน";
                        }                   
                    }

                    if (invoice.credit > 0)
                    {
                        if (typePaymentTxt.Equals(""))
                        {
                            typePaymentTxt = "เครดิต";
                        }
                        else
                        {
                            typePaymentTxt += ", เครดิต";
                        }
                    }

                    if (invoice.cheque > 0)
                    {
                        if (typePaymentTxt.Equals(""))
                        {
                            typePaymentTxt = "เช็คเงินสด";
                        }
                        else
                        {
                            typePaymentTxt += ", เช็คเงินสด";
                        }
                    }
                    //dataGridView1.Rows.Add(
                    //   ds.ToString("dd/MM/yyyy"), invoice.invoice_code, invoice.name, 
                    //   invoice.type_payment, float.Parse(invoice.total_price).ToString("#,##0.00"),float.Parse(invoice.discount).ToString("#,##0.00"), 
                    //   float.Parse(invoice.vat).ToString("#,##0.00"), float.Parse(invoice.total_net).ToString("#,##0.00"),invoice.cash.ToString("#,##0.00"), 
                    //   invoice.transfer.ToString("#,##0.00"), invoice.credit.ToString("#,##0.00"), invoice.cheque.ToString("#,##0.00"), invoice.status,
                    //   invoice.address, invoice.tel, invoice.fax, 
                    //   invoice.tax, invoice.num_print, invoice.invoice_id);


                   
                    dtm.Rows.Add(
                    invoice.invoice_date_txt, invoice.invoice_code, invoice.name,
                    invoice.type_payment, float.Parse(invoice.total_price).ToString("#,##0.00"), float.Parse(invoice.discount).ToString("#,##0.00"),
                    float.Parse(invoice.vat).ToString("#,##0.00"), float.Parse(invoice.total_net).ToString("#,##0.00"), invoice.cash.ToString("#,##0.00"),
                    invoice.transfer.ToString("#,##0.00"), invoice.credit.ToString("#,##0.00"), invoice.cheque.ToString("#,##0.00"), invoice.status,
                    invoice.address, invoice.tel, invoice.fax,
                    invoice.tax, invoice.num_print, invoice.invoice_id, printImage, printImage, cancelImage, typePaymentTxt, invoice.invoice_date);

                    
                }

                dataGridView1.DataSource = dtm;

                //DataGridViewButtonColumn bottom = new DataGridViewButtonColumn();
                //bottom.HeaderText = "";
                //bottom.Text = "พิมพ์";
                //bottom.Name = "print";
                //bottom.UseColumnTextForButtonValue = true;
                //if (dataGridView1.Columns["print"] == null)
                //{
                //    dataGridView1.Columns.Add(bottom);
                //    dataGridView1.Columns[19].Width = 70;
                //}



                //if (Global.InvoiceList.Count() > 0 && Global.USER.EMP_IS_STAFF != null && Global.USER.EMP_IS_STAFF.Equals("Y"))
                //{
                //    DataGridViewButtonColumn bottom2 = new DataGridViewButtonColumn();
                //    bottom2.HeaderText = "";
                //    bottom2.Text = "ยกเลิก";
                //    bottom2.Name = "cancel";
                //    bottom2.UseColumnTextForButtonValue = true;

                //    if (dataGridView1.Columns["cancel"] == null)
                //    {
                //        dataGridView1.Columns.Add(bottom2);
                //        dataGridView1.Columns[20].Width = 70;
                //    }
                //}

                dataGridView1.Columns[0].Width = 140;
                dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[1].Width = 130;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[2].Width = 238;
                dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[3].Width = 120;
                dataGridView1.Columns[3].Visible = false;
                dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[4].Width = 90;
                dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[5].Width = 80;
                dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[6].Width = 80;
                dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[7].Width = 90;
                dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[8].Width = 80;
                dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[9].Width = 80;
                dataGridView1.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[9].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[10].Width = 80;
                dataGridView1.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[10].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[11].Width = 80;
                dataGridView1.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;

                dataGridView1.Columns[12].Visible = false;
                dataGridView1.Columns[13].Visible = false;
                dataGridView1.Columns[14].Visible = false;
                dataGridView1.Columns[15].Visible = false;
                dataGridView1.Columns[16].Visible = false;
                dataGridView1.Columns[17].Visible = false;
                dataGridView1.Columns[18].Visible = false;
                dataGridView1.Columns[22].Visible = false;
                dataGridView1.Columns[23].Visible = false;

                if (radioButton5.Checked)
                {
                    dataGridView1.Columns[19].Visible = false;
                }
                else {
                    dataGridView1.Columns[19].Visible = true;
                }

                dataGridView1.Columns[19].Width = 90;
                dataGridView1.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[19].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[20].Width = 70;
                dataGridView1.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[20].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[21].Width = 70;
                dataGridView1.Columns[21].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[21].SortMode = DataGridViewColumnSortMode.NotSortable;

                //dtm = (DataTable)(dataGridView1.DataSource);

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getInvoices ::" + ex.ToString(), "frm_history_inv");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int rowindex = dataGridView1.CurrentCell.RowIndex;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            getInvoices();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("xxx");
        }

        private async void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                int rowindex = dataGridView2.CurrentCell.RowIndex;
                PackagePerson ps = new PackagePerson();



                ps.person_code = dataGridView2.Rows[rowindex].Cells[1].Value.ToString();
                ps.reg_no = dataGridView2.Rows[rowindex].Cells[2].Value.ToString();
                ps.package_name = dataGridView2.Rows[rowindex].Cells[3].Value.ToString();
                ps.package_price = dataGridView2.Rows[rowindex].Cells[4].Value.ToString();
                ps.package_num = int.Parse(dataGridView2.Rows[rowindex].Cells[5].Value.ToString());
                ps.package_price_total = dataGridView2.Rows[rowindex].Cells[6].Value.ToString();
                ps.discount = dataGridView2.Rows[rowindex].Cells[7].Value.ToString();
                ps.vat = dataGridView2.Rows[rowindex].Cells[8].Value.ToString();
                ps.net_total = dataGridView2.Rows[rowindex].Cells[9].Value.ToString();
                ps.num_use = int.Parse(dataGridView2.Rows[rowindex].Cells[10].Value.ToString());
                ps.package_unit = dataGridView2.Rows[rowindex].Cells[11].Value.ToString();
                ps.date_start = dataGridView2.Rows[rowindex].Cells[28].Value.ToString();
                ps.date_expire = dataGridView2.Rows[rowindex].Cells[29].Value.ToString();
                ps.max_use = int.Parse(dataGridView2.Rows[rowindex].Cells[14].Value.ToString());
                ps.type_package = dataGridView2.Rows[rowindex].Cells[15].Value.ToString();
                ps.trainer_name = dataGridView2.Rows[rowindex].Cells[16].Value.ToString();
                ps.percent_discount = int.Parse(dataGridView2.Rows[rowindex].Cells[17].Value.ToString());
                ps.trainer_code = dataGridView2.Rows[rowindex].Cells[18].Value.ToString();
                ps.id = dataGridView2.Rows[rowindex].Cells[19].Value.ToString();
                ps.package_detail = dataGridView2.Rows[rowindex].Cells[20].Value.ToString();
                ps.package_code = dataGridView2.Rows[rowindex].Cells[21].Value.ToString();
                ps.notify_num = int.Parse(dataGridView2.Rows[rowindex].Cells[22].Value.ToString());
                ps.notify_unit = dataGridView2.Rows[rowindex].Cells[23].Value.ToString();
                ps.type_vat = dataGridView2.Rows[rowindex].Cells[24].Value.ToString();
                ps.create_by = dataGridView2.Rows[rowindex].Cells[27].Value.ToString();

                string invoice_date = dataGridView2.Rows[rowindex].Cells[25].Value.ToString();
                string typePaymentTxt = dataGridView2.Rows[rowindex].Cells[26].Value.ToString();

                if (e.ColumnIndex == dataGridView2.Columns["Contract"].Index)
                {
                    ApiRest.InitailizeClient();
                    Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, ps.person_code);

                    showloading();
                    if (ps.type_package.Equals("MB"))
                    {
                        Utils.eformContract(ps, person, invoice_date, typePaymentTxt);
                    }
                    else if (ps.type_package.Equals("PT"))
                    {
                        Utils.eformContractPT(ps, person, invoice_date, typePaymentTxt);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                picLoading.Visible = false;
            }

        }


        private async void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string typeActive = "";
                int rowindex = dataGridView1.CurrentCell.RowIndex;
                Invoice invoice = new Invoice();

                invoice.total_price = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                invoice.discount = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                invoice.vat = dataGridView1.Rows[rowindex].Cells[6].Value.ToString();
                invoice.total_net = dataGridView1.Rows[rowindex].Cells[7].Value.ToString();

                invoice.invoice_code = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                invoice.invoice_date = dataGridView1.Rows[rowindex].Cells[23].Value.ToString();
                invoice.name = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                invoice.address = dataGridView1.Rows[rowindex].Cells[13].Value.ToString();
                invoice.tel = dataGridView1.Rows[rowindex].Cells[14].Value.ToString();
                invoice.fax = dataGridView1.Rows[rowindex].Cells[15].Value.ToString();
                invoice.tax = dataGridView1.Rows[rowindex].Cells[16].Value.ToString();
                invoice.num_print = int.Parse(dataGridView1.Rows[rowindex].Cells[17].Value.ToString()) + 1;
                invoice.invoice_id = dataGridView1.Rows[rowindex].Cells[18].Value.ToString();
                invoice.cash = float.Parse(dataGridView1.Rows[rowindex].Cells[8].Value.ToString());
                invoice.transfer = float.Parse(dataGridView1.Rows[rowindex].Cells[9].Value.ToString());
                invoice.credit = float.Parse(dataGridView1.Rows[rowindex].Cells[10].Value.ToString());
                invoice.cheque = float.Parse(dataGridView1.Rows[rowindex].Cells[11].Value.ToString());
                invoice.status = dataGridView1.Rows[rowindex].Cells[12].Value.ToString();
                invoice.type_payment = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();

                if (e.ColumnIndex == dataGridView1.Columns["พิมพ์"].Index)
                {
                    typeActive = "PRINT";

                    ApiRest.InitailizeClient();
                    bool flag = true;
                    Response resInvoice = await ApiProcess.manageInvoice(Global.BRANCH.branch_code, typeActive, invoice, Global.USER.user_login);
                    if (!resInvoice.status)
                    {
                        flag = false;
                    }
                    showloading();
                    if (packagePersons == null)
                    {
                        packagePersons.PackagePersonList = await ApiProcess.getPackageByInvoiceCode(invoice.invoice_code);
                    }

                    Utils.eformReceipt(invoice.type_payment, invoice, packagePersons, invoice.invoice_date_txt);

                }

                if (e.ColumnIndex == dataGridView1.Columns["พิมพ์ INV"].Index)
                {
                    typeActive = "PRINTINV";

                    ApiRest.InitailizeClient();
                    bool flag = true;
                    Response resInvoice = await ApiProcess.manageInvoice(Global.BRANCH.branch_code, typeActive, invoice, Global.USER.user_login);
                    if (!resInvoice.status)
                    {
                        flag = false;
                    }
                    if (resInvoice.data != "")
                    {
                        string[] strlist = resInvoice.data.Split('|');
                        invoice.type_payment = "INV";
                        invoice.invoice_code = strlist[0];
                        invoice.vat          = strlist[1];
                        invoice.total_price  = strlist[2];

                    }
                    showloading();
                    if (packagePersons == null)
                    {
                        packagePersons.PackagePersonList = await ApiProcess.getPackageByInvoiceCode(invoice.invoice_code);
                    }

                    Utils.eformReceipt(invoice.type_payment, invoice, packagePersons, invoice.invoice_date_txt);

                }

                if (dataGridView1.Columns["ยกเลิก"] != null && e.ColumnIndex == dataGridView1.Columns["ยกเลิก"].Index)
                {
                    frm_dialog d = new frm_dialog("ยืนยันการลบใบเสร็จ ! \r\nเลขที่ : " + invoice.invoice_code, "");
                    d.ShowDialog();
                    if (d.DialogResult == DialogResult.OK)
                    {
                        var thread1 = new Thread(showloading);
                        thread1.Start();

                        typeActive = "DEL";
                        Response resInvoice = await ApiProcess.manageInvoice(Global.BRANCH.branch_code, typeActive, invoice, Global.USER.user_login);
                        if (resInvoice.status)
                        {
                            if (packagePersons == null)
                            {
                                packagePersons.PackagePersonList = await ApiProcess.getPackageByInvoiceCode(invoice.invoice_code);
                            }

                            for (int i = 0; i < packagePersons.PackagePersonList.Count; i++)
                            {
                                PackagePerson ps = packagePersons.PackagePersonList[i];

                                ApiRest.InitailizeClient();
                                Response res = await ApiProcess.managePackageCustomer(typeActive, ps, Global.USER.user_login);
                            }
                            getInvoices();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: dataGridView1_CellClick ::" + ex.ToString(), "frm_history_inv");
            }
            finally
            {
                picLoading.Visible = false;
            }
            
        }

        
        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }

        private int rowindex = -1;
        private string invoice_code = "";

        private void dataGridView1_SelectionChanged_1(object sender, EventArgs e)
        {
           if (dataGridView1.CurrentCell != null && !invoice_code.Equals(dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells[1].Value.ToString()))
            {
                rowindex = dataGridView1.CurrentCell.RowIndex;

                invoice_code     = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                string invoice_date     = dataGridView1.Rows[rowindex].Cells[23].Value.ToString();
                string typePaymentTxt = dataGridView1.Rows[rowindex].Cells[22].Value.ToString();
                setPackagePersons(invoice_code, invoice_date, typePaymentTxt);
            }
            
        }

        private async void setPackagePersons(string invoice_code, string invoice_date, string typePaymentTxt)
        {
            try
            {
                ApiRest.InitailizeClient();
                packagePersons = new PackagePersons();

                packagePersons.PackagePersonList = await ApiProcess.getPackageByInvoiceCode(invoice_code);

                dataGridView2.Rows.Clear();
                
                for (var i = 0; i < packagePersons.PackagePersonList.Count; i++)
                {
                    PackagePerson ps = packagePersons.PackagePersonList[i];


                    Bitmap contract = global::MONKEY_APP.Properties.Resources.writing;
                    dataGridView2.Rows.Add(contract, ps.person_code, ps.reg_no,
                                     ps.package_name, ps.package_price, ps.package_num,
                                     ps.package_price_total, ps.discount, ps.vat,
                                     ps.net_total, ps.num_use, ps.package_unit,
                                     ps.date_start_txt, ps.date_expire_txt, ps.max_use,
                                     ps.type_package, ps.trainer_name, ps.percent_discount,
                                     ps.trainer_code, ps.id, ps.package_detail,
                                     ps.package_code, ps.notify_num, ps.notify_unit, 
                                     ps.type_vat, invoice_date, typePaymentTxt, ps.create_by, ps.date_start, ps.date_expire);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or เลขที่ใบเสร็จ like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();

 
                if (dataGridView1.Rows.Count == 0)
                {
                    dataGridView2.Rows.Clear();
                }

                dataGridView1_SelectionChanged(sender, e);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        
    }
}
