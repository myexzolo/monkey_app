﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MONKEY_APP
{
    class Global
    {
        public static Branch BRANCH = new Branch();
        public static User USER = new User();
        public static Person PERSON = new Person();
        
        public static Employee EMPLOYEE = new Employee();
        public static Package PACKAGE = new Package();

        public static List<Menu> MENULIST;
        public static List<MASTER> PRENAME;
        public static List<MASTER> SEX;
        public static List<MASTER> MEMBER_GROUP_AGE;
        public static List<MASTER> MEMBER_GROUP;
        public static List<MASTER> MEMBER_STATUS;
        public static List<MASTER> PACKAGE_STATUS;
        public static List<MASTER> PAY_METHOD;
        public static List<MASTER> PAYMENT_FUNC;
        public static List<MASTER> TIME_UNIT;
        public static List<MASTER> EMP_DEPARTMENT;
        public static List<MASTER> EMP_POSITION;
        //public static List<MASTER> VAT_TYPE;
        

        public static List<FingerScan> FingerScanList;

        public static List<District> DistrictList;
        public static List<Province> ProvinceList;
        public static List<Subdistrict> SubdistrictList;
        public static List<Employee> TrainerList;
        public static List<Employee> EmployeeList;
        public static List<Employee> ManagerList;
        public static List<Person> PersonList;
        public static List<Person> PersonTrainerList;
        public static List<PersonCheckin> PersonCheckinList;
        public static List<Package> PackageList;
        public static List<Invoice> InvoiceList;

    }
}
