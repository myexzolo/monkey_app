﻿using IWshRuntimeLibrary;
using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class index : Form
    {
        public string branchCode = System.Configuration.ConfigurationManager.AppSettings["BranchCode"];
        public string CURRENT_DIRECTORY = Directory.GetCurrentDirectory();
        public index()
        {
            InitializeComponent();
        }

        private void panel1_DoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void index_Load(object sender, EventArgs e)
        {
            //CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;

            var culture = new System.Globalization.CultureInfo("en-US");
            //var culture = new System.Globalization.CultureInfo("th-TH");

            Thread.CurrentThread.CurrentCulture     = culture;
            Thread.CurrentThread.CurrentUICulture   = culture;

            timer1.Start();
            checkURL();
            getMasterData();
        }

        private void checkURL()
        {
            string url = ApiRest.BaseAddress + "getVersion.php";
            Uri urlCheck = new Uri(url);
            WebRequest request = WebRequest.Create(urlCheck);
            request.Timeout = 15000;

            WebResponse response;
            try
            {
                response = request.GetResponse();
            }
            catch (Exception)
            {
                //ApiRest.BaseAddress = "http://onesittichok.com/service/";
            }
            finally {
                label2.Text = ApiRest.BaseAddress;
            }
        }

        private async void getMasterData()
        {
            ApiRest.InitailizeClient();
            try
            {
                Response vers = await ApiProcess.getVersion();
                if (vers.status)
                {
                    string newVersion = vers.message;
                    string pathFileClient = CURRENT_DIRECTORY + @"\GYM Monkey.exe";
                    if (System.IO.File.Exists(pathFileClient))
                    {
                        var versionInfoClient = FileVersionInfo.GetVersionInfo(pathFileClient);

                        var version1 = new Version(newVersion);
                        var version2 = new Version(versionInfoClient.ProductVersion.ToString());

                        var result = version1.CompareTo(version2);
                        if (result > 0)
                        {
                            CreateShortcut("ระบบบริหารจัดการฟิตเนส GYM Monkey");
                            Ftpfile.downloadFile(CURRENT_DIRECTORY);
                            
                        }
                    }
                }

                var branch = await ApiProcess.getBranch(branchCode);
                if (branch.status)
                {
                    Global.BRANCH = branch;
                }
                Global.PRENAME = await ApiProcess.getMesterDataGroupList("PRENAME");
                Global.SEX = await ApiProcess.getMesterDataGroupList("SEX");
                Global.MEMBER_GROUP_AGE = await ApiProcess.getMesterDataGroupList("MEMBER_GROUP_AGE");
                Global.MEMBER_GROUP = await ApiProcess.getMesterDataGroupList("MEMBER_GROUP");
                Global.MEMBER_STATUS = await ApiProcess.getMesterDataGroupList("MEMBER_STATUS");

                if (progressBar1.Value < 10)
                {
                    progressBar1.Value = 10;
                }

                Global.TIME_UNIT = await ApiProcess.getMesterDataGroupList("TIME_UNIT");
                Global.EMP_DEPARTMENT = await ApiProcess.getMesterDataGroupList("EMP_DEPARTMENT");
                Global.EMP_POSITION = await ApiProcess.getMesterDataGroupList("EMP_POSITION");
                //Global.VAT_TYPE = await ApiProcess.getMesterDataGroupList("VAT_TYPE");
                if (progressBar1.Value < 40)
                {
                    progressBar1.Value = 40;
                }
                //Global.SubdistrictList = await ApiProcess.getSubdistrict();

                if (progressBar1.Value < 70)
                {
                    progressBar1.Value = 70;
                }

                Global.PersonList = await ApiProcess.getPerson("ACTIVE");

                if (progressBar1.Value < 80)
                {
                    progressBar1.Value = 80;
                }

                Global.PersonTrainerList = await ApiProcess.getPerson("TRAINER");

                if (progressBar1.Value < 90)
                {
                    progressBar1.Value = 90;
                }

                Global.PackageList = await ApiProcess.getPackage("'Y'");

                if (progressBar1.Value < 100)
                {
                    progressBar1.Value = 100;
                }
                timer1.Stop();

                this.DialogResult = DialogResult.OK;
            }
            catch
            {
                timer1.Stop();
                frm_dialog d = new frm_dialog("ไม่สามารถเชื่อมต่อ Server ได้!!", "I");
                d.ShowDialog();
                this.DialogResult = DialogResult.Cancel;
            }
            
        }
        public static void CreateShortcut(string linkName)
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            + Path.DirectorySeparatorChar + linkName + ".lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            //shortcut...
            shortcut.Save();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
        }
    }
}
