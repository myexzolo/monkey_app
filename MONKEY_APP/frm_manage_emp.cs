﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThaiNationalIDCard;

namespace MONKEY_APP
{
    public partial class frm_manage_emp : Form
    {
        private ThaiIDCard idcard;

        private string typeActive;
        private string picEmp = "";
        public Employee employee;

        private class Item
        {
            public string Name;
            public string Value;
            public Item(string name, string value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }

        public frm_manage_emp(string typeActive,Employee employee)
        {
            
            InitializeComponent();
            this.typeActive = typeActive;
            this.employee = employee;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void xx_Click(object sender, EventArgs e)
        {
            
        }

        private async void genEmpCode()
        {

            try
            {
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.genEmpCode(Global.BRANCH.branch_code);
                empCode.Text = res.message;
                if (Global.BRANCH.branch_code.Equals("GYMMK01")) {
                    chkGm1.Checked = true;
                } else if (Global.BRANCH.branch_code.Equals("GYMMK02")) {
                    chkGm2.Checked = true;
                }
                else if (Global.BRANCH.branch_code.Equals("GYMMK03"))
                {
                    //chkGm3.Checked = true;
                }
                else if (Global.BRANCH.branch_code.Equals("GYMMK04"))
                {
                    //chkGm4.Checked = true;
                }
                else if (Global.BRANCH.branch_code.Equals("GYMMK05"))
                {
                    //chkGm5.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: genEmpCode ::" + ex.ToString(), "frm_manage_emp");
            }
            finally {

                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }


        private async void LoadEmp()
        {
            try
            {
                ApiRest.InitailizeClient();
                var thread1 = new Thread(showloading);
                thread1.Start();
                if (Global.ProvinceList == null)
                {
                    Global.ProvinceList = await ApiProcess.getProvince();
                }

                if (Global.DistrictList == null)
                {
                    Global.DistrictList = await ApiProcess.getDistrict();
                }

                if (Global.SubdistrictList == null)
                {
                    Global.SubdistrictList = await ApiProcess.getSubdistrict();
                }

                for (int x = 0; x < Global.ProvinceList.Count; x++)
                {
                    Province data = Global.ProvinceList[x];
                    txtaddrProvince.Items.Add(new Item(data.PROVINCE_NAME, data.PROVINCE_CODE));
                }

                idcard = new ThaiIDCard();
                nameUser.Text = Global.USER.user_name + " " + Global.USER.user_last;
                user_code.Text = Global.USER.user_login;


                for (int x = 0; x < Global.PRENAME.Count; x++)
                {
                    MASTER master = Global.PRENAME[x];
                    comboTitle.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }

                for (int x = 0; x < Global.EMP_DEPARTMENT.Count; x++)
                {
                    MASTER master = Global.EMP_DEPARTMENT[x];
                    comboDepartment.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }

                for (int x = 0; x < Global.EMP_POSITION.Count; x++)
                {
                    MASTER master = Global.EMP_POSITION[x];
                    comboPosition.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }

                for (int x = 0; x < Global.SEX.Count; x++)
                {
                    MASTER master = Global.SEX[x];
                    comboSex.Items.Add(new Item(master.DATA_DESC1, master.DATA_CODE));
                }
                if (typeActive.Equals("ADD"))
                {
                    genEmpCode();
                }
                else if (typeActive.Equals("EDIT"))
                {
                    label1.Text = "แก้ไขทะเบียนพนักงาน";
                    getEmployeeById(employee.EMP_CODE);
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: LoadEmp ::" + ex.ToString(), "frm_manage_emp");
            }
            finally
            {

                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }

        private void frm_manage_cus_Load(object sender, EventArgs e)
        {
            LoadEmp();
           
        }

        private async void getEmployeeById(string emp_code)
        {
            try
            {
                ApiRest.InitailizeClient();
                Employee emp = await ApiProcess.getEmployeeById(Global.BRANCH.branch_code, emp_code);

                if (Global.ProvinceList == null)
                {
                    Global.ProvinceList = await ApiProcess.getProvince();

                }

                if (Global.DistrictList == null)
                {
                    Global.DistrictList = await ApiProcess.getDistrict();
                }
                empCode.Text = emp.EMP_CODE;
                comboTitle.Text = emp.EMP_TITLE;
                txtName.Text = emp.EMP_NAME;
                txtLname.Text = emp.EMP_LASTNAME;
                bod.Text = emp.EMP_BIRTH_DATE;
                dateIncome.Text = emp.EMP_DATE_INCOME;
                txtNickName.Text = emp.EMP_NICKNAME;
                comboSex.Text = emp.EMP_SEX;
                txtId.Text = emp.EMP_CARD_ID;
                textTel.Text = emp.EMP_TEL;
                txtZip.Text = emp.EMP_ADDR_POSTAL;
                txtAddress1.Text = emp.EMP_ADDRESS1;
                txtAddress2.Text = emp.EMP_ADDRESS2;
                txtaddrProvince.Text = emp.EMP_ADDR_PROVINCE;
                textaddrAmphur.Text = emp.EMP_ADDR_DISTRICT;
                textaddrTambol.Text = emp.EMP_ADDR_SUBDISTRICT;
                txtWage.Text  = emp.EMP_WAGE;

                if (emp.branch1 != null && emp.branch1.Equals("Y"))
                {
                    chkGm1.Checked = true;
                }

                if (emp.branch2 != null && emp.branch2.Equals("Y"))
                {
                    chkGm2.Checked = true;
                }

                //if (emp.branch3 != null && emp.branch3.Equals("Y"))
                //{
                //    chkGm3.Checked = true;
                //}

                //if (emp.branch4 != null && emp.branch4.Equals("Y"))
                //{
                //    chkGm4.Checked = true;
                //}

                //if (emp.branch5 != null && emp.branch5.Equals("Y"))
                //{
                //    chkGm5.Checked = true;
                //}


                if (emp.EMP_IS_SALE.Equals("Y"))
                {
                    checkSale.Checked = true;
                }
                if (emp.EMP_IS_TRAINER.Equals("Y"))
                {
                    checkTrainer.Checked = true;
                }
                if (emp.EMP_IS_STAFF.Equals("Y"))
                {
                    checkManager.Checked = true;
                }
                if (emp.EMP_IS_INSTRUCTOR.Equals("Y"))
                {
                    checkInstructor.Checked = true;
                }
                

                for (int x = 0; x < Global.EMP_POSITION.Count; x++)
                {
                    MASTER master = Global.EMP_POSITION[x];
                    if (emp.EMP_POSITION == master.DATA_CODE)
                    {
                        comboPosition.SelectedIndex = x;
                    }
                }

                for (int x = 0; x < Global.EMP_DEPARTMENT.Count; x++)
                {
                    MASTER master = Global.EMP_DEPARTMENT[x];
                    if (emp.EMP_DEPARTMENT == master.DATA_CODE)
                    {
                        comboDepartment.SelectedIndex = x;
                    }
                }

                if (!emp.user_img.Equals(""))
                {
                    Utils.setImage(emp.user_img, pictureBox2);
                }

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getEmployeeById ::" + ex.ToString(), "frm_manage_emp");
            }
            finally
            {
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Refresh();

            var thread1 = new Thread(showloading);
            var thread2 = new Thread(readCard);
            thread1.Start();
            thread2.Start();
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else {
                picLoading.Visible = true;
            }
            
        }

        public static byte[] ImageToByte(Image img)
        {
            byte[] byteArr;
            try
            {
                ImageConverter converter = new ImageConverter();
                var i2 = new Bitmap(img);
                byteArr = (byte[])converter.ConvertTo(i2, typeof(byte[]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Utils.getErrorToLog(":: ImageToByte ::" + ex.ToString(), "frm_manage_emp");
                return null;
            }
            return byteArr;
        }

        private void readCard()
        {
            try
            {
                string[] readers = idcard.GetReaders();

                Personal personal = idcard.readAllPhoto();
                if (personal != null)
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            txtName.Text = personal.Th_Firstname;
                            txtLname.Text = personal.Th_Lastname;
                            txtId.Text = personal.Citizenid;
                            pictureBox2.BackgroundImage = personal.PhotoBitmap;

                            byte[] imgArray = ImageToByte(personal.PhotoBitmap);
                            picEmp = Convert.ToBase64String(imgArray);
                            //picEmp = System.Text.Encoding.Unicode.GetString(imgArray);

                            txtAddress1.Text = personal.addrHouseNo + " " + personal.addrVillageNo;
                            txtAddress2.Text = personal.addrLane + " " + personal.addrRoad;
                            txtaddrProvince.Text = personal.addrProvince;
                            textaddrAmphur.Text = personal.addrAmphur;
                            textaddrTambol.Text = personal.addrTambol;


                            comboSex.SelectedIndex = Int32.Parse(personal.Sex); 



                            comboTitle.Text = personal.Th_Prefix;
                            bod.Text = personal.Birthday.ToLongDateString();


                            var today = DateTime.Today;
                            var age = today.Year - personal.Birthday.Year;

                            if (personal.Birthday > today.AddYears(-age)) age--;

                            
                            picLoading.Visible = false;
                        }));
                    }
                    else
                    {
                        txtName.Text = personal.Th_Firstname;
                        txtLname.Text = personal.Th_Lastname;
                        txtId.Text = personal.Citizenid;
                        pictureBox2.BackgroundImage = personal.PhotoBitmap;
                        txtAddress1.Text = personal.addrHouseNo + " " + personal.addrVillageNo;
                        txtAddress2.Text = personal.addrLane + " " + personal.addrRoad;
                        txtaddrProvince.Text = personal.addrProvince;
                        textaddrAmphur.Text = personal.addrAmphur;
                        textaddrTambol.Text = personal.addrTambol;

                        byte[] imgArray = ImageToByte(personal.PhotoBitmap);
                        picEmp = Convert.ToBase64String(imgArray);

                        comboSex.SelectedIndex = Int32.Parse(personal.Sex);



                        comboTitle.Text = personal.Th_Prefix;
                        bod.Text = personal.Birthday.ToLongDateString();


                        var today = DateTime.Today;
                        var age = today.Year - personal.Birthday.Year;

                        if (personal.Birthday > today.AddYears(-age)) age--;


                        picLoading.Visible = false;
                    }

                }
                else if (idcard.ErrorCode() > 0)
                {
                    //MessageBox.Show(idcard.Error() + " " + idcard.ErrorCode());
                    if (idcard.ErrorCode() == 256)
                    {
                        MessageBox.Show("ไม่พบบัตร Smartcard");
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถอ่านบัตร Smartcard ได้");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Catch all");
                }
            }
            catch
            {
                MessageBox.Show("ไม่พบเครื่องอ่านบัตร Smartcard");
            }
            finally {
                if (picLoading.InvokeRequired)
                {
                    picLoading.Invoke(new MethodInvoker(delegate
                    {
                        picLoading.Visible = false;
                    }));
                }
                else
                {
                    picLoading.Visible = false;
                }
            }
        }

        public byte[] ConvertBitMapToByteArray(Bitmap bitmap)
        {
            byte[] result = null;

            if (bitmap != null)
            {
                MemoryStream stream = new MemoryStream();
                bitmap.Save(stream, bitmap.RawFormat);
                result = stream.ToArray();
            }

            return result;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtaddrProvince_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private async void txtaddrProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrAmphur.Items.Clear();
                textaddrTambol.Items.Clear();
                txtZip.Text = "";
                textaddrAmphur.Text = "";
                textaddrTambol.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                if (Global.DistrictList == null)
                {
                    Global.DistrictList = await ApiProcess.getDistrict();
                }

                for (int x = 0; x < Global.DistrictList.Count; x++)
                {
                    District data = Global.DistrictList[x];
                    if (data.PROVINCE_CODE.Equals(item.Value))
                    {
                        textaddrAmphur.Items.Add(new Item(data.DISTRICT_NAME, data.DISTRICT_CODE));   
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: txtaddrProvince_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_emp");
            }
            
        }

        private async void textaddrAmphur_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                textaddrTambol.Items.Clear();
                txtZip.Text = "";
                textaddrTambol.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                if (Global.SubdistrictList == null)
                {
                    Global.SubdistrictList = await ApiProcess.getSubdistrict();
                }

                for (int x = 0; x < Global.SubdistrictList.Count; x++)
                {
                    Subdistrict data = Global.SubdistrictList[x];
                    if (data.DISTRICT_CODE.Equals(item.Value))
                    {
                        textaddrTambol.Items.Add(new Item(data.SUBDISTRICT_NAME, data.SUBDISTRICT_CODE));
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: textaddrAmphur_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_emp");
            }
        }

        private void textaddrTambol_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtZip.Text = "";

                ComboBox cmb = (ComboBox)sender;
                int selectedIndex = cmb.SelectedIndex;
                string selectedValue = (string)cmb.SelectedValue;

                Item item = (Item)cmb.SelectedItem;

                txtZip.Text = item.Value;

            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: textaddrTambol_SelectedIndexChanged ::" + ex.ToString(), "frm_manage_emp");
            }
        }

        private void setAlertMessage(String str)
        {
            if (this.InvokeRequired)
            {

                this.Invoke(new MethodInvoker(delegate
                {
                    alertTxt.Visible = true;
                    alertTxt.Text = str;
                }));
            }
            else
            {
                alertTxt.Visible = true;
                alertTxt.Text = str;
            }
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var thread1 = new Thread(showloading);

                alertTxt.Text = "";
                alertTxt.Visible = false;
                this.DialogResult = DialogResult.None;

                if (comboTitle.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ คำนำหน้าชื่อ **");
                    comboTitle.Focus();
                }
                else if (txtName.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อ **");
                    txtName.Focus();
                }
                else if (txtLname.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกนามสกุล **");
                    txtLname.Focus();
                }
                else if (txtNickName.Text.Trim().Equals(""))
                {

                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกชื่อเล่น **");
                    txtNickName.Focus();
                }
                else if (comboSex.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุ เพศ **");
                    comboSex.Focus();
                }
                else if (txtId.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกเลขที่บัตรประชาชน **");
                    txtId.Focus();
                }
                else if (comboPosition.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุตำแหน่ง **");
                    comboPosition.Focus();
                }
                else if (comboDepartment.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุแผนก/สังกัด **");
                    comboDepartment.Focus();
                }
                else if (textTel.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกเบอร์โทร **");
                    textTel.Focus();
                }
                else if (txtAddress1.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณากรอกที่อยู่ **");
                    txtAddress1.Focus();
                }
                else if (txtaddrProvince.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุจังหวัด **");
                    txtaddrProvince.Focus();
                }
                else if (textaddrAmphur.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุเขต/อำเภอ **");
                    textaddrAmphur.Focus();
                }
                else if (textaddrTambol.Text.Trim().Equals(""))
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุแขวง/ตำบล **");
                    textaddrTambol.Focus();
                }
                else if (checkInstructor.Checked && txtWage.Value == 0)
                {
                    alertTxt.Visible = true;
                    setAlertMessage("** กรุณาระบุจำนวนค่าจ้าง **");
                    txtWage.Focus();
                }
                else
                {
                    Employee emp = new Employee();
                    thread1.Start();
                    emp.EMP_CODE = empCode.Text;
                    emp.EMP_TITLE = comboTitle.Text;
                    emp.EMP_NAME = txtName.Text;
                    emp.EMP_LASTNAME = txtLname.Text;
                    emp.EMP_POSITION = (comboPosition.SelectedItem as Item).Value.ToString();
                    emp.EMP_DEPARTMENT = (comboDepartment.SelectedItem as Item).Value.ToString();
                    emp.EMP_SEX = comboSex.Text;
                    emp.EMP_BIRTH_DATE = bod.Value.ToString("yyyy-MM-dd");
                    emp.EMP_TYPE = "";
                    emp.EMP_GROUP = "";
                    emp.EMP_NICKNAME = txtNickName.Text;
                    emp.EMP_DATE_INCOME = dateIncome.Value.ToString("yyyy-MM-dd");
                    emp.EMP_CARD_ID = txtId.Text;
                    emp.EMP_ADDRESS1 = txtAddress1.Text;
                    emp.EMP_ADDRESS2 = txtAddress2.Text;
                    emp.EMP_ADDR_PROVINCE = (txtaddrProvince.SelectedItem as Item).Name.ToString();
                    emp.EMP_ADDR_DISTRICT = (textaddrAmphur.SelectedItem as Item).Name.ToString();
                    emp.EMP_ADDR_SUBDISTRICT = (textaddrTambol.SelectedItem as Item).Name.ToString();
                    emp.EMP_ADDR_POSTAL = txtZip.Text;
                    emp.EMP_EMAIL = "";
                    emp.EMP_TEL = textTel.Text;
                    emp.EMP_WAGE = txtWage.Text;

                    if (checkSale.Checked)
                    {
                        emp.EMP_IS_SALE = "Y";
                    }

                    if (checkTrainer.Checked)
                    {
                        emp.EMP_IS_TRAINER = "Y";
                    }
                    if (checkManager.Checked)
                    {
                        emp.EMP_IS_STAFF = "Y";
                    }
                    if (checkInstructor.Checked)
                    {
                        emp.EMP_IS_INSTRUCTOR = "Y";
                    }

                    if (chkGm1.Checked)
                    {
                        emp.branch1 = "Y";
                    }

                    if (chkGm2.Checked)
                    {
                        emp.branch2 = "Y";
                    }


                    emp.EMP_PICTURE = picEmp;


                    ApiRest.InitailizeClient();
                    Response res = await ApiProcess.manageEmployee(Global.BRANCH.branch_code, typeActive, emp, Global.USER.user_login);
                    if (res.status)
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        alertTxt.Visible = true;
                        alertTxt.Text = "** ไม่สามารถบันทึกได้ **";
                        picLoading.Visible = false;
                    }
 
                }
            }
            catch
            {
                alertTxt.Visible = true;
                alertTxt.Text = "** ไม่สามารถเชื่อมต่อ Server ได้ **";
                picLoading.Visible = false;
            }
            finally
            {
                picLoading.Visible = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Image files(*.jpg, *.jpeg, *.jpe, *.jfif, *.png)|*.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dialog.ShowDialog() == DialogResult.OK) {
                    Bitmap imgUser = new Bitmap(dialog.FileName);
                    pictureBox2.BackgroundImage = imgUser;

                    Bitmap imgUsertmp = new Bitmap(imgUser,148, 178);
                    byte[] imgArray = ImageToByte(imgUsertmp);
                    picEmp = Convert.ToBase64String(imgArray);

                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: button2_Click ::" + ex.ToString(), "frm_manage_cus");
            }
        }
    }
}
