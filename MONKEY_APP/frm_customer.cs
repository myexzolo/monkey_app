﻿using MONKEY_APP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MONKEY_APP
{
    public partial class frm_customer : Form
    {
        frm_main fm;
        public frm_customer(frm_main fm)
        {
            this.fm = fm;
            InitializeComponent();
            
        }

        private DataTable dtm;
        //private DataView dv;


        private string active = "";

        private string picCus = "";

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();

                if (dataGridView1.Rows.Count > 0)
                {
                    rowindex = -1;
                }
                else if (dataGridView1.Rows.Count == 0)
                {
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    perSonCode = "";
                    Global.PERSON = new Person();
                    clearLab();
                    clearDataPerson();
                }

                if (txtSearch.Text.Equals(""))
                {
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    perSonCode = "";
                    Global.PERSON = new Person();
                    clearLab();
                    clearDataPerson();
                }
                else
                {
                    dataGridView1_CellContentClick(sender, null);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void showloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = true;
                }));
            }
            else
            {
                picLoading.Visible = true;
            }

        }


        private void hideloading()
        {
            if (picLoading.InvokeRequired)
            {
                picLoading.Invoke(new MethodInvoker(delegate
                {
                    picLoading.Visible = false;
                }));
            }
            else
            {
                picLoading.Visible = false;
            }

        }


        private void frm_customer_Load(object sender, EventArgs e)
        {

            var thread1 = new Thread(showloading);
            thread1.Start();

            clearLab();

            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
            int scrHeight =  Screen.PrimaryScreen.Bounds.Height;

            int panelw1 = scrWidth - 210;
       
            int panelM  = panelw1 / 100 * 60;

            panelMemberList.Width = panelM;
            panelMemberList.Height = scrHeight - 265;
            panelpakege.Width = panelw1 / 100 * 36;
            panelpakege.Height = scrHeight - 219;

            int panelpakX = panelM + 60;
            this.panelpakege.Location = new System.Drawing.Point(panelpakX, 82);

            int num = 0;
            tableLayoutPanel.ColumnCount = (num + 1);

            List<Button> buttons = new List<Button>();

            for (var i = 0; i < Global.MENULIST.Count; i++)
            {
                string btnName = "";
                int width = 125;
                string menuName = Global.MENULIST[i].page_name;
                string code = Global.MENULIST[i].page_code;
                string moduleId = Global.MENULIST[i].module_id;
                Image img = global::MONKEY_APP.Properties.Resources.folder2;

                if (moduleId.Equals("4"))
                {

                    if (code.Equals("AMEM"))
                    {
                        width = 140;
                        img = global::MONKEY_APP.Properties.Resources.folder__2_;
                    }
                    else if (code.Equals("EMEM"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.folder2;
                    }
                    else if (code.Equals("DMEM"))
                    {
                        width = 140;
                        img = global::MONKEY_APP.Properties.Resources.del;
                    }
                    else if (code.Equals("TPAG"))
                    {
                        width = 140;
                        img = global::MONKEY_APP.Properties.Resources.trans;
                    }
                    else if (code.Equals("AFIN"))
                    {
                        width = 150;
                        img = global::MONKEY_APP.Properties.Resources.fingerprints2;
                    }
                    else if (code.Equals("TFIN"))
                    {
                        width = 180;
                        img = global::MONKEY_APP.Properties.Resources.fingerprint3;
                    }

                    btnName = code;
                    buttons.Add(genButtonMenu(btnName, width, menuName, img));
                    num++;
                }

            }

            Button button = genButtonMenu("VMEM", 157, "ดูข้อมูลทั้งหมด", global::MONKEY_APP.Properties.Resources.eye2);

            buttons.Add(button);
            tableLayoutPanel.ColumnCount = buttons.Count;

            for (var i = 0; i < buttons.Count; i++)
            {
                Button btn = buttons[i];
                tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
                tableLayoutPanel.Controls.Add(btn, i, 0);
            }

            txtSearch.Focus();
            getPersons("ACTIVE");
        }


        private async void getProductByPersonId(string personCode)
        {
            try
            {
                ApiRest.InitailizeClient();
                var personList = await ApiProcess.getPerson(personCode);

                dataGridView1.DataSource = dtm;

                dataGridView1.Columns[0].Width = 50;
                dataGridView1.Columns[1].Width = 100;
                dataGridView1.Columns[2].Width = 100;
                dataGridView1.Columns[3].Width = 100;
                dataGridView1.Columns[4].Width = 150;
                dataGridView1.Columns[5].Width = 180;
                dataGridView1.Columns[6].Width = 100;
                dataGridView1.Columns[7].Width = 100;
                dataGridView1.Columns[8].Width = 120;


                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[11].Visible = false;
                dataGridView1.Columns[12].Visible = false;


                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getProductByPersonId ::" + ex.ToString(), "frm_customer");
            }

        }



        private async void getPersons(string typeSearch)
        {
            try
            {
                var thread1 = new Thread(showloading);
                thread1.Start();

                if (active != null && (active.Equals("ADD") || active.Equals("EDIT") || active.Equals("ADDF") || active.Equals("REF")) || active.Equals("DEL"))
                {
                    ApiRest.InitailizeClient();
                    Global.PersonList = await ApiProcess.getPerson(typeSearch);
                }

                dtm = new DataTable();

                dtm.Columns.Add("ลายนิ้วมือ", typeof(bool));
                dtm.Columns.Add("รหัส", typeof(string));
                dtm.Columns.Add("ชื่อเล่น", typeof(string));
                dtm.Columns.Add("คำนำหน้า", typeof(string));
                dtm.Columns.Add("ชื่อ", typeof(string));
                dtm.Columns.Add("นามสกุล", typeof(string));
                dtm.Columns.Add("วันเกิด", typeof(string));
                dtm.Columns.Add("เริ่มวันที่", typeof(string));
                dtm.Columns.Add("สิ้นสุดวันที่", typeof(string));
                dtm.Columns.Add("Note", typeof(string));
                dtm.Columns.Add("รูป", typeof(string));
                dtm.Columns.Add("สถานะ", typeof(string));
                dtm.Columns.Add("index", typeof(int));
                dtm.Columns.Add("regDate", typeof(string));
                dtm.Columns.Add("expDate", typeof(string));
                dtm.Columns.Add("เบอร์โทร", typeof(string));
                dtm.Columns.Add("เบอร์โทรฉุกเฉิน", typeof(string));
                dtm.Columns.Add("รหัสผ่าน", typeof(string));


                for (int i = 0; i < Global.PersonList.Count(); i++)
                {
                    Person person = Global.PersonList[i];

                    bool fgn = false;

                    if (person.FNG_TEMPLATE1 != null && !person.FNG_TEMPLATE1.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (person.FNG_TEMPLATE2 != null && !person.FNG_TEMPLATE2.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (person.FNG_TEMPLATE3 != null && !person.FNG_TEMPLATE3.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (person.FNG_TEMPLATE4 != null && !person.FNG_TEMPLATE4.Equals(""))
                    {
                        fgn = true;
                    }
                    else if (person.FNG_TEMPLATE5 != null && !person.FNG_TEMPLATE5.Equals(""))
                    {
                        fgn = true;
                    }

                    dtm.Rows.Add(fgn,
                        person.PERSON_CODE, person.PERSON_NICKNAME, person.PERSON_TITLE,
                        person.PERSON_NAME, person.PERSON_LASTNAME, person.PERSON_BIRTH_DATE_TXT,
                        person.PERSON_REGISTER_DATE_TXT, person.PERSON_EXPIRE_DATE_TXT, person.PERSON_NOTE, 
                        person.PERSON_IMAGE, person.PERSON_STATUS, i, person.PERSON_REGISTER_DATE, 
                        person.PERSON_EXPIRE_DATE, person.PERSON_TEL_MOBILE, person.PERSON_ER_TEL, person.PERSON_USER_PSW
                    );
                }

                dataGridView1.DataSource = dtm;

                dataGridView1.Columns[0].Width = 85;
                dataGridView1.Columns[1].Width = 90;
                dataGridView1.Columns[2].Width = 80;
                dataGridView1.Columns[3].Width = 90;
                dataGridView1.Columns[4].Width = 130;
                dataGridView1.Columns[5].Width = 169;
                dataGridView1.Columns[6].Width = 95;
                dataGridView1.Columns[7].Width = 95;
                dataGridView1.Columns[8].Width = 95;
                dataGridView1.Columns[15].Width = 95;
                dataGridView1.Columns[16].Width = 120;
                dataGridView1.Columns[17].Width = 100;

                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[11].Visible = false;
                dataGridView1.Columns[12].Visible = false;
                dataGridView1.Columns[13].Visible = false;
                dataGridView1.Columns[14].Visible = false;

                dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[15].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[16].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.Columns[17].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[8].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[15].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[16].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[17].SortMode = DataGridViewColumnSortMode.NotSortable;

                if (rowindexTmp >= 0 && active.Equals("DEL"))
                {
                    dataGridView1.Rows[rowindexTmp].Selected = true;
                    rowindexTmp = -1;
                }

                if (!txtSearch.Text.Equals(""))
                {
                    button7_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: getPersons ::" + ex.ToString(), "frm_customer");
            }
            finally
            {
                picLoading.Visible = false;
            }
            

        }

        private int GetDisplayedRowsCount()
        {
            int count = dataGridView1.Rows[dataGridView1.FirstDisplayedScrollingRowIndex].Height;
            count = dataGridView1.Height / count;
            return count;
        }



        private DataGridViewTextBoxColumn setColumnTextBox(string HeaderText, string Name, int Width) {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

            column.HeaderText = HeaderText;
            column.Name = Name;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.Width = Width;

            return column;
        }

        private Button genButtonMenu(string btnName, int width, string menuName, Image img)
        {
            Button btn = new Button();
            btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.Font = new System.Drawing.Font("TH SarabunPSK", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(67)))), ((int)(((byte)(66)))));
            btn.Image = img;
            btn.Name = btnName;
            btn.Tag = btnName;
            btn.Size = new System.Drawing.Size(width, 40);
            btn.Text = menuName;
            btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btn.UseVisualStyleBackColor = false;
            btn.Click += new System.EventHandler(this.buttonClick);

            return btn;
        }

        int indexPerson = 0;
        private async void buttonClick(object sender, EventArgs e)
        {
            string code = ((Button)sender).Tag.ToString();

            active = "";
            if (code.Equals("AMEM"))
            {
                dataGridView1.ClearSelection();
                dataGridView2.Rows.Clear();
                clearLab();
                clearDataPerson();
                perSonCode = "";
                Global.PERSON = new Person();
                frm_manage_cus fmc = new frm_manage_cus("ADD");
                var result = fmc.ShowDialog();
                if (result == DialogResult.OK)
                {
                    active = "ADD";
                    getPersons("ACTIVE");
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    clearLab();
                    perSonCode = "";
                    Global.PERSON = new Person();

                    if (!txtSearch.Text.Equals(""))
                    {
                        button7_Click(null, null);
                    }
                }
            }
            if (code.Equals("TPAG"))
            {
                if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    frm_manage_transfers fmc = new frm_manage_transfers();
                    var result = fmc.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        active = "EDIT";
                        dataGridView1.ClearSelection();
                        dataGridView2.Rows.Clear();
                        clearLab();
                        perSonCode = "";
                        Global.PERSON = new Person();

                        if (!txtSearch.Text.Equals(""))
                        {
                            button7_Click(null, null);
                        }
                    }
                }

            }
            else if (code.Equals("EMEM"))
            {
                if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    perSonCode = "";
                    frm_manage_cus fmc = new frm_manage_cus("EDIT");
                    var result = fmc.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        rowindexTmp = rowindex;
                        active = "EDIT";

                        Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, Global.PERSON.PERSON_CODE);

                        bool fgn = false;

                        if (person.FNG_TEMPLATE1 != null && !person.FNG_TEMPLATE1.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE2 != null && !person.FNG_TEMPLATE2.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE3 != null && !person.FNG_TEMPLATE3.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE4 != null && !person.FNG_TEMPLATE4.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE5 != null && !person.FNG_TEMPLATE5.Equals(""))
                        {
                            fgn = true;
                        }


                        dataGridView1.Rows[rowindex].Cells[0].Value = fgn;
                        dataGridView1.Rows[rowindex].Cells[1].Value = person.PERSON_CODE;
                        dataGridView1.Rows[rowindex].Cells[2].Value = person.PERSON_NICKNAME;
                        dataGridView1.Rows[rowindex].Cells[3].Value = person.PERSON_TITLE;
                        dataGridView1.Rows[rowindex].Cells[4].Value = person.PERSON_NAME;
                        dataGridView1.Rows[rowindex].Cells[5].Value = person.PERSON_LASTNAME;
                        dataGridView1.Rows[rowindex].Cells[6].Value = person.PERSON_BIRTH_DATE_TXT;
                        dataGridView1.Rows[rowindex].Cells[7].Value = person.PERSON_REGISTER_DATE_TXT;
                        dataGridView1.Rows[rowindex].Cells[8].Value = person.PERSON_EXPIRE_DATE_TXT;
                        dataGridView1.Rows[rowindex].Cells[9].Value = person.PERSON_NOTE;
                        dataGridView1.Rows[rowindex].Cells[10].Value = person.PERSON_IMAGE;
                        dataGridView1.Rows[rowindex].Cells[11].Value = person.PERSON_STATUS;
                        dataGridView1.Rows[rowindex].Cells[13].Value = person.PERSON_REGISTER_DATE;
                        dataGridView1.Rows[rowindex].Cells[14].Value = person.PERSON_EXPIRE_DATE;

                        //dataGridView1.Rows[rowindex].Selected = true;
                        Global.PersonList[indexPerson] = person;

                        foreach (DataRow dr in dtm.Rows) // search whole table
                        {
                            if (dr[1].ToString() == person.PERSON_CODE)
                            {
                                dr[0] = fgn;
                                dr[1] = person.PERSON_CODE;
                                dr[2] = person.PERSON_NICKNAME;
                                dr[3] = person.PERSON_TITLE;
                                dr[4] = person.PERSON_NAME;
                                dr[5] = person.PERSON_LASTNAME;
                                dr[6] = person.PERSON_BIRTH_DATE_TXT;
                                dr[7] = person.PERSON_REGISTER_DATE_TXT;
                                dr[8] = person.PERSON_EXPIRE_DATE_TXT;
                                dr[9] = person.PERSON_NOTE;
                                dr[10] = person.PERSON_IMAGE;
                                dr[11] = person.PERSON_STATUS;
                                dr[13] = person.PERSON_REGISTER_DATE;
                                dr[14] = person.PERSON_EXPIRE_DATE;
                            }
                        }
                        //getPersons("ACTIVE");

                        if (!txtSearch.Text.Equals(""))
                        {
                            button7_Click(null, null);
                        }
                    }
                }
            }
            else if (code.Equals("DMEM"))
            {
                if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    frm_dialog d = new frm_dialog("ยืนยันการลบสมาชิก ! \r\n" + Global.PERSON.PERSON_CODE + ":" + Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME, "");
                    d.ShowDialog();
                    if (d.DialogResult == DialogResult.OK)
                    {
                        delData();
                    }
                }
            }
            else if (code.Equals("AFIN"))
            {
                if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    frm_fg fgc = new frm_fg(Global.PERSON.PERSON_CODE, Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME, "PERSON");
                    var result = fgc.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        rowindexTmp = rowindex;
                        active = "ADDF";

                        Person person = await ApiProcess.getPersonById(Global.BRANCH.branch_code, Global.PERSON.PERSON_CODE);

                        bool fgn = false;

                        if (person.FNG_TEMPLATE1 != null && !person.FNG_TEMPLATE1.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE2 != null && !person.FNG_TEMPLATE2.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE3 != null && !person.FNG_TEMPLATE3.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE4 != null && !person.FNG_TEMPLATE4.Equals(""))
                        {
                            fgn = true;
                        }
                        else if (person.FNG_TEMPLATE5 != null && !person.FNG_TEMPLATE5.Equals(""))
                        {
                            fgn = true;
                        }

                        dataGridView1.Rows[rowindex].Cells[0].Value = fgn;
                    }
                }
            }
            else if (code.Equals("TFIN"))
            {
                if (Global.PERSON.PERSON_NAME != null && !Global.PERSON.PERSON_NAME.Equals(""))
                {
                    frm_fg_test fgc = new frm_fg_test(Global.PERSON.PERSON_CODE, Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME, "PERSON");
                    var result = fgc.ShowDialog();
                }
            }
            else if (code.Equals("VMEM"))
            {
                ;
            }

            
        }

        private async void delData()
        {
            try
            {
                active = "DEL";
                ApiRest.InitailizeClient();
                Response res = await ApiProcess.managePerson(Global.BRANCH.branch_code, active, Global.PERSON, Global.USER.user_login);
                if (res.status)
                {
                    clearLab();
                    clearDataPerson();
                    getPersons("ACTIVE");
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    perSonCode = "";
                    Global.PERSON = new Person();                    
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: delData ::" + ex.ToString(), "frm_customer");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private int rowindex = -1;
        private int rowindexTmp = -1;
        private string perSonCode = "";



        private async void setPersonPackage(string personCode)
        {
            
            try
            {
               ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPackageByPersonCode(personCode);
                dataGridView2.Rows.Clear();
                clearLab();
                DateTime dDate;
                for (var i = 0; i < psList.Count; i++)
                {
                    PackagePerson ps = psList[i];
                    if (!ps.person_code.Equals(Global.PERSON.PERSON_CODE)) {
                        dataGridView2.Rows.Clear();
                        clearLab();
                        break;
                    }

                    if (ps.invoiceDateTxt == null) {
                        ps.invoiceDateTxt = "";
                    }

                    dataGridView2.Rows.Add(ps.package_name, ps.date_start_txt, ps.date_expire_txt,
                        ps.use_package+"/"+ps.num_use, ps.package_unit, ps.reg_no, ps.invoice_code, ps.status, ps.package_detail,ps.invoiceDateTxt,ps.package_code);
                }
                //dataGridView2.Refresh();
            }
            catch (Exception ex) {
                Utils.getErrorToLog(":: setPersonPackage ::" + ex.ToString(), "frm_customer");

            }    
        }

        private async void setPersonCheckin(string stausCheckin,string typePakage)
        {

            try
            {
                labDC1.Text = "";
                labDC2.Text = "";
                labDC3.Text = "";
                labDC4.Text = "";

                labTC1.Text = "";
                labTC2.Text = "";
                labTC3.Text = "";
                labTC4.Text = "";

                labCC1.Text = "";
                labCC2.Text = "";
                labCC3.Text = "";
                labCC4.Text = "";

                ApiRest.InitailizeClient();
                var psList = await ApiProcess.getPersonCheckin(Global.BRANCH.branch_code, stausCheckin, Global.PERSON.PERSON_CODE, "4", typePakage, "DESC","");

                DateTime dDate;
                for (var i = 0; i < psList.Count; i++)
                {
                    PersonCheckin ps = psList[i];

                    string d1 = "";
                    string d2 = "";

                    if (DateTime.TryParse(ps.checkin_date, out dDate))
                    {
                        string[] d = ps.checkin_date.Split(' ');
                        d1 = d[0];
                        d2 = d[1];

                        DateTime de = Convert.ToDateTime(d1);
                        d1 = de.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    }

                    if (i == 0)
                    {
                        labDC1.Text = d1;
                        labTC1.Text = d2;
                        labCC1.Text = ps.cname;
                    }
                    else if (i == 1)
                    {
                        labDC2.Text = d1;
                        labTC2.Text = d2;
                        labCC2.Text = ps.cname;
                    }
                    else if (i == 2)
                    {
                        labDC3.Text = d1;
                        labTC3.Text = d2;
                        labCC3.Text = ps.cname;
                    }
                    else if (i == 3)
                    {
                        labDC4.Text = d1;
                        labTC4.Text = d2;
                        labCC4.Text = ps.cname;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.getErrorToLog(":: setPersonCheckin ::" + ex.ToString(), "frm_checkin");

            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = dtm.DefaultView;
                dv.RowFilter = string.Format("ชื่อ like '%{0}%' or รหัส like '%{0}%' or ชื่อเล่น like '%{0}%'", txtSearch.Text);

                dataGridView1.DataSource = dv.ToTable();

                if (dataGridView1.Rows.Count > 0)
                {
                    rowindex = -1;
                    dataGridView1_CellContentClick(sender, null);
                }
                else if (dataGridView1.Rows.Count == 0)
                {
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    perSonCode = "";
                    Global.PERSON = new Person();
                    clearLab();
                    clearDataPerson();
                }

                if (txtSearch.Text.Equals(""))
                {
                    dataGridView1.ClearSelection();
                    dataGridView2.Rows.Clear();
                    perSonCode = "";
                    Global.PERSON = new Person();
                    clearLab();
                    clearDataPerson();
                }
                else
                {
                    dataGridView1_CellContentClick(sender, null);
                }

                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (Global.PERSON.PERSON_NAME != null) {
                Global.PERSON.PERSON_NAME = Global.PERSON.PERSON_TITLE + Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME;
                Global.PERSON.PERSON_IMAGE = picCus;

                fm.btnBuyPackage();

            }
            
        }



        private void label29_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {

            if (dataGridView2.CurrentCell != null)
            {
                int rowindex = dataGridView2.CurrentCell.RowIndex;

                string Balance = dataGridView2.Rows[rowindex].Cells[3].Value.ToString() + " " + dataGridView2.Rows[rowindex].Cells[4].Value.ToString();

                string status = "Active";
                if (dataGridView2.Rows[rowindex].Cells[7].Value.ToString().Equals("E"))
                {
                    status = "Expire";
                }

                labelCode.Text = dataGridView2.Rows[rowindex].Cells[10].Value.ToString();
                labeNamePackage.Text = dataGridView2.Rows[rowindex].Cells[0].Value.ToString();
                labelDetail.Text = dataGridView2.Rows[rowindex].Cells[8].Value.ToString();
                labelRegNo.Text = dataGridView2.Rows[rowindex].Cells[5].Value.ToString();
                labelDateStart.Text = dataGridView2.Rows[rowindex].Cells[1].Value.ToString();
                labelDateEnd.Text = dataGridView2.Rows[rowindex].Cells[2].Value.ToString();
                labeBalance.Text = Balance;
                labelStatus.Text = status;
                labelInvoice.Text = dataGridView2.Rows[rowindex].Cells[6].Value.ToString();
                labelDateInvoce.Text = dataGridView2.Rows[rowindex].Cells[9].Value.ToString();
            }
            else {
                clearLab();
            } 
        }

        private void clearLab()
        {
            labelCode.Text = "";
            labeNamePackage.Text = "";
            labelDetail.Text = "";
            labelRegNo.Text = "";
            labelDateStart.Text = "";
            labelDateEnd.Text = "";
            labeBalance.Text = "";
            labelStatus.Text = "";
            labelInvoice.Text = "";
            labelDateInvoce.Text = "";
        }

        private void clearDataPerson()
        {
            labCode.Text = "";
            labName.Text = "";
            labLname.Text = "";
            txtNote.Text = "";
            labStatus.Text = "";
            picCus = "";
            picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
            picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            picPerson.Size = new System.Drawing.Size(148, 178);

            labDC1.Text = "";
            labDC2.Text = "";
            labDC3.Text = "";
            labDC4.Text = "";

            labTC1.Text = "";
            labTC2.Text = "";
            labTC3.Text = "";
            labTC4.Text = "";

            labCC1.Text = "";
            labCC2.Text = "";
            labCC3.Text = "";
            labCC4.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Global.PERSON.PERSON_NAME != null) {
                Global.PERSON.PERSON_NAME = Global.PERSON.PERSON_TITLE + Global.PERSON.PERSON_NAME + " " + Global.PERSON.PERSON_LASTNAME;
                Global.PERSON.PERSON_IMAGE = picCus;
                Global.PERSON.CHECKIN = "Y";

                fm.PersonCode = Global.PERSON.PERSON_CODE;
                fm.btnCheckinPerson();

            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            active = "REF";
            getPersons("ACTIVE");
            dataGridView1.ClearSelection();
            dataGridView2.Rows.Clear();
            clearLab();
            perSonCode = "";
            Global.PERSON = new Person();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void frm_customer_Shown(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
            dataGridView2.Rows.Clear();
            perSonCode = "";
            Global.PERSON = new Person();
            clearLab();
            clearDataPerson();
            hideloading();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show("xxx");
            dataGridView1.Refresh();
           if (dataGridView1.CurrentCell != null)
            {
                rowindex = dataGridView1.CurrentCell.RowIndex;

                if (!dataGridView1.Rows[rowindex].Cells[1].Value.ToString().Equals(perSonCode))
                {

                    dataGridView2.Rows.Clear();
                    clearLab();

                    perSonCode = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                    indexPerson = int.Parse(dataGridView1.Rows[rowindex].Cells[12].Value.ToString());
                    Global.PERSON.PERSON_CODE = perSonCode;
                    Global.PERSON.PERSON_NICKNAME = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                    Global.PERSON.PERSON_NAME = dataGridView1.Rows[rowindex].Cells[4].Value.ToString();
                    Global.PERSON.PERSON_LASTNAME = dataGridView1.Rows[rowindex].Cells[5].Value.ToString();
                    Global.PERSON.PERSON_NOTE = dataGridView1.Rows[rowindex].Cells[9].Value.ToString();
                    Global.PERSON.PERSON_REGISTER_DATE = dataGridView1.Rows[rowindex].Cells[13].Value.ToString();
                    Global.PERSON.PERSON_EXPIRE_DATE = dataGridView1.Rows[rowindex].Cells[14].Value.ToString();
                    Global.PERSON.PERSON_STATUS = dataGridView1.Rows[rowindex].Cells[11].Value.ToString();

                    labCode.Text = Global.PERSON.PERSON_CODE;
                    labName.Text = Global.PERSON.PERSON_NAME;
                    labLname.Text = Global.PERSON.PERSON_LASTNAME;
                    txtNote.Text = Global.PERSON.PERSON_NOTE;

                    if (Global.PERSON.PERSON_REGISTER_DATE != null && Global.PERSON.PERSON_EXPIRE_DATE != null && !Global.PERSON.PERSON_EXPIRE_DATE.Equals(""))
                    {
                        DateTime ds = DateTime.Parse(Global.PERSON.PERSON_REGISTER_DATE);
                        DateTime de = DateTime.Parse(Global.PERSON.PERSON_EXPIRE_DATE);
                        if (de > ds)
                        {
                            this.labStatus.ForeColor = System.Drawing.Color.GreenYellow;
                            labStatus.Text = "ACTIVE";
                            button2.Enabled = true;
                        }
                        else
                        {
                            this.labStatus.ForeColor = System.Drawing.Color.Red;
                            labStatus.Text = "EXPIRE";
                            button2.Enabled = false;
                        }
                    }
                    else
                    {
                        this.labStatus.ForeColor = System.Drawing.Color.Yellow;
                        labStatus.Text = "INACTIVE";
                        button2.Enabled = false;
                    }

                    Global.PERSON.PERSON_STATUS = labStatus.Text;


                    if (!dataGridView1.Rows[rowindex].Cells[10].Value.ToString().Equals(""))
                    {
                        picCus = dataGridView1.Rows[rowindex].Cells[10].Value.ToString();
                        Utils.setImage(picCus, picPerson);
                    }
                    else
                    {
                        picCus = "";
                        picPerson.BackgroundImage = global::MONKEY_APP.Properties.Resources.man_user;
                        picPerson.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
                        picPerson.Size = new System.Drawing.Size(148, 178);
                    }
                    Global.PERSON.PERSON_IMAGE = picCus;

                    Global.PERSON.CHECKIN = "";
                    setPersonPackage(Global.PERSON.PERSON_CODE);
                    setPersonCheckin("'CI'", "'MB'");
                }
            }
        }
    }
}
